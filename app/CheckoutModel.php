<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckoutModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'orders';
    protected $fillable = [
        'user_id', 'frame_id', 'total_tile', 'total_price', 'coupon_code', 'is_applied_coupon',
        'address_id', 'delivery_date', 'order_status'
    ];
    //public $timestamps = false;
     //'full_name', 'street_address', 'city_id', 'state_id', 'zip_code',
      //  'country_id', 'contact_number', 'is_applied_coupon'
}
