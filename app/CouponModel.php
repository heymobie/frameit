<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'coupon';
    protected $fillable = [
        'name', 'start_date', 'end_date', 'no_of_uses', 'uses_limit', 'code', 'type', 'value'
    ];
    //public $timestamps = false;
}
