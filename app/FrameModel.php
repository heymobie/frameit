<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class FrameModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'frames';
    protected $fillable = [
        'title', 'quantity', 'price', 'images',
    ];
    //public $timestamps = false;
}
