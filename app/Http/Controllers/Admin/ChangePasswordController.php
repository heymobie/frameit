<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;

class ChangePasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin/change_password');
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("message","Password changed successfully !");
    }

    public function change_username(Request $request){
        $user_id = base64_decode($request->id);
        $data['user_info'] = User::find($user_id);

        return view('admin/change_username')->with($data) ;
    }

    public function update_username(Request $request){

        $user_id = $request->input('user_id') ;
        $email = $request->input('email') ;

        $userData = User::where('id', $user_id)->first();
        
        $userData->email = $email;
        $userData->updated_at = date('Y-m-d H:i:s');
        $res = $userData->save();

        if($res){
            session::flash('message', 'Username updated succesfully.');
            return redirect('admin/change_username');
        }else{
            session::flash('error', 'Somthing went wrong.');
            return redirect('admin/change_username');
        } 
    }

}