<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\CmsModel;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;

class CmsmgmtController extends Controller 
{
	public function cms_mgmt_list() {
		$data['cms_list'] = DB::table('cms_mgmt')->get();
		return view('admin/cms/cms_mgmt_list')->with($data);
	}

	public function add_cms_mgmt(){
		return view('admin/cms/add_cms_mgmt'); 
	}

	public function submit_cms_mgmt(Request $request){

		$validator = Validator::make($request->all(), [
			'page_name' => 'required',
			'page_title' => 'required',
			'description' => 'required'
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/add_cms_mgmt')->withErrors($validator)->withInput(); 
		} else {

			$page_name = $request->page_name;
			$page_title = $request->page_title;
			$description = $request->description;
			
			$obj = new CmsModel;
			$obj->page_name = $page_name;
			$obj->title = $page_title;
			$obj->description = $description;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$res = $obj->save();
			if($res){
				session::flash('message', 'Record Addeed Succesfully.');
				return redirect('admin/cms_mgmt_list');
			}else{
				session::flash('error', 'Record not inserted.');
				return redirect('admin/cms_mgmt_list');
			}
		}
	}


	public function edit_cms_mgmt(Request $request){
		$cms_id = base64_decode($request->id);
		$data['cms_info'] = CmsModel::find($cms_id);

		return view('admin/cms/edit_cms_mgmt')->with($data) ;
	}

	public function update_cms_mgmt(Request $request){

		$cms_id = $request->input('cms_id') ;
		$page_title = $request->input('page_title') ;
		$page_name = $request->input('page_name') ;
		$description = $request->input('description') ;

		$cmsData = CmsModel::where('id', $cms_id)->first();

		$cmsData->title = $page_title;
		$cmsData->page_name = $page_name;
		$cmsData->description = $description;;
		$cmsData->updated_at = date('Y-m-d H:i:s');
		$res = $cmsData->save();
		
		if($res){
			session::flash('message', 'Record updated succesfully.');
			return redirect('admin/cms_mgmt_list');
		}else{
			session::flash('error', 'Somthing went wrong.');
			return redirect('admin/cms_mgmt_list');
		} 
	} 


	public function change_cms_status(Request $request)
    {
        $cms_info = CmsModel::find($request->cms_id);
        $cms_info->status = $request->status;
        $cms_info->save();
  
        return response()->json(['success'=>'Cms mgmt status change successfully.']);
    }


	public function delete_cms_mgmt(Request $request) {
		$cms_id = $request->cms_id;

		$cms_info = DB::table('cms_mgmt')->where('id','=',$cms_id)->first();

		$res = DB::table('cms_mgmt')->where('id', '=', $cms_id)->delete();	

		if ($res) {
			return json_encode(array('status' => 'success','msg' => 'Data has been deleted successfully!'));
		} else {
			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
		}

	}

	

}
?>