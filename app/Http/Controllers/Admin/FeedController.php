<?php

namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller; 

use App\FeedsModel;

use Illuminate\Support\Facades\Auth; 

use Validator, DB;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use Session;



class FeedController extends Controller 

{

	public function feed_list() {

		$data['feed_list'] = DB::table('feeds')->get();

		return view('admin/feeds/feed_list')->with($data);

	}



	public function add(){

		return view('admin/feeds/add_feed'); 

	}



	public function submit_feed(Request $request){

		//print_r($request->all());die;

		$validator = Validator::make($request->all(), [

			'title' => 'required',

			'description' => 'required',

			'picture' => 'required|mimes:jpeg,png,jpg,gif,svg|max:5000',

		]);

		if ($validator->fails()) {

			session::flash('error', 'Validation error.');

			return redirect('/admin/add')->withErrors($validator)->withInput(); 

		} else {



			$title = $request->title;

			$description = $request->description;



			if ($request->hasFile('picture')){



				$feedImage = $request->file('picture');

				$extension  = $feedImage->getClientOriginalName();

				$imgName = time().'_'.$extension;

				$destinationPath = public_path('/uploads/feed_images/');

				$feedImage->move($destinationPath, $imgName);



			}



			$obj = new FeedsModel;

			$obj->title = $title;

			$obj->description = $description;

			$obj->picture = $imgName;

			$obj->status = 1;

			$obj->created_at = date('Y-m-d H:i:s');

			$res = $obj->save();

			if($res){
				$users = DB::table('users')->get();

				foreach($users as $user){
					if(!empty($user->device_token)){
						$title ="Please check new article added !";
						$this->notification($user->device_token, $title);
					}
					
				}

				session::flash('message', 'Record Addeed Succesfully.');

				return redirect('admin/feed_list');

			}else{

				session::flash('error', 'Record not inserted.');

				return redirect('admin/feed_list');

			}

		}

	}





	public function edit(Request $request){

		$feed_id = base64_decode($request->id);

		$data['feed_info'] = FeedsModel::find($feed_id);



		return view('admin/feeds/edit_feed')->with($data) ;

	}



	public function update_feed(Request $request){



		$feed_id = $request->input('feed_id') ;

		$title = $request->input('title') ;

		$description = $request->input('description') ;



		//$request->validate([

			//'picture' => 'image|mimes:jpeg,png,jpg|max:2048'

		//]);



		$feedData = FeedsModel::where('id', $feed_id)->first();



		if ($request->hasFile('picture')){

			$image_path = public_path("/uploads/feed_images/".$feedData->picture);

			if (file_exists($image_path)) {

				@unlink($image_path);

			}



			$feedImage = $request->file('picture');

			$extension  = $feedImage->getClientOriginalName();

			$imgName = time().'_'.$extension;

			$destinationPath = public_path('/uploads/feed_images/');

			$feedImage->move($destinationPath, $imgName);

			

		} else {

			$imgName = $feedData->picture;

		}



		$feedData->title = $title;

		$feedData->description = $description;

		$feedData->picture = $imgName;

		$feedData->updated_at = date('Y-m-d H:i:s');

		$res = $feedData->save();

		

		if($res){

			session::flash('message', 'Record updated succesfully.');

			return redirect('admin/feed_list');

		}else{

			session::flash('error', 'Somthing went wrong.');

			return redirect('admin/feed_list');

		} 

	}





	public function change_feed_status(Request $request)

    {

        $feed = FeedsModel::find($request->feed_id);

        $feed->status = $request->status;

        $feed->save();

  		

        return response()->json(['success'=>'User status change successfully.']);

    } 





	public function delete_feed(Request $request) {

		$feed_id = $request->feed_id;



		$feed_info = DB::table('feeds')->where('id','=',$feed_id)->first();

		$media_url = $feed_info->picture;



		$res = DB::table('feeds')->where('id', '=', $feed_id)->delete();



		if ($res) {

			@unlink(url('/').'/public/uploads/feed_images/'.$media_url);

			return json_encode(array('status' => 'success','msg' => 'Image has been deleted successfully!'));

		} else {

			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));

		}



	}
	 public function notification($token, $title)

    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $token=$token;



        $notification = [

            'title' => 'FRAMEiT',

            'body' => $title,

            'tag' =>'article',

            'sound' => true,

        ];

        

        $extraNotificationData = $notification;



        $fcmNotification = [

            //'registration_ids' => $tokenList, //multple token array

            'to'        => $token, //single token

            'notification' => $notification,

            'data' => $extraNotificationData

        ];



        $headers = [

            'Authorization: key=AAAASk5-u2g:APA91bHQE__pE-TXQszZb-TorN-8v9dbStYqnp_vIfVWitAXGrnfuVr4YZInoK53FZPEPIj6ej3izSvMwNkbWEb54dcrflqMMwAyjtnjpU1TwS4CyxP_9iHQRAYDlC25yI32Vhs6-4sZ',

            'Content-Type: application/json'

        ];





        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$fcmUrl);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));

        $result = curl_exec($ch);

        curl_close($ch);



        return true;

    }





}

?>