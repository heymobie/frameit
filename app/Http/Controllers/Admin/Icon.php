<?php
	namespace App\Http\Controllers\Admin;

	use Illuminate\Support\Facades\Hash;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller; 
	use App\User;
	use App\IconModel;
	use App\Helpers\Helper;
	use Illuminate\Support\Facades\Auth; 
	use Validator, DB;
	use Illuminate\Validation\Rule;
	use Twilio\Rest\Client;
	use Session;
	use Mail;

	class Icon extends Controller{

		public function show_icons(){
	        $icon_details = DB::select('select * from icon_table');
	        return view('admin/icons/show_icons',['icon_details'=>$icon_details]);
	    }

		public function addIcons(){
	        return view('admin/icons/add_icon'); 
	    }
	 

		public function submit_icons(Request $request){
	        $validator = Validator::make($request->all(), [
	            'icon_image' => 'required',
	            'icon_title' => 'required',
	            'icon_description' => 'required'
	            
	        ]);
	        if ($validator->fails()) {
	            session::flash('error', 'Validation error.');
	            return redirect('/admin/addIcons')->withErrors($validator)->withInput(); 
	        } else {
	            $icon_image = $request->file('icon_image');
	            $icon_title = $request->input('icon_title');
	            $icon_description = $request->input('icon_description');

	            
	            $icon_image->move(public_path('/uploads/icons/'),$icon_image->getClientOriginalName());
	            
	            DB::insert('insert into icon_table (icon_image,icon_title,icon_description,status,created_at) values(?,?,?,1,?)',[$icon_image->getClientOriginalName(),$icon_title, $icon_description,date('Y-m-d H:i:s')]);

	            session::flash('message', 'Record Addeed Succesfully.');

	            return redirect('admin/showIcons');
	            
	        }
	    }

	    public function editIcons($id){
	        $icons = DB::select('select * from icon_table where icon_id = ?',[$id]);
	        return view('admin/icons/edit_icons',['icon_details'=>$icons]); 
	    }

	    public function updateIcons(Request $request, $id){
            $icons = DB::select('select * from icon_table where icon_id = ?',[$id]);
            
            $icon_image_data = (array)$icons[0]; 
            
            $icon_image_one = $request->file('icon_image');
            $icon_title = $request->input('icon_title');
            $icon_description = $request->input('icon_description');

            if($icon_image_one){
                $icon_image = $icon_image_one->getClientOriginalName();
                $icon_image_one->move(public_path('/uploads/icons/'),$icon_image_one->getClientOriginalName());
            }else{
                $icon_image = $icon_image_data['icon_image'];
            }
            
            
            DB::update('update icon_table set icon_image = ?, icon_title= ?, icon_description = ? where icon_id = ?',[$icon_image,$icon_title,$icon_description, $id]);

            session::flash('message', 'Record Updated Succesfully.');

            return redirect('admin/showIcons');
            
        

    	}

    	public function deleteIcons(Request $request) {

	        $icon_id = $request->icon_id;

             

	        $icon_info = DB::table('icon_table')->where('icon_id','=',$icon_id)->first();
	        
	        $media_url = $icon_info->icon_image;
	        



	        $res = DB::table('icon_table')->where('icon_id', '=', $icon_id)->delete();



	        if ($res) {

	            @unlink(url('/').'/public/uploads/icons/'.$media_url);

	            return json_encode(array('status' => 'success','msg' => 'Icon Details has been deleted successfully!'));

	        } else {

	            return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));

	        }



	    }
	    public function change_icon_status(Request $request)

	    {
	        
	        $feed = IconModel::find($request->icon_id);
	        
	        $feed->status = $request->status;

	        $feed->save();

	        

	        return response()->json(['success'=>'Feed Title status change successfully.']);

	    } 
	}
?>