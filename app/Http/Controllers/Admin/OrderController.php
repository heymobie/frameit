<?php

namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller; 

use App\CheckoutModel;

use Illuminate\Support\Facades\Auth; 

use Validator, DB;

use Illuminate\Validation\Rule;

use Twilio\Rest\Client;

use App\Helpers\Helper;

use Session;

use Mail;



class OrderController extends Controller 

{

	public function order_list() {

		$data['order_list'] =  DB::table('orders')

        ->select('frames.title','frames.images','frames.currency','users.first_name', 'users.last_name', 'orders.id', 'orders.total_tile', 'orders.total_price', 

            'orders.shipping_price', 'orders.coupon_discount', 'orders.wallet_discount', 'orders.address_id','orders.frame_id', 'orders.color', 'orders.order_status', 'orders.delivery_date', 'orders.status', 'orders.vendor_id', 'orders.created_at')

        ->join('users','users.id','=','orders.user_id')

        ->join('frames','frames.id','=','orders.frame_id')

        ->where('orders.status', 1)

        ->where('users.status', 1)

        ->orderBy('orders.id', 'DESC')

        ->get();

		return view('admin/orders/order_list')->with($data);

	}



	public function view_order_details(Request $request){

		$order_id = base64_decode($request->id);

        $data['order_info'] =  DB::table('orders')

        ->select('frames.images', 'frames.type' ,'frames.currency','users.first_name', 'users.last_name', 'orders.id', 'orders.total_price', 'orders.color','orders.shipping_price', 'shipping_price.currency as shipping_currency', 'orders.address_id', 'users.email', 'users.contact_number as contact', 'users.address','orders.payment_status',

            'orders.order_status', 'orders.delivery_date', 'orders.created_at', 'shipping_address.fullname','shipping_address.street_address','shipping_address.city', 'orders.vendor_id',

            'shipping_address.state','countries.name as country','shipping_address.zip_code','shipping_address.contact_number')

        ->join('users','users.id','=','orders.user_id')

        ->join('frames','frames.id','=','orders.frame_id')

        ->join('shipping_address','shipping_address.id','=','orders.address_id')

        ->join('countries','countries.id','=','shipping_address.country')

        ->join('shipping_price','shipping_price.country_id','=','shipping_address.country')

        ->where('orders.id', $order_id )

        ->where('users.status', 1)

        ->first();



		$data['order_details'] =  DB::table('order_detail')

        ->select('frames.images', 'frames.price','frames.currency', 'order_detail.order_id', 'order_detail.tile_image', 'order_detail.rotation', 'order_detail.quantity',  'orders.address_id', 

          'orders.frame_id', 'orders.shipping_price', 'shipping_price.currency as shipping_currency', 'orders.order_status','orders.color','orders.payment_status',

            'orders.total_price', 'orders.coupon_discount', 'orders.wallet_discount' , 'orders.delivery_date')

        ->join('orders','orders.id','=','order_detail.order_id')

        ->join('frames','frames.id','=','orders.frame_id')

        ->join('shipping_address','shipping_address.id','=','orders.address_id')

        ->join('shipping_price','shipping_price.country_id','=','shipping_address.country')

        ->where('order_detail.order_id',$order_id)

        ->get();


        $data['vendor'] =  DB::table('vendor')

        ->where('status',1)

        ->get();



		return view('admin/orders/view_order_details')->with($data) ;

	}



	public function change_order_status(Request $request)

    {

        $order_info = CheckoutModel::find($request->order_id);

        $date = $order_info->created_at;

        $order_info->order_status = $request->status;

        $res = $order_info->save();



        if ($res) {



            $users = DB::table('users')->where('id','=',$order_info->user_id)->first(); 



            $username = $users->first_name." ".$users->last_name;



            $title ="Hey ". $username ." your order status is ". $request->status;



            //$device_token ="ftkpkxjW37Q:APA91bE_27LnEi3ziGsQpVbZnvh6MlNxovsEVVQSGPDMlnTYt47IiQP25JuSwC0AqiGeGwHa3LAFZ9mw8IKmRkMD1lzKRSeMEBPNCpvsIkKA4AcNAieLbQ-OFlXq74jiCUo2xfPy2fi8";

            

            $this->notification($users->device_token, $title, $request->order_id, $date);

          

                      

            if ($_SERVER['SERVER_NAME'] != 'localhost') {

                $data['url'] = url('/');

                $data['status'] = $request->status;

                $data['first_name'] = $users->first_name;

                $data['last_name'] = $users->last_name;

                $data['status'] = $request->status;

                $data['order_id'] = $request->order_id;





                $fromEmail = Helper::getFromEmail();

                $inData['from_email'] = $fromEmail;

                $inData['email'] = $users->email;

                Mail::send('emails.invoice.order_status_template',$data, function ($message) use ($inData) {

                    $message->from($inData['from_email'],'FRAMEiT');

                    $message->to($inData['email']);

                    $message->subject('FRAMEiT - Order Status Mail');

                });



            }



            return response()->json(['success'=>'Order status change successfully.']);



        }   

    }


    public function assignordertovendor(Request $request)

    {

        $order_info = CheckoutModel::find($request->order_id);

        $date = $order_info->created_at;

        $order_info->vendor_id = $request->vendor_id;

        $res = $order_info->save();



        if ($res) {



            // $users = DB::table('users')->where('id','=',$order_info->user_id)->first(); 



            // $username = $users->first_name." ".$users->last_name;



            // $title ="Hey ". $username ." your order status is ". $request->vendor_id;



            //$device_token ="ftkpkxjW37Q:APA91bE_27LnEi3ziGsQpVbZnvh6MlNxovsEVVQSGPDMlnTYt47IiQP25JuSwC0AqiGeGwHa3LAFZ9mw8IKmRkMD1lzKRSeMEBPNCpvsIkKA4AcNAieLbQ-OFlXq74jiCUo2xfPy2fi8";

            

            //$this->notification($users->device_token, $title, $request->order_id, $date);

          

                      

            // if ($_SERVER['SERVER_NAME'] != 'localhost') {

            //     $data['url'] = url('/');

            //     $data['status'] = $request->vendor_id;

            //     $data['first_name'] = $users->first_name;

            //     $data['last_name'] = $users->last_name;

            //     $data['status'] = $request->vendor_id;

            //     $data['order_id'] = $request->order_id;





            //     $fromEmail = Helper::getFromEmail();

            //     $inData['from_email'] = $fromEmail;

            //     $inData['email'] = $users->email;

            //     Mail::send('emails.invoice.order_status_template',$data, function ($message) use ($inData) {

            //         $message->from($inData['from_email'],'FRAMEiT');

            //         $message->to($inData['email']);

            //         $message->subject('FRAMEiT - Order Status Mail');

            //     });



            // }



            return response()->json(['success'=>'Order assigned successfully.']);



        }   

    }





    public function notification($token, $title, $order_id, $date)

    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $token=$token;



        $notification = [

            'id' => $order_id,

            'title' => 'FRAMEiT',

            'body' => $title,

            'tag' =>'order_status',

            'date' =>$date,

            'sound' => true,

        ];

        

        $extraNotificationData = $notification;



        $fcmNotification = [

            //'registration_ids' => $tokenList, //multple token array

            'to'        => $token, //single token

            'notification' => $notification,

            'data' => $extraNotificationData

        ];



        $headers = [

            'Authorization: key=AAAASk5-u2g:APA91bHQE__pE-TXQszZb-TorN-8v9dbStYqnp_vIfVWitAXGrnfuVr4YZInoK53FZPEPIj6ej3izSvMwNkbWEb54dcrflqMMwAyjtnjpU1TwS4CyxP_9iHQRAYDlC25yI32Vhs6-4sZ',

            'Content-Type: application/json'

        ];





        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$fcmUrl);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));

        $result = curl_exec($ch);

        curl_close($ch);



        return true;

    }





	public function delete_order_item(Request $request) {



		$order_id = $request->order_id;



		$order_info = DB::table('orders')->where('id','=',$order_id)->first();



		$res = DB::table('orders')->where('id', '=', $order_id)->delete();	



		if ($res) {

			return json_encode(array('status' => 'success','msg' => 'Order has been deleted successfully!'));

		} else {

			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));

		}



	}



}

?>