<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\ReferralModel;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;

class ReferralController extends Controller 
{
	public function referral_commission_list() {
		$data['commission_list'] = DB::table('referral_commission_settings')->get();
		return view('admin/referrals/referral_commission_list')->with($data);
	}

	public function customer_earning_list() {
		$data['earning_list'] =  DB::table('wallet_transaction')
        ->select('users.first_name', 'users.last_name', 'wallet_transaction.id', 'wallet_transaction.credit_amount', 'wallet_transaction.debit_amount', 
        	'users.wallet_balance','wallet_transaction.status', 'wallet_transaction.created_at')
        ->leftjoin('users','users.id','=','wallet_transaction.user_id')
        ->where('users.status', 1)
        ->get();
		return view('admin/referrals/customer_earning_list')->with($data);
	}

	public function add_referral_commission(){	
		return view('admin/referrals/add_referral_commission'); 
	}

	public function submit_referral_commission(Request $request){
		//print_r($request->all());die;
		$validator = Validator::make($request->all(), [
			'no_of_orders' => 'required',
			'order_amount' => 'required',
			'referral_amount' => 'required'
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/add_referral_commission')->withErrors($validator)->withInput(); 
		} else {

			$no_of_orders = $request->no_of_orders;
			$order_amount = $request->order_amount;
			$referral_amount = $request->referral_amount;

			$obj = new ReferralModel;
			$obj->no_of_orders = $no_of_orders;
			$obj->order_amount = $order_amount;
			$obj->referral_amount = $referral_amount;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$res = $obj->save();
			if($res){
				session::flash('message', 'Record Addeed Succesfully.');
				return redirect('admin/referral_commission_list');
			}else{
				session::flash('error', 'Record not inserted.');
				return redirect('admin/referral_commission_list');
			}
		}
	}


	public function edit_referral_commission(Request $request){
		$commission_id = base64_decode($request->id);
		$data['commission_info'] = ReferralModel::find($commission_id);

		return view('admin/referrals/edit_referral_commission')->with($data) ;
	}

	public function update_referral_commission(Request $request){

		$commission_id = $request->input('commission_id') ;
		$no_of_orders = $request->input('no_of_orders') ;
		$order_amount = $request->input('order_amount') ;
		$referral_amount = $request->input('referral_amount') ;

		$commissionData = ReferralModel::where('id', $commission_id)->first();

		$commissionData->no_of_orders = $no_of_orders;
		$commissionData->order_amount = $order_amount;
		$commissionData->referral_amount = $referral_amount;
		$commissionData->updated_at = date('Y-m-d H:i:s');
		$res = $commissionData->save();
		
		if($res){
			session::flash('message', 'Record updated succesfully.');
			return redirect('admin/referral_commission_list');
		}else{
			session::flash('error', 'Somthing went wrong.');
			return redirect('admin/referral_commission_list');
		} 
	}


	public function change_commission_status(Request $request)
    {
        $commission = ReferralModel::find($request->commission_id);
        $commission->status = $request->status;
        $commission->save();
  
        return response()->json(['success'=>'Referral commission status change successfully.']);
    } 


	public function delete_referral_commission(Request $request) {
		$commission_id = $request->commission_id;

		$commission_info = DB::table('referral_commission_settings')->where('id','=',$commission_id)->first();

		$res = DB::table('referral_commission_settings')->where('id', '=', $commission_id)->delete();

		if ($res) {
			return json_encode(array('status' => 'success','msg' => 'Referral commission has been deleted successfully!'));
		} else {
			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
		}

	}


}
?>