<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\ShipCodeModel;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;

class ShipmentTrackController extends Controller 
{
	public function tracking_code_list() {
		$data['track_list'] =  DB::table('order_tracking')
        ->select('users.first_name','users.last_name' ,'order_tracking.id', 'order_tracking.order_id', 'order_tracking.tracking_code', 'order_tracking.tracking_status', 'order_tracking.created_at')
        ->join('users','users.id','=','order_tracking.user_id')
        ->get();
		return view('admin/shipping/tracking_code_list')->with($data);
	}

	public function add_tracking_code(){	
		return view('admin/shipping/add_tracking_code')->with($data); 
	}

	public function submit_tracking_code(Request $request){
		//print_r($request->all());die;
		$validator = Validator::make($request->all(), [
			'country_id' => 'required',
			'price' => 'required',
			'currency' => 'required',
			'delivery_days' => 'required'
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/add_tracking_code')->withErrors($validator)->withInput(); 
		} else {

			$country_id = $request->country_id;
			$price = $request->price;
			$currency = $request->currency;
			$delivery_days = $request->delivery_days;


			$obj = new ShipCodeModel;
			$obj->country_id = $country_id;
			$obj->price = $price;
			$obj->currency = $currency;
			$obj->delivery_days = $delivery_days;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$res = $obj->save();
			if($res){
				session::flash('message', 'Record Addeed Succesfully.');
				return redirect('admin/tracking_code_list');
			}else{
				session::flash('error', 'Record not inserted.');
				return redirect('admin/add_tracking_code');
			}
		}
	}


	public function edit_tracking_code(Request $request){
		$track_id = base64_decode($request->id);
		$data['track_info'] = ShipCodeModel::find($track_id);

		return view('admin/shipping/edit_tracking_code')->with($data) ;
	}

	public function update_tracking_code(Request $request){

		$track_id = $request->input('track_id') ;
		$tracking_code = $request->input('tracking_code') ;

		$trackData = ShipCodeModel::where('id', $track_id)->first();

		$trackData->tracking_code = $tracking_code;
		$trackData->updated_at = date('Y-m-d H:i:s');
		$res = $trackData->save();
		
		if($res){
			session::flash('message', 'Record updated succesfully.');
			return redirect('admin/tracking_code_list');
		}else{
			session::flash('error', 'Somthing went wrong.');
			return redirect('admin/tracking_code_list');
		} 
	}


	public function change_tracking_code_status(Request $request)
    {
        $track = ShipCodeModel::find($request->track_id);
        $track->tracking_status = $request->status;
        $track->save();
  
        return response()->json(['success'=>'Shipment tracking code status change successfully.']);
    } 


	public function delete_tracking_code(Request $request) {
		$track_id = $request->track_id;

		$fee_info = DB::table('order_tracking')->where('id','=',$track_id)->first();

		$res = DB::table('order_tracking')->where('id', '=', $track_id)->delete();

		if ($res) {
			return json_encode(array('status' => 'success','msg' => 'Shipment tracking code has been deleted successfully!'));
		} else {
			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
		}

	}


}
?>