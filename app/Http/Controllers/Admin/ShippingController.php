<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\ShipPriceModel;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;

class ShippingController extends Controller 
{
	public function shipping_price_list() {
		$data['price_list'] =  DB::table('shipping_price')
        ->select('countries.name','shipping_price.price' ,'shipping_price.currency', 'shipping_price.delivery_days', 'shipping_price.id', 'shipping_price.status', 'shipping_price.created_at')
        ->join('countries','countries.id','=','shipping_price.country_id')
        ->get();
		return view('admin/shipping/shipping_price_list')->with($data);
	}

	public function add_shipping_price(){	
		$data['country_list'] = DB::table('countries')->get();
		return view('admin/shipping/add_shipping_price')->with($data); 
	}

	public function submit_shipping_price(Request $request){
		//print_r($request->all());die;
		$validator = Validator::make($request->all(), [
			'country_id' => 'required',
			'price' => 'required',
			'delivery_days' => 'required'
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/add_shipping_price')->withErrors($validator)->withInput(); 
		} else {

			$country_id = $request->country_id;
			$price = $request->price;
			$delivery_days = $request->delivery_days;


			$obj = new ShipPriceModel;
			$obj->country_id = $country_id;
			$obj->price = $price;
			$obj->currency = 'MYR';
			$obj->delivery_days = $delivery_days;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$res = $obj->save();
			if($res){
				session::flash('message', 'Record Addeed Succesfully.');
				return redirect('admin/shipping_price_list');
			}else{
				session::flash('error', 'Record not inserted.');
				return redirect('admin/add_shipping_price');
			}
		}
	}


	public function edit_shipping_price(Request $request){
		$fee_id = base64_decode($request->id);
		$data['country_list'] = DB::table('countries')->get();
		$data['fee_info'] = ShipPriceModel::find($fee_id);

		return view('admin/shipping/edit_shipping_price')->with($data) ;
	}

	public function update_shipping_price(Request $request){

		$fee_id = $request->input('fee_id') ;
		$country_id = $request->input('country_id') ;
		$price = $request->input('price') ;
		$delivery_days = $request->input('delivery_days') ;

		$feeData = ShipPriceModel::where('id', $fee_id)->first();

		$feeData->country_id = $country_id;
		$feeData->price = $price;
		$feeData->currency = 'MYR';
		$feeData->delivery_days = $delivery_days;
		$feeData->updated_at = date('Y-m-d H:i:s');
		$res = $feeData->save();
		
		if($res){
			session::flash('message', 'Record updated succesfully.');
			return redirect('admin/shipping_price_list');
		}else{
			session::flash('error', 'Somthing went wrong.');
			return redirect('admin/shipping_price_list');
		} 
	}


	public function change_shipping_price_status(Request $request)
    {
        $fee = ShipPriceModel::find($request->fee_id);
        $fee->status = $request->status;
        $fee->save();
  
        return response()->json(['success'=>'Shipping fee status change successfully.']);
    } 


	public function delete_shipping_price(Request $request) {
		$fee_id = $request->fee_id;

		$fee_info = DB::table('shipping_price')->where('id','=',$fee_id)->first();

		$res = DB::table('shipping_price')->where('id', '=', $fee_id)->delete();

		if ($res) {
			return json_encode(array('status' => 'success','msg' => 'Shipping fee has been deleted successfully!'));
		} else {
			return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
		}

	}


}
?>