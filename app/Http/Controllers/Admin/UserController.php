<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Mail;

class UserController extends Controller 
{
	public function user_list() {
		$data['user_list'] = User::where('role_id','=',2)->orderby('id','DESC')->get();
		return view('admin/users/user_list')->with($data);
	}

	public function add_user(){
		return view('admin/users/add_user'); 
	}


	public function submit_user(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required|unique:users,email',
			'password' => 'required',
			'confirm_password' => 'required|same:password',
			'contact_number' => 'required|numeric|unique:users,contact_number',
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/add_user')->withErrors($validator)->withInput(); 
		} else {
			$vrfn_code = Helper::generateRandomString(6);
			$name = $request->name;
			$email = $request->email;
			$password = $request->password;
			$contact_number = $request->contact_number;
			$name_arr = explode(' ', $name);

			$obj = new User;
			$obj->first_name = (!empty($name_arr[0]) ? $name_arr[0] : '');
			$obj->last_name = (!empty($name_arr[1]) ? $name_arr[1] : '');
			$obj->email = $email;
			$obj->contact_number = $contact_number;
			$obj->password = bcrypt($password);
			$obj->role_id = 2;
			$obj->is_verify_email = 1;
			$obj->is_verify_contact = 0;
			$obj->wallet_balance = 0;
			$obj->register_by = 'web';
			$obj->vrfn_code = $vrfn_code;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$obj->updated_at = date('Y-m-d H:i:s');
			$res = $obj->save();

			if ($res) {

				$users = User::where('email','=',$email)->first();

				$my_referral_code = Helper::my_simple_crypt($users->id,'e');
				$users->my_referral_code = $my_referral_code;
				$users->save();

				if ($_SERVER['SERVER_NAME'] != 'localhost') {

					$vrfn_url = url('/').'/email_verification/'.$vrfn_code.'_'.$users->id;

					$fromEmail = Helper::getFromEmail();
					$inData['from_email'] = $fromEmail;
					$inData['email'] = $email;
	                /*$inData['body'] = "<table>
                	<thead>
                		<tr>
                			<td>name</td>
                			<td>Link</td>
                		</tr>
                	</thead>
                	<tbody>
                		<tr>
                			<td>".$name."</td>
                			<td><a href='".$vrfn_url."'>Verification Link</a></td>
                		</tr>
                	</tbody>
                	</table>";*/
                	$inData['body'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                	<html xmlns="http://www.w3.org/1999/xhtml">
                	<head>
                	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                	<meta name="viewport" content="width=device-width, initial-scale=1"/>
                	<title>Verify your FRAMEiT account</title>
                	<style type="text/css">img{max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}a img{border: none;}table{border-collapse: collapse !important;}#outlook a{padding:0;}.ReadMsgBody{width: 100%;}.ExternalClass{width: 100%;}.backgroundTable{margin: 0 auto; padding: 0; width: 100% !important;}table td{border-collapse: collapse;}.ExternalClass *{line-height: 115%;}.container-for-gmail-android{min-width: 600px;}/* General styling */ *{font-family: Helvetica, Arial, sans-serif;}body{-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; margin: 0 !important; height: 100%; color: #676767;}td{font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;}a{color: #676767; text-decoration: none !important;}.pull-left{text-align: left;}.pull-right{text-align: right;}.header-lg, .header-md, .header-sm{font-size: 32px; font-weight: 700; line-height: normal; padding: 35px 0 0; color: #4d4d4d;}.header-md{font-size: 24px;}.header-sm{padding: 5px 0; font-size: 18px; line-height: 1.3;}.content-padding{padding: 20px 0 30px;}.mobile-header-padding-right{width: 290px; text-align: right; padding-left: 10px;}.mobile-header-padding-left{width: 290px; text-align: left; padding-left: 10px;}.free-text{width: 100% !important; padding: 10px 60px 0px;}.block-rounded{border-radius: 5px; border: 1px solid #e5e5e5; vertical-align: top;}.button{padding: 55px 0 0;}.info-block{padding: 0 20px; width: 260px;}.mini-block-container{padding: 30px 50px; width: 500px;}.mini-block{background-color: #ffffff; width: 498px; border: 1px solid #cccccc; border-radius: 5px; padding: 60px 75px;}.block-rounded{width: 260px;}.info-img{width: 258px; border-radius: 5px 5px 0 0;}.force-width-img{width: 480px; height: 1px !important;}.force-width-full{width: 600px; height: 1px !important;}.user-img img{width: 82px; border-radius: 5px; border: 1px solid #cccccc;}.user-img{width: 92px; text-align: left;}.user-msg{width: 236px; font-size: 14px; text-align: left; font-style: italic;}.code-block{padding: 10px 0; border: 1px solid #cccccc; width: 20px; color: #4d4d4d; font-weight: bold; font-size: 18px;}.mini-img{padding: 5px; width: 140px;}.mini-img img{border-radius: 5px; width: 140px;}.mini-imgs{padding: 25px 0 30px;}.progress-bar{padding: 0 15px 0;}.step{vertical-align: top;}.step img{width: 109px; height: 78px;}.active{font-weight: bold;}</style>
                	<style type="text/css" media="screen"> @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700); </style>
                	<style type="text/css" media="screen"> @media screen{/* Thanks Outlook 2013! */ *{font-family: "Oxygen", "Helvetica Neue", "Arial", "sans-serif" !important;}}</style>
                	<style type="text/css" media="only screen and (max-width: 480px)"> /* Mobile styles */ @media only screen and (max-width: 480px){table[class*="container-for-gmail-android"]{min-width: 290px !important; width: 100% !important;}table[class="w320"]{width: 320px !important;}td[class*="mobile-header-padding-left"]{width: 160px !important;}img[class="force-width-gmail"]{display: none !important; width: 0 !important; height: 0 !important;}td[class="mobile-block"]{display: block !important;}td[class="mini-img"], td[class="mini-img"] img{width: 150px !important;}td[class*="mobile-header-padding-left"]{width: 160px !important; padding-left: 0 !important;}td[class*="mobile-header-padding-right"]{width: 160px !important; padding-right: 0 !important;}td[class="header-lg"]{font-size: 24px !important; padding-bottom: 5px !important;}td[class="header-md"]{font-size: 18px !important; padding-bottom: 5px !important;}td[class="content-padding"]{padding: 5px 0 30px !important;}td[class="button"]{padding: 5px !important;}td[class*="free-text"]{padding: 10px 18px 30px !important;}img[class="force-width-img"], img[class="force-width-full"]{display: none !important;}td[class="info-block"]{display: block !important; width: 280px !important; padding-bottom: 40px !important;}td[class="info-img"], img[class="info-img"]{width: 278px !important;}td[class="mini-block-container"]{padding: 8px 20px !important; width: 280px !important;}td[class="mini-block"]{padding: 20px 0 !important;}td[class*="step"] img{width: 86px !important; height: 62px !important;}td[class="progress-bar"]{padding: 0 11px 25px;}td[class="user-img"]{display: block !important; text-align: center !important; width: 100% !important; padding-bottom: 10px;}td[class="user-msg"]{display: block !important; padding-bottom: 20px;}}</style>
                	</head>
                	<body bgcolor="#f7f7f7">
                	<table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">
                	<tr>
                	<td align="left" valign="top" width="100%">
                	<center>
                	<table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" style="background-color:#333;background: #333;">
                	<tr>
                	<td width="100%" height="80" valign="top" style="text-align: center; vertical-align:middle;">
                	<center>
                	<table cellpadding="0" cellspacing="0" width="600" class="w320">
                	<tr>
                	<td class="pull-left mobile-header-padding-left" style="vertical-align: middle;"> <a href="'.url('/').'"><img width="137" height="47" src="'.url("/").'/resources/front_assets/img/logo.png" alt="logo"></a> </td>
                	</tr>
                	</table>
                	</center>
                	</td>
                	</tr>
                	</table>
                	</center>
                	</td>
                	</tr>
                	<tr>
                	<td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
                	<center>
                	<table cellspacing="0" cellpadding="0" width="600" class="w320">
                	<tr>
                	<td class="header-lg"> Almost done! </td>
                	</tr>
                	<tr>
                	<td class="free-text"> Your Account is created successfully. Your account details </td>
                	</tr>
                	<tr>
                	<td class="mini-block-container">
                	<table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                	<tr>
                	<td class="mini-block">
                	<table cellpadding="0" cellspacing="0" width="100%">
                	<tr>
                	<td class="progress-bar">
                	<table cellpadding="0" cellspacing="0" width="100%"></table>
                	</td>
                	</tr>
                	<tr>
                	<td class="button">
                	<div>
                        <p><b>Username</b> :  $email</p> 
                        <p><b>Password</b> :  $password</p> 
                	</div>
                	</td>
                	</tr>
                	</table>
                	</td>
                	</tr>
                	</table>
                	</td>
                	</tr>
                	</table>
                	</center>
                	</td>
                	</tr>
                	<tr>
                	<td align="center" valign="top" width="100%" style="background-color: #f7f7f7; height: 100px;">
                	<center>
                	<table cellspacing="0" cellpadding="0" width="600" class="w320">
                	<tr>
                	<td style="padding: 25px 0 25px">04-451-1555<br/>In5 Inovation Centre, Dubai Media City <br/>info@frameit.com <br/><strong>(C) 2020 Frame Hub Middle East FZ LLC. </strong><br/><br/></td>
                	</tr>
                	</table>
                	</center>
                	</td>
                	</tr>
                	</table>
                	</body>
                	</html>';  

                	Mail::send([], [], function ($message) use ($inData) {
                		$message->from($inData['from_email'],'FRAMEiT');
                		$message->to($inData['email']);
                		$message->subject('FRAMEiT - Verification');
                		$message->setBody($inData['body'], 'text/html');
                	});

                }

                session::flash('message', 'Account has been created successfully. Please check your email address and get your account details!');
                return redirect('admin/user_list');

            } else {

            	session::flash('error', 'OOPs! Some internal issue occured.');
            	return redirect('admin/add_user');
            }
        } 

    }


    public function edit_user(Request $request){
    	$user_id = base64_decode($request->id);
    	$data['user_info'] = User::find($user_id);

    	return view('admin/users/edit_user')->with($data) ;
    }

    public function update_user(Request $request){

    	$user_id = $request->input('user_id') ;
    	$email = $request->input('email') ;
    	$contact_number = $request->input('contact_number') ;

    	$userData = User::where('id', $user_id)->first();
    	
    	$userData->email = $email;
    	$userData->contact_number = $contact_number;
    	$userData->updated_at = date('Y-m-d H:i:s');
    	$res = $userData->save();

    	if($res){
    		session::flash('message', 'User updated succesfully.');
    		return redirect('admin/user_list');
    	}else{
    		session::flash('error', 'Somthing went wrong.');
    		return redirect('admin/user_list');
    	} 
    }

    public function user_profile(Request $request) {
    	$user_id = base64_decode($request->id);
    	$data['user_info'] = User::find($user_id);

    	$data['followers'] = DB::table('follower')->where('user_id','=',$user_id)->where('request_status','=',1)->count();

    	$data['following'] = DB::table('follower')->where('follower_id','=',$user_id)->where('request_status','=',1)->count();

    	return view('admin/users/user_profile')->with($data);
    }

    public function change_user_status(Request $request)
    {
    	$user = User::find($request->user_id);
    	$user->status = $request->status;
    	$user->save();

    	return response()->json(['success'=>'User status change successfully.']);
    }

    public function delete_user(Request $request) {
        $user_id = $request->user_id;

        $frame_info = DB::table('users')->where('id','=',$user_id)->first();

        $res = DB::table('users')->where('id', '=', $user_id)->delete();

        if ($res) {
            return json_encode(array('status' => 'success','msg' => 'User has been deleted successfully!'));
        } else {
            return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
        }

    }


    public function change_user_password(Request $request){

        $user_id = base64_decode($request->id);
        $data['user_info'] = User::find($user_id);
        return view('admin/users/change_user_password')->with($data);

    }

    public function update_user_password(Request $request) {
        
        $user_id = $request->user_id;
        $user_info = User::find($user_id);

        /*if (!(Hash::check($request->get('current-password'), $user_info->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }*/

        $validatedData = $request->validate([
            //'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user_info->password = bcrypt($request->get('new-password'));
        $user_info->save();

        return redirect()->back()->with("message","Password changed successfully !");
    }



    public function video_list(Request $request) {
    	$user_id = base64_decode($request->id);

    	$data['user_info'] = User::find($user_id);

    	/*$data['video_list'] = DB::table('post_media')->where('media_type','=','video')->get();*/

    	$data['video_list'] = DB::table('post_media')
    	->join('posts', 'post_media.post_id', '=', 'posts.post_id')
    	->where('posts.user_id','=',$user_id)
    	->where('post_media.media_type','=','video')
    	->select('post_media.*')
    	->get();

    	return view('admin/video_list')->with($data);
    }

    public function image_list(Request $request) {
    	$user_id = base64_decode($request->id);

    	$data['user_info'] = User::find($user_id);

    	/*$data['image_list'] = DB::table('post_media')->where('media_type','=','image')->get();*/

    	$data['image_list'] = DB::table('post_media')
    	->join('posts', 'post_media.post_id', '=', 'posts.post_id')
    	->where('posts.user_id','=',$user_id)
    	->where('post_media.media_type','=','image')
    	->select('post_media.*')
    	->get();

    	return view('admin/image_list')->with($data);
    }	

    public function delete_image(Request $request) {
    	$image_id = $request->image_id;

    	$image_info = DB::table('post_media')->where('id','=',$image_id)->first();
    	$media_url = $image_info->media_url;

    	$res = DB::table('post_media')->where('id', '=', $image_id)->delete();

    	if ($res) {
    		@unlink(url('/').'/public/uploads/feed_images/'.$media_url);
    		return json_encode(array('status' => 'succ','msg' => 'Image has been deleted successfully!'));
    	} else {
    		return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
    	}

    }

    public function delete_video(Request $request) {
    	$video_id = $request->video_id;

    	$image_info = DB::table('post_media')->where('id','=',$video_id)->first();
    	$media_url = $image_info->media_url;

    	$res = DB::table('post_media')->where('id', '=', $video_id)->delete();

    	if ($res) {
    		@unlink(url('/').'/public/uploads/feed_videos/'.$media_url);
    		return json_encode(array('status' => 'succ','msg' => 'Video has been deleted successfully!'));
    	} else {
    		return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
    	}

    }

}
?>