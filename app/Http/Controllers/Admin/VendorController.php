<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\CheckoutModel;
use App\User;
use App\Vendor;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Mail;

class VendorController extends Controller 
{
	public function user_list() {
		$data['user_list'] = Vendor::orderby('vendor_id','DESC')->get();
		return view('admin/vendor/user_list')->with($data);
	}

	public function add_user(){
		return view('admin/vendor/add_user'); 
	}


	public function submit_user(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'full_name' => 'required',
			'email' => 'required|unique:vendor,email',
			'password' => 'required',
			'confirm_password' => 'required|same:password',
			'contact_number' => 'required|numeric|unique:vendor,contact_number',
		]);
		if ($validator->fails()) {
			session::flash('error', 'Validation error.');
			return redirect('/admin/vendor/add_user')->withErrors($validator)->withInput(); 
		} else {
			$vrfn_code = Helper::generateRandomString(6);
			$name = $request->full_name;
			$email = $request->email;
			$password = $request->password;
			$contact_number = $request->contact_number;
			//$name_arr = explode(' ', $name);

			$obj = new Vendor;
			$obj->full_name = $name;
			$obj->email = $email;
			$obj->contact_number = $contact_number;
			$obj->password = bcrypt($password);
            $obj->address = $request->address;
            $obj->state = $request->state;
            $obj->city = $request->city;
            $obj->zipcode = $request->zipcode;
			$obj->is_verify_email = 1;
			$obj->vrfn_code = $vrfn_code;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$obj->updated_at = date('Y-m-d H:i:s');
			$res = $obj->save();

			if ($res) {

				$users = Vendor::where('email','=',$email)->first();

				// $my_referral_code = Helper::my_simple_crypt($users->id,'e');
				// $users->my_referral_code = $my_referral_code;
				// $users->save();

				if ($_SERVER['SERVER_NAME'] != 'localhost') {

					$vrfn_url = url('/').'/email_verification/'.$vrfn_code.'_'.$users->id;

					$fromEmail = Helper::getFromEmail();
					$inData['from_email'] = $fromEmail;
					$inData['email'] = $email;
	                /*$inData['body'] = "<table>
                	<thead>
                		<tr>
                			<td>name</td>
                			<td>Link</td>
                		</tr>
                	</thead>
                	<tbody>
                		<tr>
                			<td>".$name."</td>
                			<td><a href='".$vrfn_url."'>Verification Link</a></td>
                		</tr>
                	</tbody>
                	</table>";*/
                	$inData['body'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                	<html xmlns="http://www.w3.org/1999/xhtml">
                	<head>
                	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                	<meta name="viewport" content="width=device-width, initial-scale=1"/>
                	<title>Verify your FRAMEiT account</title>
                	<style type="text/css">img{max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}a img{border: none;}table{border-collapse: collapse !important;}#outlook a{padding:0;}.ReadMsgBody{width: 100%;}.ExternalClass{width: 100%;}.backgroundTable{margin: 0 auto; padding: 0; width: 100% !important;}table td{border-collapse: collapse;}.ExternalClass *{line-height: 115%;}.container-for-gmail-android{min-width: 600px;}/* General styling */ *{font-family: Helvetica, Arial, sans-serif;}body{-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; margin: 0 !important; height: 100%; color: #676767;}td{font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;}a{color: #676767; text-decoration: none !important;}.pull-left{text-align: left;}.pull-right{text-align: right;}.header-lg, .header-md, .header-sm{font-size: 32px; font-weight: 700; line-height: normal; padding: 35px 0 0; color: #4d4d4d;}.header-md{font-size: 24px;}.header-sm{padding: 5px 0; font-size: 18px; line-height: 1.3;}.content-padding{padding: 20px 0 30px;}.mobile-header-padding-right{width: 290px; text-align: right; padding-left: 10px;}.mobile-header-padding-left{width: 290px; text-align: left; padding-left: 10px;}.free-text{width: 100% !important; padding: 10px 60px 0px;}.block-rounded{border-radius: 5px; border: 1px solid #e5e5e5; vertical-align: top;}.button{padding: 55px 0 0;}.info-block{padding: 0 20px; width: 260px;}.mini-block-container{padding: 30px 50px; width: 500px;}.mini-block{background-color: #ffffff; width: 498px; border: 1px solid #cccccc; border-radius: 5px; padding: 60px 75px;}.block-rounded{width: 260px;}.info-img{width: 258px; border-radius: 5px 5px 0 0;}.force-width-img{width: 480px; height: 1px !important;}.force-width-full{width: 600px; height: 1px !important;}.user-img img{width: 82px; border-radius: 5px; border: 1px solid #cccccc;}.user-img{width: 92px; text-align: left;}.user-msg{width: 236px; font-size: 14px; text-align: left; font-style: italic;}.code-block{padding: 10px 0; border: 1px solid #cccccc; width: 20px; color: #4d4d4d; font-weight: bold; font-size: 18px;}.mini-img{padding: 5px; width: 140px;}.mini-img img{border-radius: 5px; width: 140px;}.mini-imgs{padding: 25px 0 30px;}.progress-bar{padding: 0 15px 0;}.step{vertical-align: top;}.step img{width: 109px; height: 78px;}.active{font-weight: bold;}</style>
                	<style type="text/css" media="screen"> @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700); </style>
                	<style type="text/css" media="screen"> @media screen{/* Thanks Outlook 2013! */ *{font-family: "Oxygen", "Helvetica Neue", "Arial", "sans-serif" !important;}}</style>
                	<style type="text/css" media="only screen and (max-width: 480px)"> /* Mobile styles */ @media only screen and (max-width: 480px){table[class*="container-for-gmail-android"]{min-width: 290px !important; width: 100% !important;}table[class="w320"]{width: 320px !important;}td[class*="mobile-header-padding-left"]{width: 160px !important;}img[class="force-width-gmail"]{display: none !important; width: 0 !important; height: 0 !important;}td[class="mobile-block"]{display: block !important;}td[class="mini-img"], td[class="mini-img"] img{width: 150px !important;}td[class*="mobile-header-padding-left"]{width: 160px !important; padding-left: 0 !important;}td[class*="mobile-header-padding-right"]{width: 160px !important; padding-right: 0 !important;}td[class="header-lg"]{font-size: 24px !important; padding-bottom: 5px !important;}td[class="header-md"]{font-size: 18px !important; padding-bottom: 5px !important;}td[class="content-padding"]{padding: 5px 0 30px !important;}td[class="button"]{padding: 5px !important;}td[class*="free-text"]{padding: 10px 18px 30px !important;}img[class="force-width-img"], img[class="force-width-full"]{display: none !important;}td[class="info-block"]{display: block !important; width: 280px !important; padding-bottom: 40px !important;}td[class="info-img"], img[class="info-img"]{width: 278px !important;}td[class="mini-block-container"]{padding: 8px 20px !important; width: 280px !important;}td[class="mini-block"]{padding: 20px 0 !important;}td[class*="step"] img{width: 86px !important; height: 62px !important;}td[class="progress-bar"]{padding: 0 11px 25px;}td[class="user-img"]{display: block !important; text-align: center !important; width: 100% !important; padding-bottom: 10px;}td[class="user-msg"]{display: block !important; padding-bottom: 20px;}}</style>
                	</head>
                	<body bgcolor="#f7f7f7">
                	<table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">
                	<tr>
                	<td align="left" valign="top" width="100%">
                	<center>
                	<table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" style="background-color:#333;background: #333;">
                	<tr>
                	<td width="100%" height="80" valign="top" style="text-align: center; vertical-align:middle;">
                	<center>
                	<table cellpadding="0" cellspacing="0" width="600" class="w320">
                	<tr>
                	<td class="pull-left mobile-header-padding-left" style="vertical-align: middle;"> <a href="'.url('/').'"><img width="137" height="47" src="'.url("/").'/resources/front_assets/img/logo.png" alt="logo"></a> </td>
                	</tr>
                	</table>
                	</center>
                	</td>
                	</tr>
                	</table>
                	</center>
                	</td>
                	</tr>
                	<tr>
                	<td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
                	<center>
                	<table cellspacing="0" cellpadding="0" width="600" class="w320">
                	<tr>
                	<td class="header-lg"> Almost done! </td>
                	</tr>
                	<tr>
                	<td class="free-text"> Your Account is created successfully. Your account details </td>
                	</tr>
                	<tr>
                	<td class="mini-block-container">
                	<table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                	<tr>
                	<td class="mini-block">
                	<table cellpadding="0" cellspacing="0" width="100%">
                	<tr>
                	<td class="progress-bar">
                	<table cellpadding="0" cellspacing="0" width="100%"></table>
                	</td>
                	</tr>
                	<tr>
                	<td class="button">
                	<div>
                        <p><b>Username</b> :  $email</p> 
                        <p><b>Password</b> :  $password</p> 
                	</div>
                	</td>
                	</tr>
                	</table>
                	</td>
                	</tr>
                	</table>
                	</td>
                	</tr>
                	</table>
                	</center>
                	</td>
                	</tr>
                	<tr>
                	<td align="center" valign="top" width="100%" style="background-color: #f7f7f7; height: 100px;">
                	<center>
                	<table cellspacing="0" cellpadding="0" width="600" class="w320">
                	<tr>
                	<td style="padding: 25px 0 25px">04-451-1555<br/>In5 Inovation Centre, Dubai Media City <br/>info@frameit.com <br/><strong>(C) 2020 Frame Hub Middle East FZ LLC. </strong><br/><br/></td>
                	</tr>
                	</table>
                	</center>
                	</td>
                	</tr>
                	</table>
                	</body>
                	</html>';  

                	Mail::send([], [], function ($message) use ($inData) {
                		$message->from($inData['from_email'],'FRAMEiT');
                		$message->to($inData['email']);
                		$message->subject('FRAMEiT - Verification');
                		$message->setBody($inData['body'], 'text/html');
                	});

                }

                session::flash('message', 'Account has been created successfully. Please check your email address and get your account details!');
                return redirect('admin/vendor/user_list');

            } else {

            	session::flash('error', 'OOPs! Some internal issue occured.');
            	return redirect('admin/vendor/add_user');
            }
        } 

    }


    public function edit_user(Request $request){
    	$user_id = base64_decode($request->id);
    	$data['user_info'] = Vendor::where('vendor_id', $user_id)->first();

    	return view('admin/vendor/edit_user')->with($data) ;
    }

    public function update_user(Request $request){

    	$user_id = $request->input('user_id') ;
    	$email = $request->input('email') ;
    	$contact_number = $request->input('contact_number') ;

    	$userData = Vendor::where('vendor_id', $user_id)->first();
    
        $userData->full_name = $request->full_name;
        $userData->email = $email;
        $userData->contact_number = $contact_number;
        $userData->address = $request->address;
        $userData->state = $request->state;
        $userData->city = $request->city;
        $userData->zipcode = $request->zipcode;
        $userData->updated_at = date('Y-m-d H:i:s');

    	$res = $userData->save();

    	if($res){
    		session::flash('message', 'User updated succesfully.');
    		return redirect('admin/vendor/user_list');
    	}else{
    		session::flash('error', 'Somthing went wrong.');
    		return redirect('admin/vendor/user_list');
    	} 
    }

    public function user_profile(Request $request) {
    	$user_id = base64_decode($request->id);
    	$data['user_info'] = Vendor::find($user_id);

    	$data['followers'] = DB::table('follower')->where('user_id','=',$user_id)->where('request_status','=',1)->count();

    	$data['following'] = DB::table('follower')->where('follower_id','=',$user_id)->where('request_status','=',1)->count();

    	return view('admin/vendor/user_profile')->with($data);
    }

    public function change_user_status(Request $request)
    {
    	$user = Vendor::find($request->user_id);
    	$user->status = $request->status;
    	$user->save();

    	return response()->json(['success'=>'User status change successfully.']);
    }

    public function delete_user(Request $request) {
        $user_id = $request->user_id;

        $frame_info = DB::table('vendor')->where('vendor_id','=',$user_id)->first();

        $res = DB::table('vendor')->where('vendor_id', '=', $user_id)->delete();

        if ($res) {
            return json_encode(array('status' => 'success','msg' => 'User has been deleted successfully!'));
        } else {
            return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
        }

    }


    public function change_user_password(Request $request){

        $user_id = base64_decode($request->id);
        $data['user_info'] = Vendor::where('vendor_id','=',$user_id)->first();
        return view('admin/vendor/change_user_password')->with($data);

    }

    public function update_user_password(Request $request) {
        
        $user_id = $request->user_id;
        $user_info = Vendor::find($user_id);

        /*if (!(Hash::check($request->get('current-password'), $user_info->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }*/

        $validatedData = $request->validate([
            //'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user_info->password = bcrypt($request->get('new-password'));
        $user_info->save();

        return redirect()->back()->with("message","Password changed successfully !");
    }



    public function video_list(Request $request) {
    	$user_id = base64_decode($request->id);

    	$data['user_info'] = User::find($user_id);

    	/*$data['video_list'] = DB::table('post_media')->where('media_type','=','video')->get();*/

    	$data['video_list'] = DB::table('post_media')
    	->join('posts', 'post_media.post_id', '=', 'posts.post_id')
    	->where('posts.user_id','=',$user_id)
    	->where('post_media.media_type','=','video')
    	->select('post_media.*')
    	->get();

    	return view('admin/video_list')->with($data);
    }

    public function image_list(Request $request) {
    	$user_id = base64_decode($request->id);

    	$data['user_info'] = User::find($user_id);

    	/*$data['image_list'] = DB::table('post_media')->where('media_type','=','image')->get();*/

    	$data['image_list'] = DB::table('post_media')
    	->join('posts', 'post_media.post_id', '=', 'posts.post_id')
    	->where('posts.user_id','=',$user_id)
    	->where('post_media.media_type','=','image')
    	->select('post_media.*')
    	->get();

    	return view('admin/image_list')->with($data);
    }	

    public function delete_image(Request $request) {
    	$image_id = $request->image_id;

    	$image_info = DB::table('post_media')->where('id','=',$image_id)->first();
    	$media_url = $image_info->media_url;

    	$res = DB::table('post_media')->where('id', '=', $image_id)->delete();

    	if ($res) {
    		@unlink(url('/').'/public/uploads/feed_images/'.$media_url);
    		return json_encode(array('status' => 'succ','msg' => 'Image has been deleted successfully!'));
    	} else {
    		return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
    	}

    }

    public function delete_video(Request $request) {
    	$video_id = $request->video_id;

    	$image_info = DB::table('post_media')->where('id','=',$video_id)->first();
    	$media_url = $image_info->media_url;

    	$res = DB::table('post_media')->where('id', '=', $video_id)->delete();

    	if ($res) {
    		@unlink(url('/').'/public/uploads/feed_videos/'.$media_url);
    		return json_encode(array('status' => 'succ','msg' => 'Video has been deleted successfully!'));
    	} else {
    		return json_encode(array('status' => 'error','msg' => 'Some internal issue occured.'));
    	}

    }

    public function vendorlogin() {

        return view('vendor/login');

    }

    public function submit_login(Request $request) {

        $request->email;

        $request->password;



        $validator = Validator::make($request->all(), [

            'email' => 'required',

            'password' => 'required|min:6'

        ]);

        if ($validator->fails()) {

            return redirect('/login')

            ->withErrors($validator)

            ->withInput();

        } else {



            $inputVal = $request->all(); 



            $credentials = array(

                'email' => $inputVal['email'],

                'password' => $inputVal['password'],

                'status' => 1

            ); 

            

            
            if (Auth::guard('vendor')->attempt(['email' => $request->email, 'password' => $request->password])) {
                        $details = Auth::guard('vendor')->user();
                        $user = $details['original'];
                        return redirect('/vendor/dashboard');
                        //echo "ghhhhgh";
                       // return $user;
            }else{ 

                $user = DB::table('users')->where('email',$inputVal['email'])->first();

                if(!empty($user)){

                    //if ($user->user_status != 1) {
                    if ($user->status != 1) {

                        Auth::logout();

                        session::flash('error', 'Your acount is inactive.');

                        return redirect('/admin_login');

                    } else {  

                        Auth::logout();

                        session::flash('error', 'Email or Password is incorrect.' );

                        return redirect('/admin_login');

                        

                    } 

                }else{  

                    session::flash('error', 'Your account does not exist.' );

                    return redirect('/admin_login');

                }  

            

        }

    }

}

public function dashboard() {

        $id = Auth::guard('vendor')->user()->vendor_id;

        if(!empty($id)){
                  $data['orders'] = DB::table('orders')->where('vendor_id', $id)->get();

                    $data['users'] = DB::table('users')->where('status',1)->get();

                    $data['visitors'] = DB::table('users')->where('status',1)->get();

                    return view('vendor/dashboard')->with($data);
        }else{
                return redirect('/vendorlogin');
        }
    }

    public function logout(){

        Auth::logout();

        return redirect('/vendorlogin');

    }

    public function order_list() {

         $id = Auth::guard('vendor')->user()->vendor_id;

         //print_r($id);

        $data['order_list'] =  DB::table('orders')

        ->select('frames.title','frames.images','frames.currency','users.first_name', 'users.last_name', 'orders.id', 'orders.total_tile', 'orders.total_price', 

            'orders.shipping_price', 'orders.coupon_discount', 'orders.wallet_discount', 'orders.address_id','orders.frame_id', 'orders.order_status', 'orders.delivery_date', 'orders.status', 'orders.vendor_id', 'orders.created_at')

        ->join('users','users.id','=','orders.user_id')

        ->join('frames','frames.id','=','orders.frame_id')

        ->where('orders.vendor_id',$id)

        ->where('orders.status', 1)

        ->where('users.status', 1)

        ->orderBy('orders.id', 'DESC')

        ->get();

        return view('vendor/order_list')->with($data);

    }



    public function view_order_details(Request $request){

        $order_id = base64_decode($request->id);

        $data['order_info'] =  DB::table('orders')

        ->select('frames.images', 'frames.type' ,'frames.currency','users.first_name', 'users.last_name', 'orders.id', 'orders.total_price', 'orders.shipping_price', 'shipping_price.currency as shipping_currency', 'orders.address_id', 'users.email', 'users.contact_number as contact', 'users.address',

            'orders.order_status', 'orders.delivery_date', 'orders.created_at', 'shipping_address.fullname','shipping_address.street_address','shipping_address.city', 'orders.vendor_id',

            'shipping_address.state','countries.name as country','shipping_address.zip_code','shipping_address.contact_number')

        ->join('users','users.id','=','orders.user_id')

        ->join('frames','frames.id','=','orders.frame_id')

        ->join('shipping_address','shipping_address.id','=','orders.address_id')

        ->join('countries','countries.id','=','shipping_address.country')

        ->join('shipping_price','shipping_price.country_id','=','shipping_address.country')

        ->where('orders.id', $order_id )

        ->where('users.status', 1)

        ->first();



        $data['order_details'] =  DB::table('order_detail')

        ->select('frames.images', 'frames.price','frames.currency', 'order_detail.order_id', 'order_detail.tile_image', 'order_detail.rotation', 'order_detail.quantity',  'orders.address_id', 

          'orders.frame_id', 'orders.shipping_price', 'shipping_price.currency as shipping_currency', 'orders.order_status', 

            'orders.total_price', 'orders.coupon_discount', 'orders.wallet_discount' , 'orders.delivery_date')

        ->join('orders','orders.id','=','order_detail.order_id')

        ->join('frames','frames.id','=','orders.frame_id')

        ->join('shipping_address','shipping_address.id','=','orders.address_id')

        ->join('shipping_price','shipping_price.country_id','=','shipping_address.country')

        ->where('order_detail.order_id',$order_id)

        ->get();


        $data['vendor'] =  DB::table('vendor')

        ->where('status',1)

        ->get();



        return view('vendor/view_order_details')->with($data) ;

    }


    public function change_order_status(Request $request)

    {

        $order_info = CheckoutModel::find($request->order_id);

        $date = $order_info->created_at;

        $order_info->order_status = $request->status;

        $res = $order_info->save();



        if ($res) {



            $users = DB::table('users')->where('id','=',$order_info->user_id)->first(); 



            $username = $users->first_name." ".$users->last_name;



            $title ="Hey ". $username ." your order status is ". $request->status;



            //$device_token ="ftkpkxjW37Q:APA91bE_27LnEi3ziGsQpVbZnvh6MlNxovsEVVQSGPDMlnTYt47IiQP25JuSwC0AqiGeGwHa3LAFZ9mw8IKmRkMD1lzKRSeMEBPNCpvsIkKA4AcNAieLbQ-OFlXq74jiCUo2xfPy2fi8";

            

            $this->notification($users->device_token, $title, $request->order_id, $date);

          

                      

            if ($_SERVER['SERVER_NAME'] != 'localhost') {

                $data['url'] = url('/');

                $data['status'] = $request->status;

                $data['first_name'] = $users->first_name;

                $data['last_name'] = $users->last_name;

                $data['status'] = $request->status;

                $data['order_id'] = $request->order_id;





                $fromEmail = Helper::getFromEmail();

                $inData['from_email'] = $fromEmail;

                $inData['email'] = $users->email;

                Mail::send('emails.invoice.order_status_template',$data, function ($message) use ($inData) {

                    $message->from($inData['from_email'],'FRAMEiT');

                    $message->to($inData['email']);

                    $message->subject('FRAMEiT - Order Status Mail');

                });



            }



            return response()->json(['success'=>'Order status change successfully.']);



        }   

    }

     public function notification($token, $title, $order_id, $date)

    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $token=$token;



        $notification = [

            'id' => $order_id,

            'title' => 'FRAMEiT',

            'body' => $title,

            'tag' =>'order_status',

            'date' =>$date,

            'sound' => true,

        ];

        

        $extraNotificationData = $notification;



        $fcmNotification = [

            //'registration_ids' => $tokenList, //multple token array

            'to'        => $token, //single token

            'notification' => $notification,

            'data' => $extraNotificationData

        ];



        $headers = [

            'Authorization: key=AAAASk5-u2g:APA91bHQE__pE-TXQszZb-TorN-8v9dbStYqnp_vIfVWitAXGrnfuVr4YZInoK53FZPEPIj6ej3izSvMwNkbWEb54dcrflqMMwAyjtnjpU1TwS4CyxP_9iHQRAYDlC25yI32Vhs6-4sZ',

            'Content-Type: application/json'

        ];





        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$fcmUrl);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));

        $result = curl_exec($ch);

        curl_close($ch);



        return true;

    }

}
