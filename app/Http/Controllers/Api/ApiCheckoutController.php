<?php 
namespace App\Http\Controllers\Api;
 
use Illuminate\Http\Request; 
 
use App\Http\Controllers\Controller; 
 
use App\CheckoutModel, App\User, App\Group_request, Hash; 
 
use Illuminate\Support\Facades\Auth; 
 
use Validator, DB;
 
use Illuminate\Validation\Rule;
 
use App\Helpers\Helper;
 
use Session;
 
use Illuminate\Routing\UrlGenerator;
 
use Illuminate\Database\QueryException;
 
use Mail;
 
class ApiCheckoutController extends Controller 
{ 
 
    public $successStatus = true;
 
    public $failureStatus = false;
 
    /**  
    * create feed api  
    *  
    * @return \Illuminate\Http\Response  
    */ 
 
    public function createOrder(Request $request){ 

        $forminput =  $request->all(); 

        $validator = Validator::make($request->all(), [  

            'user_id'  => 'required', 

            'address_id'  => 'required', 

            'frame_id'  => 'required', 

            'total_tile'  => 'required', 

            'total_price'  => 'required',

            //'tile_image.*' => 'mimes:jpeg,png,jpg,gif,svg|max:5000' 

            ],

            [   

                'user_id.required'     => 'required user_id',

                'address_id.required'     => 'required address_id',

                'frame_id.required'     => 'required frame_id', 

                'total_tile.required'     => 'required total_tile',

                'total_price.required'     => 'required total_price', 
            ] 

        );

        if ($validator->fails()) { 

            $messages = $validator->messages();

            foreach ($messages->all() as $message) 
            {    

                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);             
            }             

        }

        // $ref_id = !empty($forminput['ref_id'])? $forminput['ref_id']:'';

        // $payment_status = !empty($forminput['payment_status'])? $forminput['payment_status']:'';

        $wallet_amount = !empty($forminput['wallet_amount'])? $forminput['wallet_amount']:'';

        $total_amt = $forminput['total_price'];

        $coupon_code = !empty($forminput['coupon_code'])? $forminput['coupon_code']:'';

        $country_info = DB::table('shipping_address')->where('id',$forminput['address_id'])->first();

        $country_name = $country_info->country;

        $day_info = DB::table('shipping_price')->where('country_id',$country_name)->first();

        $ship_price = $day_info->price;

        $delivery_days = $day_info->delivery_days;

        $current_date = date('Y-m-d');

        $delivery_date = date('Y-m-d',strtotime($current_date.'+ ' .$delivery_days. 'days')); 

        $wallet_discount = 0;

        if(!empty($wallet_amount)){

            $wallet_disc  = $total_amt + $ship_price - $wallet_amount;

            $wallet_discount =  $total_amt + $ship_price - $wallet_disc;             

            $wallet_info = DB::table('users')->where('id', $forminput['user_id'])->where('status',1)->first(); 

            $wallet_bal = $wallet_info->wallet_balance;

            $updated_balance = $wallet_bal - $wallet_amount;

            $update_wallet_amount = DB::table('users')->where( 'id', $forminput['user_id'] )->update( ['wallet_balance' =>$updated_balance] ); 

        }

        $coupon_discount = 0;

        if(!empty($coupon_code)){

            $coupon_info = DB::table('coupon')->where('code',$coupon_code)->where('status',1)->first();

            $type = $coupon_info->type; 

            $coupon_value = $coupon_info->value;

            if($type == 'percentage'){

                $disc_price = ($total_amt * $coupon_value)/100;

                $coupon_disc = $total_amt - $disc_price ; 

                $coupon_discount = $total_amt - $coupon_disc; 

            }else{

                $coupon_disc = $total_amt - $coupon_value ;

                $coupon_discount = $total_amt - $coupon_disc; 

            } 

        }


        if(!empty($coupon_discount && $wallet_discount)){

            $total_price = $total_amt + $ship_price - $coupon_discount - $wallet_discount;

        }elseif(!empty($coupon_discount)){ 

            $total_price = $total_amt + $ship_price - $coupon_discount; 

        }elseif(!empty($wallet_discount)){ 

            $total_price = $total_amt + $ship_price - $wallet_discount;

        }else{ 

            $total_price = $total_amt + $ship_price; 

        }


        if(!empty($forminput['ref_id'])){
            $ref_id = $forminput['ref_id'];
        }else{
            $ref_id = "";
        }
        if(!empty($forminput['paymethod'])){
            $paymethod = $forminput['paymethod'];
        }else{
            $paymethod = "";
        }
        if(!empty($forminput['card_numnber'])){
            $card_numnber = $forminput['card_numnber'];
        }else{
            $card_numnber = "";
        }
        if(!empty($forminput['gettransactid'])){
            $gettransactid = $forminput['gettransactid'];
        }else{
            $gettransactid = "";
        }
        if(!empty($forminput['payment_status'])){
            $payment_status = 'completed';
        }else{
            $payment_status = "";
        }
         
        DB::table('orders')->insert( 

            [ 

              'user_id' =>  $forminput['user_id'], 

              'address_id' =>  $forminput['address_id'],

              'frame_id' =>  $forminput['frame_id'],

              'total_tile' =>  $forminput['total_tile'],

              'total_price' =>  $total_price,

              'coupon_discount' =>  $coupon_discount, 

              'wallet_discount' =>  $wallet_discount,

              'shipping_price' =>  $ship_price, 

              'coupon_code' => !empty($forminput['coupon_code'])? $forminput['coupon_code']:'',

              'is_applied_coupon' => !empty($forminput['coupon_code'])? '1':'0',

              'delivery_date' =>  $delivery_date,

              'ref_id'   => $ref_id,

              'payment_status'=>$payment_status,

              'order_status' =>  'pending', 

              'paymethod' => $paymethod,

              'card_numnber' => $card_numnber,

              'gettransactid' => $gettransactid,

              'vendor_id' => 0, 

              'created_at' =>  date('Y-m-d H:i:s'), 

              'status' => 1, 
            ]

        );

        $order_id = DB::getpdo()->lastInsertId();

        ///priyanka code

        $today = date("Ymd");

        $unique = '#'.$today . $order_id;

        $orderidupdate = DB::table('orders')->where('id', $order_id)->update(['order_id' => $unique]); 
        if($forminput['wishlist_id']==0){ 

        }else{

            $wishlist_id =$forminput['wishlist_id'];

            DB::table('wishlists')->where('id', $wishlist_id)->delete();

            DB::table('wishlist_detail')->where('wishlist_id', $wishlist_id)->delete();

        }

         if(!empty($wallet_amount) && $wallet_amount>0){
             DB::table('wallettransaction')->insert(
            [
              'user_id' =>  $forminput['user_id'],
              'order_id' =>  $order_id,
              'credit_amount' =>  $updated_balance,
              'debit_amount' =>  $wallet_amount,
              'status' => 1,
              'created_at' =>  date('Y-m-d H:i:s'),

            ]
        );

        }

        $referral_info = DB::table('my_referrals')->where('user_id', $forminput['user_id'])->where('status',1)->first();


        if(!empty($referral_info)){

            $accepted_id  = $referral_info->refferal_id;

            $check_orders = DB::table('orders')->where('user_id', $forminput['user_id'])->get();

            $order_total = 0;

            foreach ($check_orders as $amt) {

                $order_total += $amt->total_price; 

            }

            if(!empty($check_orders)){

                    $order_count = count($check_orders); 

                    $comm = DB::table('referral_commission_settings')->where('status',1)->first();

                    $no_of_orders = $comm->no_of_orders;

                    $order_amt =   $comm->order_amount;

                    $ref_amt =  $comm->referral_amount;

                if(($no_of_orders <= $order_count) && ($order_amt <= $order_total)) {


                    $wallet_info = DB::table('users')->where('id', $referral_info->refferal_id)->where('status',1)->first();

                    $ref_amt1 = ($ref_amt / 100) * $order_total;

                    $wallet_balance = $wallet_info->wallet_balance + $ref_amt1;


                    $updateData = DB::table('users')->where('id',$referral_info->refferal_id )->update(['wallet_balance' =>$wallet_balance]);


                    $statusData = DB::table('my_referrals')->where('user_id',$forminput['user_id'] )->update(['status' =>0]);

                        DB::table('wallettransaction')->insert(
                            [
                                'user_id' =>  $referral_info->refferal_id,
                                'order_id' =>  $order_id,
                                'credit_amount' =>  $ref_amt1,
                                'debit_amount' =>  0,
                                'status' => 1,
                                'created_at' =>  date('Y-m-d H:i:s'),
                            ]
                        );  
                }
            }
        } 

        //end 

        if(!empty($order_id)){

            $check_qty = DB::table('frames')->where('id',$forminput['frame_id'])->first();

            $frame_qty = $check_qty->quantity;

            $order_qty = $forminput['total_tile']; 

            $updated_qty = $frame_qty - $order_qty; 

            $updateData = DB::table('frames')->where('id',$forminput['frame_id'] )->update(['quantity' => $updated_qty]);
            $tracking_code = Helper::generateRandomString(6);

            DB::table('order_tracking')->insert( [
                'user_id' =>  $forminput['user_id'],

                'order_id' =>  $order_id, 

                'tracking_code' =>  $tracking_code, 

                'tracking_status' =>  1 ,

                'created_at' =>  date('Y-m-d H:i:s')

                ] 

            ); 
        } 

        $imagename = array();

        $quantity = array(); 

        $filename = $request->image_data;

        //print_r($filename); die;

        $check = true;

        if($filename= $request->image_data){

            foreach ($filename as $key => $value) {

                $name = time().'_'.$value['imagename'];

                $qty = $value['quantity'];

                $rotation = $value['rotation'];

                $imagename[]=$name;

                $quantity[]=$qty;

                $rot[]=$rotation;

                $data[] = [

                    'order_id'=>$order_id, 'user_id'=> $request->user_id,

                    'tile_image' => $name, 'quantity' => $qty,

                    'rotation'=>$rotation,

                    'created_at' =>  date('Y-m-d H:i:s')
                ];
            }

            $check = DB::table('order_detail')->insert($data);

        }

        $media_json = array();

        $files=$request->file('tile_image');

        if($files=$request->file('tile_image')){

            foreach($files as $file){

                $name = time().'_'.$file->getClientOriginalName();

                $destinationPath = public_path('/uploads/order_images');

                $imagePath = $destinationPath. '/'.  $name;

                $jsonPath = url('/public/uploads/order_images'). '/'.  $name;

                $file->move( $destinationPath,$imagePath );

            }

        }

        if( $check ){

            $users = User::where('id','=',$forminput['user_id'])->first();

            $data['user_info'] = $users;

            $data['url'] = url('/');

            $data['order_info'] =  DB::table('orders')

            ->select('frames.images','frames.currency','users.first_name', 'users.last_name', 'orders.id', 'orders.total_tile', 'orders.total_price', 
                'orders.shipping_price', 'orders.coupon_discount', 'orders.wallet_discount', 'orders.address_id','orders.frame_id', 'orders.order_status', 'orders.delivery_date', 'orders.status', 'orders.created_at','.shipping_address.fullname','shipping_address.street_address','shipping_address.city','shipping_address.state','countries.name as country','shipping_address.zip_code','shipping_address.contact_number')

            ->join('users','users.id','=','orders.user_id')
            
            ->join('frames','frames.id','=','orders.frame_id')

            ->join('shipping_address','shipping_address.id','=','orders.address_id')

            ->join('countries','countries.id','=','shipping_address.country')

            ->where('orders.id', $order_id)

            ->where('users.status', 1)

            ->first();

            $data['order_details'] =  DB::table('order_detail')

            ->select('frames.title','frames.images','frames.price','frames.currency', 'order_detail.order_id', 'order_detail.tile_image', 'order_detail.quantity',  'orders.address_id', 
              'orders.frame_id', 'orders.shipping_price', 'shipping_price.currency as shipping_currency', 'orders.order_status', 

                'orders.total_price', 'orders.coupon_discount', 'orders.wallet_discount' , 'orders.delivery_date')

            ->join('orders','orders.id','=','order_detail.order_id')

            ->join('frames','frames.id','=','orders.frame_id')

            ->join('shipping_address','shipping_address.id','=','orders.address_id')

            ->join('shipping_price','shipping_price.country_id','=','shipping_address.country')

            ->where('order_detail.order_id',$order_id)

            ->get();

            if ($_SERVER['SERVER_NAME'] != 'localhost') {

                $fromEmail = Helper::getFromEmail();

                $inData['from_email'] = $fromEmail;

                $inData['email'] = $users->email;

                Mail::send('emails.invoice.invoice_template', $data, function ($message) use ($inData) {

                    $message->from($inData['from_email'],'FRAMEiT');

                    $message->to($inData['email']);

                    $message->subject('FRAMEiT - Order Confirmation Mail');

                });

            }

            $today = date("Ymd");

            $unique = '#'.$today . $order_id;

            return response()->json(['status'=>$this->successStatus, 'msg' => 'Order created successfully', 'response'=>['user_id' => $request->user_id, 'order_id' => $order_id ,'customer_order_id'=>$unique ]]);
        }else{

            return response()->json(['status'=>$this->failureStatus, 'msg' => 'Something went wrong']); 
        }

    }

    public function createWishlist(Request $request){
        $forminput =  $request->all();
        //print_r($forminput);die;
        $validator = Validator::make($request->all(), [ 
            'user_id'  => 'required',
            'address_id'  => 'required',
            'frame_id'  => 'required',
            'total_tile'  => 'required',
            'total_price'  => 'required',
            //'tile_image.*' => 'mimes:jpeg,png,jpg,gif,svg|max:5000'
            ],

            [   
                'user_id.required'     => 'required user_id',
                'address_id.required'     => 'required address_id',
                'frame_id.required'     => 'required frame_id',
                'total_tile.required'     => 'required total_tile',
                'total_price.required'     => 'required total_price',
            ]
        );
        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);
            }            
        }

        $user_id = !empty($forminput['user_id'])? $forminput['user_id']:'';
        $wishlist_id = !empty($forminput['wishlist_id'])? $forminput['wishlist_id']:'';
        if(!empty($wishlist_id) && !empty($user_id)){
            $res = DB::table('wishlists')->where('id', '=', $wishlist_id)->where('user_id', '=', $user_id)->delete();

            if ($res) {
                $rest = DB::table('wishlist_detail')->where('wishlist_id', '=', $wishlist_id)->delete();
            }
        }

        $total_amt=$forminput['total_price'];
        $coupon_code=$forminput['coupon_discount'];
        $coupon_discount = 0;
        if(!empty($coupon_code)){
            $coupon_info = DB::table('coupon')->where('code',$coupon_code)->where('status',1)->first();
            $type = $coupon_info->type;
            $coupon_value = $coupon_info->value;
            if($type == 'percentage'){
                $disc_price = ($total_amt * $coupon_value)/100;
                $coupon_disc = $total_amt - $disc_price ;
                $coupon_discount = $total_amt - $coupon_disc;

            }else{
                $coupon_disc = $total_amt - $coupon_value ;
                $coupon_discount = $total_amt - $coupon_disc;
            }

        }

        $country_info = DB::table('shipping_address')->where('id',$forminput['address_id'])->first();
        $country_name = $country_info->country;
        $day_info = DB::table('shipping_price')->where('country_id',$country_name)->first();
        $ship_price = $day_info->price;
        if(!empty($ship_price)){

                $ship_price=$ship_price;

        }else{

            $ship_price=0;

        }
        $wallet_amount = $forminput['wallet_amount'];
        $wallet_discount = 0;
        if(!empty($wallet_amount)){

            $wallet_disc  = $total_amt + $ship_price - $wallet_amount;

            $wallet_discount =  $total_amt + $ship_price - $wallet_disc;             

        }

        DB::table('wishlists')->insert(
            [

              'user_id' =>  $forminput['user_id'],
              'address_id' =>  $forminput['address_id'],
              'frame_id' =>  $forminput['frame_id'],
              'total_tile' =>  $forminput['total_tile'],
              'total_price' =>  $forminput['total_price'],
              'coupon_discount' =>  number_format((float)$coupon_discount, 2),
              'wallet_discount' =>  number_format((float)$wallet_discount, 2),
              'shipping_price' =>  $ship_price,
              'coupon_code' => !empty($coupon_code)? $coupon_code:'',
              'is_applied_coupon' => !empty($coupon_code)? '1':'0',
              'created_at' =>  date('Y-m-d H:i:s'),
              'status' => 1,
            ]
        );
        $order_id = DB::getpdo()->lastInsertId();
        $imagename = array();
        $quantity = array();
        $filename = $request->image_data;
        //print_r($filename); die;
        $check = true;
        if($filename= $request->image_data){
            foreach ($filename as $key => $value) {
                $name = time().'_'.$value['imagename'];
                $qty = $value['quantity'];
                $rotate = $value['rotation'];
                $imagename[]=$name;
                $quantity[]=$qty;
                $rotation[]=$rotate;
                $data[] = [
                    'wishlist_id'=>$order_id, 'user_id'=> $request->user_id, 
                    'tile_image' => $name, 'quantity' => $qty, 'status' => 1,
                    'rotation'=>$rotate,
                    'created_at' =>  date('Y-m-d H:i:s')
                ];
            }
            $check = DB::table('wishlist_detail')->insert($data);
        }
        $media_json = array();
        $files=$request->file('tile_image');
        if($files=$request->file('tile_image')){
            foreach($files as $file){
                $name = time().'_'.$file->getClientOriginalName();
                $destinationPath = public_path('/uploads/order_images');
                $imagePath = $destinationPath. '/'.  $name;
                $jsonPath = url('/public/uploads/order_images'). '/'.  $name;
                $file->move( $destinationPath,$imagePath );
            }
        }
        if( $check ){
            return response()->json(['status'=>$this->successStatus, 'msg' => 'Wishlist created successfully', 'response'=>['user_id' => $request->user_id, 'wishlist_id' => $order_id  ]]);
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'Something went wrong']); 
        }
    }
 
    public function getAllUserWishList(Request $request){
        $forminput =  $request->all();
            $validator = Validator::make($request->all(), [ 
                'user_id'  => 'required',
            ],
            [   
                'user_id.required' => 'required user_id',
            ]
        );

        //$orderList = DB::table('orders')->where('user_id', $forminput['user_id'])->get();
        $wishList =  DB::table('wishlists')
        ->select('frames.title','wishlists.id','wishlists.user_id', 'wishlists.frame_id', 'wishlists.total_tile',  'wishlists.total_price', 'wishlists.coupon_discount','wishlists.wallet_discount','wishlists.shipping_price', 'wishlists.created_at')
        ->join('frames','frames.id','=','wishlists.frame_id')
        ->where('wishlists.user_id', $forminput['user_id'])
        ->get();

        $result = array();
        $results = array();
        foreach( $wishList as $key => $order ){
            $result['wishlist_id'] =  $order->id;
            $result['user_id'] =  $order->user_id;
            $result['frame_id'] =  $order->frame_id;
            $result['frame_name'] =  $order->title;
            $result['total_tile'] =  $order->total_tile;
            $result['total_price'] =  $order->total_price;
            $result['shipping_price'] =  $order->shipping_price;
            $result['coupon_discount'] =  number_format((float)$order->coupon_discount, 2);
            $result['wallet_discount'] =  number_format((float)$order->wallet_discount, 2);
            $result['created_at'] = $order->created_at ;
            $results[] =  $result;
        }
        if( sizeof($wishList) ){
            return response()->json(['status'=>$this->successStatus, 
                'msg' => 'Wish list successfully',
                'response'=> ['wishList' => $results]
            ]);
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No order found']); 
        }

    }
 
    public function getUserWishlistDetails(Request $request){
        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
            'wishlist_id'  => 'required',
        ],
        [   
            'wishlist_id.required' => 'required wishlist_id',
        ]
        );
        $orderList =  DB::table('wishlist_detail')
        ->select('frames.price', 'frames.images','frames.second_image','frames.type' ,'wishlist_detail.id','wishlist_detail.wishlist_id', 'wishlist_detail.user_id', 'wishlist_detail.rotation', 'wishlist_detail.tile_image', 'wishlist_detail.quantity', 'wishlists.total_tile',  'wishlists.total_price', 'wishlists.coupon_discount', 'wishlists.wallet_discount', 'wishlists.shipping_price', 'wishlists.frame_id','wishlists.address_id','wishlists.coupon_code', 'wishlists.created_at')
        ->join('wishlists','wishlists.id','=','wishlist_detail.wishlist_id')
        ->join('frames','frames.id','=','wishlists.frame_id')
        ->where('wishlist_detail.wishlist_id', $forminput['wishlist_id'])
        ->get();
        $result = array();
        $results = array();
        foreach( $orderList as $key => $order ){
            if(!empty($order->rotation)){
                $rotation=$order->rotation;
            }else{
                $rotation=0;
            }
            $total = (($order->total_price)+($order->shipping_price)) + number_format((float)$order->coupon_discount, 2) + number_format((float)$order->wallet_discount, 2);
            $frame_images_path = url('/public/uploads/frame_images/');
            $tile_images_path = url('/public/uploads/order_images/');
            $result['wishlist_id'] =  $order->wishlist_id;
            $result['user_id'] =  $order->user_id;
            $result['address_id'] =  $order->address_id;
            $result['frame_id'] =  $order->frame_id;
            $result['frameUrl'] =  $frame_images_path;
            $result['tileUrl'] =  $tile_images_path;
            $result['frameImage'] =  $order->images;
            $result['framesecondImage'] =  $order->second_image;
            $result['frameType'] =  $order->type;
            $result['tileImage'] =  $order->tile_image;
            $result['tilePrice'] =  $order->price;
            $result['quantity'] =  $order->quantity;
            $result['rotation'] =  $rotation;
            $result['total_tile'] =  $order->total_tile;
            $result['sub_total_price'] =  $order->total_price;
            $result['total_price'] =  $total;
            $result['shipping_price'] =  $order->shipping_price;
            $result['shipping_currency'] =  "MYR";
            $result['coupon_code'] =  $order->coupon_code;
            $result['coupon_discount'] =  number_format((float)$order->coupon_discount, 2);
            $result['wallet_discount'] =  number_format((float)$order->wallet_discount, 2);
            $result['created_at'] =  $order->created_at;
            $results[] =  $result;  
        }
        if( sizeof($orderList) ){
            return response()->json(['status'=>$this->successStatus, 
                'msg' => 'Wishlist Details successfully',
                'response'=> ['wishlistDetails' => $results]
            ]);
        }else{
          return response()->json(['status'=>$this->failureStatus, 'msg' => 'No Wishlist found']); 
        }
    }

}