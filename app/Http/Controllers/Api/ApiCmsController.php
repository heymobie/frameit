<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\CmsModel, App\FaqModel, App\Group_request, Hash; 
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Session;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Database\QueryException;

class ApiCmsController extends Controller 
{
    public $successStatus = true;
    public $failureStatus = false;
    
    /** 
    * create feed api 
    * 
    * @return \Illuminate\Http\Response 
    */ 

    public function getAllFaqList(){

        $faqList = DB::table('faqs_mgmt')->where('status', 1)->get();

        if( sizeof($faqList) ){
            return response()->json(['status'=>$this->successStatus, 
                'msg' => 'Faq list successfully',
                'response'=>['faqList' => $faqList]
            ]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No Faq found']); 
        }
    }
    


    public function getCmsPageList(Request $request){

        $forminput   = $request->all();

        $pageList = DB::table('cms_mgmt')->where('page_name', $forminput['page_name'])->where('status', 1)->first();

        if( !empty($pageList) ){
            return response()->json(['status'=>$this->successStatus, 
                'msg' => 'CMS list successfully',
                'response'=>['pageList' => $pageList]
            ]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No Page found']); 
        }

    }



}