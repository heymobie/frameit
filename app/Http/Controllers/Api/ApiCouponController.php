<?php



namespace App\Http\Controllers\Api;



use Illuminate\Http\Request; 



use App\Http\Controllers\Controller; 



use App\CouponModel, App\Group_request, Hash; 



use Illuminate\Support\Facades\Auth; 



use Validator, DB;



use Illuminate\Validation\Rule;



use Session;



use Illuminate\Routing\UrlGenerator;



use Illuminate\Database\QueryException;







class ApiCouponController extends Controller 
 
{



    public $successStatus = true;



    public $failureStatus = false;



    



    /** 



    * create feed api 



    * 



    * @return \Illuminate\Http\Response 



    */ 







    public function getUserCouponList(Request $request){







        $user_id = $request->user_id;







        $user_info = DB::table('users')->where('id', $user_id)->where('status',1)->first();







        if(!empty($user_info)){







            $signup_date = (!empty($user_info->created_at) ? date('Y-m-d',strtotime($user_info->created_at)) : 'N/A');


            $couponlist = DB::table('coupon')->Join('temp_coupon', 'temp_coupon.code_coupon', '=', 'coupon.code')->where('status',1)->where('temp_coupon.user_id',$user_id)->get();



            if(sizeof($couponlist)){


                $result = array();



                $results = array();



                $start_date = "";



                $end_date = "";



                foreach( $couponlist as $key => $coupon ){







                    $start_date = (!empty($coupon->start_date) ? date('Y-m-d',strtotime($coupon->start_date)) : 'N/A');







                    $end_date = (!empty($coupon->end_date) ? date('Y-m-d',strtotime($coupon->end_date)) : 'N/A');







                    $result['id'] =  $coupon->id;



                    $result['name'] =  $coupon->name;



                    $result['type'] =  $coupon->type;



                    $result['value'] =  $coupon->value;



                    $result['code'] =  $coupon->code;



                    $result['no_of_uses'] =  $coupon->no_of_uses;



                    $result['uses_limit'] =  $coupon->uses_limit;



                    $result['first_signup_only'] =  $coupon->first_signup_only;



                    $result['start_date'] =  $coupon->start_date;



                    $result['end_date'] =  $coupon->end_date;



                    $result['status'] =  $coupon->status;



                    $result['created_at'] = $coupon->created_at ;



                    $result['updated_at'] = $coupon->updated_at ;



                    $results[] =  $result;



                }





                $current_date = date('Y-m-d');

                if($end_date >= $current_date){

            
                    return response()->json(['status'=>$this->successStatus, 
                        'msg' => 'Coupon list successfully',
                        'response'=>['couponList' => $results]
                    ]);

               }else{

                     return response()->json(['status'=>$this->failureStatus, 'msg' => 'No Coupon found']);

                }


            }else{


                return response()->json(['status'=>$this->failureStatus, 'msg' => 'No Coupon found']);

            }

        }else{

            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No account available please signup first']);







        }







        



    }







    public function userApplyCouponCode(Request $request){







        $user_id = $request->user_id;



        $coupon_code = $request->coupon_code;







        $user_info = DB::table('users')->where('id', $user_id)->where('status',1)->first();







        if(!empty($user_info)){







            $coupon_info = DB::table('coupon')->where('code',$coupon_code)->where('status',1)->first();







            if( !empty($coupon_info) ){







                $uses_limit = $coupon_info->uses_limit;



                $end_date = $coupon_info->end_date;



                $current_date = date('Y-m-d');

                if($end_date >= $current_date){

                    $order_info = DB::table('orders')->where('user_id',$user_id)->where('coupon_code',$coupon_code)->get();

                    $uses_count = count($order_info);               
                    if($uses_count < $uses_limit){

                        DB::table('temp_coupon')->where('user_id', '=', $user_id)->delete();

                        $data = [
                                'user_id'=> $user_id,
                                'code_coupon' => $coupon_code,
                            ]; 

                        $check = DB::table('temp_coupon')->insert($data);

                        $value = $coupon_info->value;
                        $type = $coupon_info->type;
                        return response()->json(['status'=>$this->successStatus, 



                            'msg' => 'Coupon apply successfully',



                            'response'=>['coupon_type' => $type, 'coupon_value' => $value]



                        ]);







                    }else{







                        return response()->json(['status'=>$this->failureStatus, 'msg' => 'Coupon uses limit over ']);



                    }







                }else{



                    return response()->json(['status'=>$this->failureStatus, 'msg' => 'Coupon code expire']);



                }   



                



            }else{



                return response()->json(['status'=>$this->failureStatus, 'msg' => 'Coupon code invalid']); 



            }



        }else{



            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No account available please signup first']);



        }







    }











    public function getUserWalletAmount(Request $request){







        $user_id = $request->user_id;







        $user_info = DB::table('users')->where('id', $user_id)->where('status',1)->first();







        if(!empty($user_info)){







            $wallet_amount = $user_info->wallet_balance;







            return response()->json(['status'=>$this->successStatus, 



                'msg' => 'Wallet balance list successfully',



                'response'=>['user_id' => $user_id, 'wallet_balance' => $wallet_amount ]



            ]);



            



        }else{







            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No user found']);







        }



    



    }











    public function getUserReferralLink(Request $request){







        $user_id = $request->user_id;







        $user_info = DB::table('users')->where('id', $user_id)->where('status',1)->first();







        if(!empty($user_info)){







            $referral_code = $user_info->my_referral_code;







            $referral_url = url('/').'/signup/'.$referral_code;







            return response()->json(['status'=>$this->successStatus, 



                'msg' => 'Referal url list successfully',



                'response'=>['user_id' => $user_id, 'referral_url' => $referral_url ]



            ]);



            



        }else{







            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No user found']);







        }



    



    }















    public function getUserRewardHistory(Request $request){







        $forminput =  $request->all();



            $validator = Validator::make($request->all(), [ 



                'user_id'  => 'required',



            ],



            [   



                'user_id.required' => 'required user_id',



            ]



        );







        $user_id = $forminput['user_id'];







        $user_info = DB::table('users')->where('id', $user_id)->where('status',1)->first();







        if(!empty($user_info)){







            $referral_code = $user_info->my_referral_code;



            $referral_url = url('/').'/signup/'.$referral_code;



            $available_credit = number_format((float)$user_info->wallet_balance, 2);



            $redeem_credit = 0;







            // $redeemList =  DB::table('wallet_transaction')



            // ->select('orders.id','wallet_transaction.debit_amount','orders.created_at')



            // ->leftjoin('orders','orders.user_id','=','wallet_transaction.user_id')



            // ->where('wallet_transaction.user_id', $forminput['user_id'])



            // ->get();







             $redeemList =  DB::table('wallettransaction')



            ->select('orders.id','wallettransaction.debit_amount','orders.created_at')



            ->leftjoin('orders','orders.id','=','wallettransaction.order_id')



            ->where('wallettransaction.user_id', $forminput['user_id'])



            ->get();



            



            $result = array();



            $results = array();



            foreach( $redeemList as $key => $reward ){



                if($reward->debit_amount > 0){



                    $result['order_id'] =  $reward->id;



                    $result['amount_redeem'] =  number_format((float)$reward->debit_amount, 2);



                    $result['purchase_date'] =  $reward->created_at;



                    $results[] =  $result;

                }

            }







            $ref_info = DB::table('my_referrals')->where('refferal_id', $user_id)->get();



            //$ref_user = $ref_info->user_id;

            // die;



            foreach( $ref_info as $key => $refinfo1 ){



            $rewardList[] =  DB::table('orders')

                ->select('users.first_name','users.last_name','orders.created_at','wallettransaction.credit_amount')

                ->join('users','users.id','=','orders.user_id')

                ->join('wallettransaction','orders.id','=','wallettransaction.order_id')

                ->where('orders.user_id', $refinfo1->user_id)

                ->get();    



            }



            // print_r(!empty($rewardList));

            // die;



            $result1 = array();



            $results1 = array();

            if(!empty($rewardList)){

                

                //print_r($rewardList);

                                

                foreach( $rewardList as $rewards ){

                    if(!empty($rewards[0]->first_name)){

                        $result1['first_name'] =  $rewards[0]->first_name;

                        $result1['last_name'] =  $rewards[0]->last_name;

        

                        $result1['credit_reward'] =  number_format((float)$rewards[0]->credit_amount, 2);

        

                        $result1['referal_order_date'] =  $rewards[0]->created_at;

        

                        $results1[] =  $result1;

                       

                    }

                 

                    

    

                }

            }

           





            $promoList =  DB::table('coupon')



            ->select('*')



           // ->where('code', 'ZxcEq3')



            ->get();



            



            $result2 = array();



            $results2 = array();



            foreach( $promoList as $key => $promo ){



                $result2['name'] = $promo->name;



                $result2['value'] = $promo->value;



                $result2['type'] =  $promo->type;



                $result2['expiry_date'] =  $promo->end_date;



                $result2['code'] =  $promo->code;



                $results2[] =  $result2;



            }







            return response()->json(['status'=>$this->successStatus, 



                'msg' => 'Reward list successfully',



                'response'=>['referral_url' =>  $referral_url, 'available_credit' => $available_credit, 'redeem_credit' => $redeem_credit, 'redeemList' => $results, 'rewardList' => $results1,'couponList' => $results2 ]



            ]);







        }else{







            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No user found']);







        }







    }







      



    public function getUserPromoRewardCalculation(Request $request){







        $user_id = $request->user_id;



        $coupon_code = !empty($request->coupon_code)? $request->coupon_code:'';



        $reward_point = !empty($request->reward_point)? $request->reward_point:'';



        $type = !empty($request->type)? $request->type:'';



        $total_amt = !empty($request->total_amount)? $request->total_amount:'';







        $user_info = DB::table('users')->where('id', $user_id)->where('status',1)->first();







        if(!empty($user_info)){







            if($type == "Coupon"){







                $coupon_info = DB::table('coupon')->where('code',$coupon_code)->where('status',1)->first();







                if( !empty($coupon_info) ){







                    $uses_limit = $coupon_info->uses_limit;



                    $end_date = $coupon_info->end_date;



                    $current_date = date('Y-m-d');







                    if($end_date >= $current_date){







                        $order_info = DB::table('orders')->where('user_id',$user_id)->where('coupon_code',$coupon_code)->get();







                        $uses_count = count($order_info);               







                        if($uses_count < $uses_limit){







                            $coupon_value = $coupon_info->value;



                            $type = $coupon_info->type;



                            if($type == 'percentage'){



                                $disc_price = ($total_amt * $coupon_value)/100;



                                $coupon_disc = $total_amt - $disc_price ;



                                $coupon_discount = $total_amt - $coupon_disc;



                            }else{



                                $coupon_disc = $total_amt - $coupon_value ;



                                $coupon_discount = $total_amt - $coupon_disc;



                            }







                            return response()->json(['status'=>$this->successStatus, 



                                'msg' => 'Coupon apply successfully',



                                'response'=>['discount_value' => number_format((float)$coupon_disc, 2), 'total_amt' =>$total_amt]



                            ]);







                        }else{







                            return response()->json(['status'=>$this->failureStatus, 'msg' => 'Coupon uses limit over ']);



                        }







                    }else{



                        return response()->json(['status'=>$this->failureStatus, 'msg' => 'Coupon code expire']);



                    }   



                    



                }else{



                    return response()->json(['status'=>$this->failureStatus, 'msg' => 'Coupon code invalid']); 



                }



            }else{



                if($reward_point==0){



                    $discount_value = $total_amt; 



                }else{



                    $discount_value = $total_amt - $reward_point; 



                }



                   



                return response()->json(['status'=>$this->successStatus, 



                    'msg' => 'Coupon apply successfully',



                    'response'=>['discount_value' => number_format((float)$discount_value, 2), 'total_amt' =>$total_amt]



                ]);



            }







        }else{



            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No account available please signup first']);



        }







    }











}