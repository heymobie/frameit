<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\CheckoutModel, App\Group_request, Hash; 
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Session;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Database\QueryException;

class ApiOrderController extends Controller 
{
    public $successStatus = true;
    public $failureStatus = false;
    
    /** 
    * create oeder api 
    * 
    * @return \Illuminate\Http\Response 
    */ 

    public function getAllUserOrderList(Request $request){

        $forminput =  $request->all();
            $validator = Validator::make($request->all(), [ 
                'user_id'  => 'required',
            ],
            [   
                'user_id.required' => 'required user_id',
            ]
        );


        //$orderList = DB::table('orders')->where('user_id', $forminput['user_id'])->get();

        $orderList =  DB::table('orders')
        ->select('frames.title','orders.id','orders.user_id', 'orders.frame_id', 'orders.total_tile',  'orders.total_price', 'orders.coupon_discount','orders.wallet_discount','orders.shipping_price', 'orders.order_status', 'orders.delivery_date', 'order_tracking.tracking_code', 
         'orders.created_at')
        ->join('order_tracking','order_tracking.order_id','=','orders.id')
        ->join('frames','frames.id','=','orders.frame_id')
        ->where('orders.user_id', $forminput['user_id'])
        ->get();
        
        $result = array();
        $results = array();
        foreach( $orderList as $key => $order ){

            $result['order_id'] =  $order->id;
            $result['user_id'] =  $order->user_id;
            $result['frame_id'] =  $order->frame_id;
            $result['frame_name'] =  $order->title;
            $result['total_tile'] =  $order->total_tile;
            $result['total_price'] =  $order->total_price;
            $result['shipping_price'] =  $order->shipping_price;
            $result['coupon_discount'] =  $order->coupon_discount;
            $result['wallet_discount'] =  $order->wallet_discount;
            $result['order_status'] =  $order->order_status;
            $result['delivery_date'] =  $order->delivery_date;
            $result['tracking_code'] =  $order->tracking_code;
            $result['created_at'] = $order->created_at ;
            $results[] =  $result;
        }

        if( sizeof($orderList) ){
            return response()->json(['status'=>$this->successStatus, 
                'msg' => 'Order list successfully',
                'response'=> ['orderList' => $results]
            ]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No order found']); 
        }
    }

    public function getUserOrderDetails(Request $request){
        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
            'order_id'  => 'required',
        ],
        [   
            'order_id.required' => 'required order_id',
        ]
        );

        $orderList =  DB::table('order_detail')
        ->select('frames.price', 'frames.images','frames.second_image', 'frames.type' ,'order_detail.order_id', 'orders.order_id as customer_order_id' ,'order_detail.user_id', 'order_detail.tile_image', 'order_detail.quantity','order_detail.rotation','orders.total_tile',  'orders.total_price', 'orders.coupon_discount', 'orders.wallet_discount', 'orders.shipping_price', 'orders.frame_id','orders.number_people','orders.color','orders.order_status','orders.address_id', 'orders.delivery_date', 'order_tracking.tracking_code', 'orders.created_at')
        ->join('orders','orders.id','=','order_detail.order_id')
        ->join('frames','frames.id','=','orders.frame_id')
        ->join('order_tracking','order_tracking.order_id','=','orders.id')
        ->where('order_detail.order_id', $forminput['order_id'])
        ->get();


            

        // print_r($orderList);
        // die;
        foreach( $orderList as $key => $order1 ){

            $country_info = DB::table('shipping_address')->where('id',$order1->address_id)->first();
        } 

        //$country_info = DB::table('shipping_address')->where('id',$order1->address_id)->first();

        

        $country_name = $country_info->country;
        $day_info = DB::table('shipping_price')->where('country_id',$country_name)->first();

        $sub_total = (($order1->total_price)-($order1->shipping_price)) + number_format((float)$order1->coupon_discount, 2) + number_format((float)$order1->wallet_discount, 2);

        $result = array();
        $results = array();
        foreach( $orderList as $key => $order ){
            if(!empty($order->rotation)){
                $rotation=$order->rotation;
            }else{
                $rotation=0;
            }
            $frame_images_path = url('/public/uploads/frame_images/');
            $tile_images_path = url('/public/uploads/order_images/');
            $result['order_id'] =  $order->order_id;
            $result['customer_order_id']= $order->customer_order_id;
            $result['user_id'] =  $order->user_id;
            $result['frameUrl'] =  $frame_images_path;
            $result['tileUrl'] =  $tile_images_path;
            $result['frameImage'] =  $order->images;
            $result['framesecondImage'] =  $order->second_image;
            $result['frameType'] =  $order->type;
            $result['tileImage'] =  $order->tile_image;
            $result['tilePrice'] =  $order->price;
            $result['quantity'] =  $order->quantity;
            $result['rotation'] =  $rotation;
            $result['total_tile'] =  $order->total_tile;
            $result['sub_total_price'] =  $sub_total;
            $result['total_price'] =  number_format((float)$order->total_price, 2);
            $result['shipping_price'] =  $order->shipping_price;
            $result['shipping_currency'] =  $day_info->currency;
            $result['coupon_discount'] =  number_format((float)$order->coupon_discount, 2);
            $result['wallet_discount'] =  number_format((float)$order->wallet_discount, 2);
            $result['order_status'] =  $order->order_status;
            $result['delivery_date'] =  $order->delivery_date;
            $result['tracking_code'] =  $order->tracking_code;
            $result['color'] =  $order->color;
            $result['number_people'] =  $order->number_people;
            $result['created_at'] =  $order->created_at;
            $results[] =  $result;  
        }

        if( sizeof($orderList) ){
            return response()->json(['status'=>$this->successStatus, 
                'msg' => 'Order Detail list successfully',
                'response'=> ['orderDetails' => $results]
            ]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No Order found']); 
        }
    }


}