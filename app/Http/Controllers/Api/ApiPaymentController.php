<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\PaymentModel, App\CardModel, App\User, App\Group_request, Hash; 
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Session;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Database\QueryException;
use Braintree_Transaction;
use Braintree_ClientToken;

class ApiPaymentController extends Controller 
{
    public $successStatus = true;
    public $failureStatus = false;
    
    /** 
    * create feed api 
    * 
    * @return \Illuminate\Http\Response 
    */ 

    public function saveCardDetails(Request $request){
        $forminput = $request->all();
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'card_number' => 'required',
            'expiry_month' => 'required',
            'expiry_year' => 'required',
            'cvv' => 'required'
        ],
        [   
            'user_id.required'     => 'required user_id',
            'card_number'   => 'required card_number',
            'expiry_month'   => 'required expiry_month',
            'expiry_year'   => 'required expiry_year',
            'cvv'  => 'required cvv'
        ]
        );

        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }            
        }

        $obj = new CardModel;
        $obj->user_id = $forminput['user_id'];
        $obj->card_number = $forminput['card_number'];
        $obj->expiry_month = $forminput['expiry_month'];
        $obj->expiry_year = $forminput['expiry_year'];
        $obj->cvv = $forminput['cvv'];
        $obj->status = 1;
        $obj->created_at = date('Y-m-d H:i:s');
        $res = $obj->save();

        if( $res ){
            if (!empty($forminput['user_id'])) {
                $credentials = array(
                    'card_status' => 1
                ); 

                $updateData = DB::table('users')->where('id',$forminput['user_id'] )->update( $credentials );
            }
            return response()->json(['status'=>$this->successStatus, 'msg' => 'Card added successfully','response'=>['card_status' =>1]]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'Something went wrong']); 
        }

    }


    public function updateCardDetails(Request $request){
        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
            'user_id'  => 'required',
            'card_number'  => 'required',
            'expiry_month'  => 'required',
            'expiry_year'  => 'required',
            'cvv'  => 'required',
        ]
        );
        
        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }            
        }

        $arrayData = array(
            'card_number'=>$forminput['card_number'],
            'expiry_month'=>$forminput['expiry_month'],
            'expiry_year'=>$forminput['expiry_year'],
            'cvv'=>$forminput['cvv'],
            'updated_at'=>date('Y-m-d H:i:s')
        );

        $updateData = DB::table('card_details')->where( 'user_id',$forminput['user_id'] )->update( $arrayData );

        if( $updateData ){
            return response()->json(['status'=>$this->successStatus, 'msg' => 'Card updated successfully', 'response'=>['card_status' =>1]]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'Something went wrong']); 
        }
    }


    public function getCardDetails(Request $request){
        $user_id = $request->user_id;

        $cardList =  DB::table('card_details')->where('user_id',$user_id)->first();

        if( !empty($cardList) ){
            return response()->json(['status'=>$this->successStatus, 
                'msg' => 'Card list successfully', 'response'=>['cardList' => $cardList ]
            ]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'No card found']); 
        }
    }


    public function savePaymentDetail(Request $request){
        //print_r($request->all()); die();
        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
            'user_id'  => 'required',
            'order_id'  => 'required',
            'txn_id'  => 'required',
            'txn_amount'  => 'required',
            'payment_method'  => 'required',
            'card_number'  => 'required',
            'expiry_month'  => 'required',
            'expiry_year'  => 'required',
            'txn_status'  => 'required',
            'txn_date'  => 'required'
          ],
          [   
            'user_id.required'     => 'required user_id',
            'order_id.required'     => 'required order_id',
            'txn_id.required'     => 'required txn_id',
            'txn_amount.required'     => 'required txn_amount',
            'payment_method'      => 'required payment_method',
            'card_number'   => 'required card_number',
            'expiry_month'   => 'required expiry_month',
            'expiry_year'   => 'required expiry_year',
            'txn_status'  => 'required txn_status',
            'txn_date'  => 'required txn_date',
          ]
        );
        
        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }            
        }

        $obj = new PaymentModel;
        $obj->user_id = $forminput['user_id'];
        $obj->order_id = $forminput['order_id'];
        $obj->txn_id = $forminput['txn_id'];
        $obj->txn_amount = $forminput['txn_amount'];
        $obj->payment_method = $forminput['payment_method'];
        $obj->card_number = $forminput['card_number'];
        $obj->expiry_month = $forminput['expiry_month'];
        $obj->expiry_year = $forminput['expiry_year'];
        $obj->cvv = "XXX";
        $obj->txn_status = $forminput['txn_status'];
        $obj->txn_date = $forminput['txn_date'];
        $obj->created_at = date('Y-m-d H:i:s');
        $res = $obj->save();

        if( $res ){
            return response()->json(['status'=>$this->successStatus, 'msg' => 'Payment added successfully']); 
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'Something went wrong']); 
        }
    }

    public function createBraintreeToken(Request $request)
    {
        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
            'user_id'  => 'required'
          ],
          [   
            'user_id.required'     => 'required user_id'
          ]
        );
        
        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }            
        }

        $users = User::where('id', $forminput['user_id'])->first();
        $users->braintree_token = Braintree_ClientToken::generate();
        $res = $users->save();
        if( $res ){

            return response()->json(['status'=>$this->successStatus, 'msg' => 'Token list successfully','response'=>['token' =>$users->braintree_token]]);
            
        }else{
            return response()->json(['status'=>$this->failureStatus, 'msg' => 'Something went wrong']); 
        }
    }

    public function createPayment(Request $request){

        $forminput =  $request->all();
        $validator = Validator::make($request->all(), [ 
            'user_id'  => 'required',
            'nonce'  => 'required',
            'amount'  => 'required'
          ],
          [   
            'user_id.required'     => 'required user_id',
            'amount.required'     => 'required amount',
            'nonce.required'     => 'required nonce'
          ]
        );
        
        if ($validator->fails()) { 
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {   
                return response()->json(['status'=>$this->failureStatus,'msg'=>$message]);            
            }            
        }

        $nonce =$forminput['nonce'];
        $amount =$forminput['amount'];
        $user_id =$forminput['user_id'];

        $status = Braintree_Transaction::sale([
            'amount' => $amount,
            'paymentMethodNonce' => $nonce,
            'options' => [
            'submitForSettlement' => True
            ]
        ]);


        if( $status->success == true ){

            $payment_method = "MasterCard";
            $txn_id = "020200409134801";
            $card_number="4444";
            $expiry_month= "05";
            $expiry_year = "2021";
            $txn_status = "approved";
            $txn_date = "2020-04-09 13:48:01.000000";

            return response()->json(['status'=>$this->successStatus, 'msg' => 'Payment created successfully','response'=>['user_id' =>$user_id ,
                'txn_amount' =>$amount, 'txn_id' => $txn_id, 'payment_method' => $payment_method, 'card_number' => $card_number, 'expiry_month' => $expiry_month, 
                'expiry_year ' => $expiry_year , 'txn_status' => $txn_status, 'txn_date' => $txn_date]]);

        }else{

           return response()->json(['status'=>$this->failureStatus, 'msg' => 'Something went wrong']); 
        }

       // return response()->json($status);
        
    //return $status->success;

    //if($status->success==1){


    }

        

  }