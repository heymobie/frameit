<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Route;
//use App\User;
use App\Helpers\Helper;

class LoginController extends Controller 
{
	//public function login(Request $request) {
		//return view('user/login');
	//}

	public function login_one(Request $request) {
		return view('user/login_one');
	}

	public function login_two(Request $request) {
		return view('user/login_two');
	}

	public function login_three(Request $request) {
		return view('user/login_three');
	}

	public function register_name(Request $request) {
		$request->session()->put('customer_name', $request->firstName);
		return 1;

		//echo session('customer_name');
	}

	public function register_email(Request $request) {
		$email = $request->email;
		$customer_name = session('customer_name');
		$name_arr = explode(' ', $customer_name);

		$vrfn_code = Helper::generateRandomString(6);

		$obj = User::where('email','=',$email)->first();
		if (!empty($obj->id)) {
			# code...
		} else {
			$obj = new User;
			$obj->first_name = (!empty($name_arr[0]) ? $name_arr[0] : '');
			$obj->last_name = (!empty($name_arr[1]) ? $name_arr[1] : '');
			$obj->email = $email;
			$obj->password = bcrypt($vrfn_code);
			$obj->role_id = 2;
			$obj->is_verify_email = 0;
			$obj->is_verify_contact = 0;
			//$obj->my_referral_code = ;
			$obj->wallet_balance = 0;
			$obj->register_by = 'web';
			$obj->vrfn_code = $vrfn_code;
			$obj->status = 1;
			$obj->created_at = date('Y-m-d H:i:s');
			$obj->updated_at = date('Y-m-d H:i:s');
			$res = $obj->save();
			if ($res) {
				return 1;
			} else {
				return 0;
			}
		}

		
	}


	public function email_verification(Request $request)
    {
        $code = $request->id;
        $arr = explode("_", $code);
        $vrfn_code = $arr[0];
        $user_id = $arr[1];

        $user_date = User::find($user_id);
        if ($user_date->vrfn_code == $vrfn_code) {
            $user_date->vrfn_code = '';
            $user_date->is_verify_email = 1;
            $user_date->save();
            session::flash('message', 'Your account is activated. You can do login.');
        } else {
            session::flash('error', 'link expire.');
        }
        return redirect('/');
    }

    public function custom_auth(Request $request)
    {

    Auth::loginUsingId($request->id);

    if(!empty(Session::get('guser_cat_id'))){ $catid = Session::get('guser_cat_id'); }else{ $catid = ''; }

    //return redirect('/select_frame');

    return redirect('/category/'.$catid);

    }

    public function signup(Request $request) {
    	$referral_code = request()->segments(2);
        $data['user'] = User::where('my_referral_code', 'LIKE', '%' . $referral_code[1] . '%')->first();
    //     print_r($data['user']);
    // 	die;
		return view('user/signup')->with($data);
	}
}
?>