<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User;
use App\ShipModel, App\Group_request, Hash, App\PaymentLogs; 
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Route;
use App\Helpers\Helper;
use Mail;
use Redirect;
class PageController extends Controller 
{

	public function terms_of_service(){
		$terms_of_service = DB::select("select * from cms_mgmt where page_name='Terms of Service'");
		return view('user/terms_service',["terms_of_service"=>$terms_of_service]);
	}

	public function about_us(){
		$about_us = DB::select("select * from cms_mgmt where page_name='About Us'");
		return view('user/about_us',["about_us"=>$about_us]);
	}

} 
?>