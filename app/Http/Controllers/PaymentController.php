<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User;
use App\ShipModel, App\Group_request, Hash, App\PaymentLogs; 
use Illuminate\Support\Facades\Auth; 
use Validator, DB;
use Illuminate\Validation\Rule;
use Twilio\Rest\Client;
use Session;
use Route;
use App\Helpers\Helper;
use Mail;
 use Braintree_Transaction;

class PaymentController extends Controller
{



public function process(Request $request)
{
    $payload = $request->input('payload', false);
    $nonce = $payload['nonce'];

    $status = Braintree_Transaction::sale([
  'amount' => '10.00',
  'paymentMethodNonce' => $nonce,
  'options' => [
      'submitForSettlement' => True
  ]
    ]);

    return response()->json($status);
}
     public function payments(Request $request) 
     {
           
           return view('checkout');
     }

     public function handleonlinepay(Request $request){  
        
       // $input = $request->input();

      print_r($request->all());
      die;
        try {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                
                // Creating a customer - If you want to create customer uncomment below code.
                /*  $customer = \Stripe\Customer::create(array(
                        'email' => $request->stripeEmail,
                        'source' => $request->stripeToken,
                        'card' => $request->stripeCard
                    ));

                    $stripe_id = $customer->id;
                
                // Card instance
                // $card = \Stripe\Card::create($customer->id, $request->tokenId); 
                */
            
                $unique_id = uniqid(); // just for tracking purpose incase you want to describe something.
              $total_amt = Session::get('total_price');
                // Charge to customer
                $charge = \Stripe\Charge::create(array(
                    'description' => "Plan: ",
                    'source' => $request->stripeToken,                    
                    'amount' => (int)($total_amt * 100),
                    'currency' => 'USD',
                    'shipping' => [
                      'name' => 'Jenny Rosen',
                      'address' => [
                      'line1' => '510 Townsend St',
                      'postal_code' => '98140',
                      'city' => 'San Francisco',
                      'state' => 'CA',
                      'country' => 'US',
                      ],
                    ],

                  ));
                               
                // Insert into the database
                

                DB::table('payment_logs')->insert(
                            [
                              'amount'=> $total_amt,
                              'plan'=> 'Plan',
                              'charge_id'=>$charge->id,
                              'stripe_id'=>$unique_id,                     
                              'quantity'=>1
                                
                            ]

                        );

                return response()->json([
                    'message' => 'Charge successful, Thank you for payment!',
                    'state' => 'success'
                ]);                
            } catch (\Exception $ex) {
                return response()->json([
                    'message' => 'There were some issue with the payment. Please try again later.',
                    'state' => 'error'
                ]);
            }             
                
    }
}