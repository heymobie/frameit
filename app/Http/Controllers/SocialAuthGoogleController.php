<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Socialite;
use Auth;
use Exception;
//use App\User;
use App\Helpers\Helper;


class SocialAuthGoogleController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }


    public function callback()
    {
        try {
            
        
            $googleUser = Socialite::driver('google')->user();
            /*echo "<pre>";print_r($googleUser);die;*/
            $existUser = User::where('email',$googleUser->email)->first();
            

            if($existUser) {
                Auth::loginUsingId($existUser->id);
            }
            else {
                $name_arr = explode(' ', $googleUser->name);
                $vrfn_code = Helper::generateRandomString(6);

                $user = new User;
                $user->password = md5(rand(1,10000));


                $user->social_id = $googleUser->id;
                $user->profile_pic = (!empty($googleUser->avatar) ? $googleUser->avatar : '');
                $user->first_name = (!empty($name_arr[0]) ? $name_arr[0] : '');
                $user->last_name = (!empty($name_arr[1]) ? $name_arr[1] : '');
                $user->email = $googleUser->email;
                $user->password = bcrypt($vrfn_code);
                $user->role_id = 2;
                $user->is_verify_email = 1;
                $user->is_verify_contact = 0;
                $user->wallet_balance = 0;
                $user->register_by = 'gmail';
                $user->vrfn_code = '';
                $user->status = 1;
                $user->created_at = date('Y-m-d H:i:s');
                $user->updated_at = date('Y-m-d H:i:s');
                $user->save();
                

                $users = User::where('email','=',$googleUser->email)->first();
				$my_referral_code = Helper::my_simple_crypt($users->id,'e');
				$users->my_referral_code = $my_referral_code;
				$users->save();
				//$user_info = User::where('social_id', $googleUser->id)->first();
				Auth::loginUsingId($user->id);
            }
            return redirect()->to('/select_frame');
        } 
        catch (Exception $e) {
            return 'error';
        }
    }
}