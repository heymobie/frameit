<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipCodeModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'order_tracking';
    protected $fillable = [
        'order_id', 'user_id', 'tracking_code'
    ];
    //public $timestamps = false;
}
