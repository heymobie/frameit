<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'shipping_address';
    protected $fillable = [
        'user_id', 'fullname', 'street_address', 'city', 'state', 'country', 'zip_code', 'contact_number'
    ];
    //public $timestamps = false;
}
