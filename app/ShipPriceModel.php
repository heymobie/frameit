<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipPriceModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'shipping_price';
    protected $fillable = [
        'country_id', 'price', 'currency', 'delivery_days'
    ];
    //public $timestamps = false;
}
