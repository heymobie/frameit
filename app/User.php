<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'contact_number', 'role_id', 'is_verify_email', 'is_verify_contact', 'my_referral_code', 'user_referral_id', 'wallet_balance', 'profile_pic', 'gender', 'dob', 'country_id', 'state_id', 'city_id', 'postal_code', 'latitude', 'longitude', 'address', 'parent_id', 'social_id', 'register_by', 'device_id', 'devide_token', 'device_type', 'vrfn_code', 'status', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
