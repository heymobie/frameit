@extends('admin.layout.layout')
@section('title', 'Catagory List')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">
@endsection

@section('current_page_js')
<script src="{{url('/')}}/resources/assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{url('/')}}/resources/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#user_list').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<script type="text/javascript">
 function delete_user(user_id){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
     type: 'POST',
     url: "<?php echo url('/admin/delete_user'); ?>",
     enctype: 'multipart/form-data',
     data:{user_id:user_id,'_token':'<?php echo csrf_token(); ?>'},
     beforeSend:function(){
       return confirm("Are you sure you want to delete this user?");
     },
     success: function(resultData) { 
       console.log(resultData);
       var obj = JSON.parse(resultData);
       if (obj.status == 'success') {
         setTimeout(function() {
          $('#success_message').fadeOut("slow");
        }, 2000 );
        $("#row" + user_id).remove();
       } 
     },
     error: function(errorData) {
      console.log(errorData);
      alert('Please refresh page and try again!');
    }
  });
}
</script>
<script>
  $('.toggle-class').on('change', function() {
    var status = $(this).prop('checked') == true ? 1 : 0; 
    var user_id = $(this).data('id');
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "<?php echo url('/admin/change_user_status'); ?>",
      data: {'status': status, 'user_id': user_id},
      success: function(data){
        $('#success_message').fadeIn().html(data.success);
        setTimeout(function() {
          $('#success_message').fadeOut("slow");
        }, 2000 );
      },
      error: function(errorData) {
        console.log(errorData);
        alert('Please refresh page and try again!');
      }
    });
  })
</script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <p style="display: none;" id="success_message" class="alert alert-success"></p>
     @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
       </ul>
     </div>
     @endif

     @if(Session::has('message'))
     <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
     @endif

     @if(Session::has('error'))
     <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
     @endif
     
     <div class="row mb-2">
       <div class="col-sm-6">
        <h1 class="m-0 text-dark">Category List</h1>
      </div>
      <!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
         <li class="breadcrumb-item"><a href="#">Home</a></li>
         <li class="breadcrumb-item active">Category List</li>
       </ol>
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->
 </div>
 <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">

  <a href="{{url('/admin/add_category')}}" class="btn btn-primary">Add</a>
  <!-- Small boxes (Stat box) -->
  <table id="user_list" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>SNo.</th>
        <th>Category Name</th>
        <th>Head Count</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
     @if(!$category_list->isEmpty())
     <?php $i=1; ?>
     @foreach($category_list as $arr)
     <tr id="row{{$arr->id}}">
      <td>{{$i}}</td>
      <td>{{$arr->category_name}}</td>
     
      <td><?php if($arr->head_count==1){ echo 'Active';}else{ echo 'Deactivate'; } ?></td>
      <td>
        <input data-id="{{$arr->cat_id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $arr->cat_status ? 'checked' : '' }}>
      </td>
      <!--<td>{{(!empty($arr->created_at) ? date('d-m-Y H:i   A',strtotime($arr->created_at)) : 'N/A')}}</td>-->
      <td>
        <a href="{{url('/admin/edit_category')}}/{{base64_encode($arr->cat_id)}}"><i class="fa fa-edit" aria-hidden="true" alt="user" title="user"></i></a>

     </td>
   </tr>
   <?php $i++; ?>
   @endforeach
   @endif

 </tbody>
</table>  
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection         