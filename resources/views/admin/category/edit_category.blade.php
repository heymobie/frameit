@extends('admin.layout.layout')
@section('title', 'Edit Category')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <div class="row mb-2">
     <div class="col-sm-6">
      <h1 class="m-0 text-dark">Category </h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
       <li class="breadcrumb-item"><a href="#">Home</a></li>
       <li class="breadcrumb-item active">Edit Category</li>
     </ol>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">

  @if ($message = Session::get('message'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('error'))
  <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('warning'))
  <div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('info'))
  <div class="alert alert-info alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($errors->any())
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif

 <!-- Small boxes (Stat box) -->
 <form action="{{url('/admin/update_category')}}" id="userupdateForm" method="post" enctype="multipart/form-data">
  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
  <input type="hidden" name="cat_id" id="cat_id" value="{{(!empty($user_info->cat_id) ? $user_info->cat_id : '')}}" />

  <div class="form-group">
    <label for="exampleFormControlInput1">Category Name</label>
    <input type="text" class="form-control" name="category_name" id="exampleFormControlInput1" value="{{(!empty($user_info->category_name) ? $user_info->category_name : '')}}" placeholder="Category Name">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Head Count</label>
    <select class="form-control" name="head_count">
      <option value="1" <?php if($user_info->head_count==1){ echo 'selected';} ?>>Active</option>
      <option value="0" <?php if($user_info->head_count==0){ echo 'selected';} ?>>Deactive</option>
    </select>
  </div>
  

 
  <div class="row">
    <!-- /.col -->
    <div class="col-4">
      <button class="btn btn-primary" name="submit" type="submit">Submit</button>
    </div>
    <!-- /.col -->
  </div>
</form> 
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
<script type="text/javascript">
  $('#userupdateForm').validate({ 
      // initialize the plugin
      rules: {
        email: {
          required: true,
          email: true
        },
        contact_number: {
          required: true,
          digits:true,
          minlength : 8,
          maxlength : 13
        },
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
  </script>
</div>
@endsection         