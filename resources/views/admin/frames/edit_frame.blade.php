@extends('admin.layout.layout')

@section('title', 'Edit Product')



@section('current_page_css')

<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

@endsection



@section('content')

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <div class="content-header">

   <div class="container-fluid">

    <div class="row mb-2">

     <div class="col-sm-6">

      <h1 class="m-0 text-dark">Product </h1>

    </div>

    <!-- /.col -->

    <div class="col-sm-6">

      <ol class="breadcrumb float-sm-right">

       <li class="breadcrumb-item"><a href="#">Home</a></li>

       <li class="breadcrumb-item active">Edit Product</li>

     </ol>

   </div>

   <!-- /.col -->

 </div>

 <!-- /.row -->

</div>

<!-- /.container-fluid -->

</div>

<!-- /.content-header -->

<!-- Main content -->

<section class="content">

 <div class="container-fluid">



  @if ($message = Session::get('message'))

  <div class="alert alert-success alert-block">

    <button type="button" class="close" data-dismiss="alert">×</button> 

    <strong>{{ $message }}</strong>

  </div>

  @endif





  @if ($message = Session::get('error'))

  <div class="alert alert-danger alert-block">

    <button type="button" class="close" data-dismiss="alert">×</button> 

    <strong>{{ $message }}</strong>

  </div>

  @endif





  @if ($message = Session::get('warning'))

  <div class="alert alert-warning alert-block">

    <button type="button" class="close" data-dismiss="alert">×</button> 

    <strong>{{ $message }}</strong>

  </div>

  @endif





  @if ($message = Session::get('info'))

  <div class="alert alert-info alert-block">

    <button type="button" class="close" data-dismiss="alert">×</button> 

    <strong>{{ $message }}</strong>

  </div>

  @endif





  @if ($errors->any())

  <div class="alert alert-danger">

    <button type="button" class="close" data-dismiss="alert">×</button> 

    <ul>

     @foreach ($errors->all() as $error)

     <li>{{ $error }}</li>

     @endforeach

   </ul>

 </div>

 @endif



 <!-- Small boxes (Stat box) -->

 <form action="{{url('/admin/update_frame')}}" id="frameForm" method="post" enctype="multipart/form-data">

  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

  <input type="hidden" name="frame_id" id="frame_id" value="{{(!empty($frame_info->id) ? $frame_info->id : '')}}" />

  <div class="form-group">

    <label for="exampleFormControlInput1">Title</label>

    <input type="text" class="form-control" name="title" id="exampleFormControlInput1" value="{{(!empty($frame_info->title) ? $frame_info->title : '')}}" placeholder="Title">

  </div>



  <div class="form-group">

    <label for="exampleFormControlInput1">Quantity</label>

    <input type="number" class="form-control" name="quantity" id="exampleFormControlInput1" value="{{(!empty($frame_info->quantity) ? $frame_info->quantity : '')}}" placeholder="Quantity">

  </div>



  <div class="form-group">

    <label for="exampleFormControlInput1">Amount</label>

    <input type="number" class="form-control" name="price" value="{{(!empty($frame_info->price) ? $frame_info->price : '')}}" id="exampleFormControlInput1" placeholder="Amount">

  </div>


   <div class="form-group">

    <label for="exampleFormControlInput1">Frame Type</label>
    <select class="form-control" name="type" id="type">
      <option value="square" <?php if($frame_info->type == "square"){echo 'selected';} ?>>Square</option>
      <option value="rectangle" <?php if($frame_info->type == "rectangle"){echo 'selected';} ?>>Rectangle</option>
    </select>
  </div>


   <div class="form-group">

  <label for="exampleFormControlInput1">Frame Category</label>
    <select class="form-control" name="category_id" id="category_id">
       <!--  <option value="">Select Category</option> -->
        @foreach($category as $c)
          <option value="{{$c->cat_id}}" <?php if($frame_info->category_id == $c->cat_id){echo 'selected';} ?>>{{$c->category_name}}</option>
        @endforeach
    </select>
  </div>


  <div class="form-group">

    <label for="exampleFormControlInput1">Size</label>

    <input type="text" class="form-control" name="size" value="{{(!empty($frame_info->size) ? $frame_info->size : '')}}" id="exampleFormControlInput1" placeholder="Size">

  </div>


  <div class="form-group">

    <label for="exampleFormControlInput1">Size Inch</label>

    <input type="text" class="form-control" name="size_inch" value="{{(!empty($frame_info->size_inch) ? $frame_info->size_inch : '')}}" id="exampleFormControlInput1" placeholder="Size Inch">

  </div>


  <div class="form-group">

    <label for="exampleFormControlInput1">Face Count</label>

    <input type="text" class="form-control" name="face_count" value="{{(!empty($frame_info->face_count) ? $frame_info->face_count : '')}}" id="exampleFormControlInput1" placeholder="Face Count">

  </div>




  <div class="form-group">

    <label for="exampleFormControlFile1">Upload Product</label>

    <input type="file" name="images" class="form-control-file" id="exampleFormControlFile1">

  </div>

<!--    <div class="form-group">

    <label for="exampleFormControlFile1">Horizontal Upload Product</label>

    <input type="file" name="second_image" class="form-control-file" id="exampleFormControlFile1">

  </div>
 -->


  <div class="row">

    <!-- /.col -->

    <div class="col-4">

      <button class="btn btn-primary" name="submit" type="submit">Submit</button>

    </div>

    <!-- /.col -->

  </div>

</form> 

<!-- /.row -->





</div>

<!-- /.container-fluid -->

</section>

<!-- /.content -->

</div>

@endsection         