@extends('admin.layout.layout')
@section('title', 'View Order Details')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">
@endsection

@section('current_page_js')
<script src="{{url('/')}}/resources/assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{url('/')}}/resources/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <div class="row mb-2">
     <div class="col-sm-6">
      <h1 class="m-0 text-dark">Order Details</h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
       <li class="breadcrumb-item"><a href="#">Home</a></li>
       <li class="breadcrumb-item active">Order Details</li>
     </ol>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">

  @if ($errors->any())
  <div class="alert alert-danger">
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif

 @if(Session::has('message'))
 <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
 @endif

 @if(Session::has('error'))
 <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
 @endif

 <!-- Small boxes (Stat box) -->
 <table id="" class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>SNo.</th>
      <th>Tile Image</th>
      <th>Quntity</th>
      <th>Price</th>
      <th>Sub-Total</th>
    </tr>
  </thead>
  <tbody>
    @if(!$order_details->isEmpty())
    <?php $i=1; $sum=0; $subtotal=0;?>
    @foreach($order_details as $arr)
    <?php 
      $sub_total = $arr->price * $arr->quantity;
      $sum += $sub_total;
      $grand_total = $sum + $arr->shipping_price - $arr->coupon_discount - $arr->wallet_discount ; 
     ?>
    <tr>
      <td>{{$i}}</td>
      <td>
        <img style="width: 150px;" src="{{url('/')}}/public/uploads/order_images/{{$arr->tile_image}}">
      </td>
      <td>{{$arr->quantity}}</td>
      <td>{{$arr->price}}  {{$arr->currency}}</td>
      <td>{{$sub_total}}  {{$arr->currency}}</td>
    </tr>
    <?php $i++; ?>
    @endforeach
    <tr>
        <td></td><td></td><td></td><td><b>Total Price</b></td><td><b>{{$sum}}  {{$arr->currency}}</b></td> 
    </tr>
    <tr>
        <td></td><td></td><td></td><td><b>Shipping Price</b></td><td><b>{{$arr->shipping_price}}  {{$arr->currency}}</b></td> 
    </tr>
    <tr>
        <td></td><td></td><td></td><td><b>Coupon Amount</b></td><td><b>{{$arr->coupon_discount}}  {{$arr->currency}}</b></td> 
    </tr>
    <tr>
        <td></td><td></td><td></td><td><b>Wallet Amount</b></td><td><b>{{$arr->wallet_discount}}  {{$arr->currency}}</b></td> 
    </tr>
    <tr>
        <td></td><td></td><td></td><td><b>Grand Total</b></td><td><b>{{$grand_total}}  {{$arr->currency}}</b></td> 
    </tr>
    @endif
  </tbody>
</table>   
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection         