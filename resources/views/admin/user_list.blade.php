@extends('admin.layout.layout')
@section('title', 'User List')

@section('current_page_css')
   <link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('current_page_js')
   <script src="{{url('/')}}/resources/assets/plugins/datatables/jquery.dataTables.js"></script>
   <script src="{{url('/')}}/resources/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
   <script type="text/javascript">
      $(function () {
          $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
          });
        });
   </script>
@endsection

@section('content')
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
               <div class="container-fluid">
                  <div class="row mb-2">
                     <div class="col-sm-6">
                        <h1 class="m-0 text-dark">User List</h1>
                     </div>
                     <!-- /.col -->
                     <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                           <li class="breadcrumb-item"><a href="#">Home</a></li>
                           <li class="breadcrumb-item active">User List</li>
                        </ol>
                     </div>
                     <!-- /.col -->
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
               <div class="container-fluid">


                  <!-- Small boxes (Stat box) -->
                     <table id="example2" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>SNo.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact Number</th>
                        <th>Gender</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                        <tbody>
                           @if(!$user_list->isEmpty())
                              <?php $i=1; ?>
                              @foreach($user_list as $arr)
                                 <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$arr->fullname}}</td>
                                    <td>{{$arr->email}}</td>
                                    <td>{{$arr->user_mob}}</td>
                                    <td>{{$arr->user_gender}}</td>
                                    <td>
                                       <a href="{{url('/admin/user_profile')}}/{{base64_encode($arr->id)}}"><i class="fa fa-user" aria-hidden="true" alt="profile" title="profile"></i></a>

                                       <a href="{{url('/admin/video_list')}}/{{base64_encode($arr->id)}}"><i class="fa fa-file-video" aria-hidden="true" alt="video gallery" title="video gallery"></i></a>

                                       <a href="{{url('/admin/image_list')}}/{{base64_encode($arr->id)}}"><i class="fa fa-image" aria-hidden="true" alt="image gallery" title="image gallery"></i></a>

                                       <a href=""><i class="fas fa-user-friends" aria-hidden="true" alt="Followers" title="Followers"></i></a>

                                       <a href=""><i class="fas fa-users-cog" aria-hidden="true" alt="Following" title="Following"></i></a>

                                       <a href=""><i class="fas fa-user-slash" aria-hidden="true" alt="Blocked Followers" title="Blocked Followers"></i></a>

                                       <a href="" ><i class="fa fa-rss" aria-hidden="true" alt="Feeds" title="Feeds"></i></a>
                                    </td>
                                 </tr>
                                 <?php $i++; ?>
                              @endforeach
                           @endif
                            
                      </tbody>
                    </table>   
                  <!-- /.row -->

                  
               </div>
               <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
         </div>
@endsection         