@extends('admin.layout.layout')
@section('title', 'Change Password')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <div class="row mb-2">
     <div class="col-sm-6">
      <h1 class="m-0 text-dark">Password</h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
       <li class="breadcrumb-item"><a href="#">Home</a></li>
       <li class="breadcrumb-item active">Change Password</li>
     </ol>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">

   @if ($message = Session::get('message'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('error'))
  <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('warning'))
  <div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($message = Session::get('info'))
  <div class="alert alert-info alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
  @endif


  @if ($errors->any())
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif
 <!-- Small boxes (Stat box) -->
 <form action="{{url('/admin/vendor/update_user_password')}}" id="updateuserpassForm" method="post" enctype="multipart/form-data">

  <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />

  <input type="hidden" name="user_id" value="{{(!empty($user_info->vendor_id) ? $user_info->vendor_id : '')}}" />

  <!--<div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}"">
    <label for="new-password">Current Password</label>
    <input type="password" class="form-control" name="current-password" id="current-password" placeholder="Current Password">
    @if ($errors->has('current-password'))
        <span class="help-block">
            <strong>{{ $errors->first('current-password') }}</strong>
        </span>
    @endif
  </div>-->

  <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
    <label for="new-password">New Password</label>
    <input type="password" class="form-control" name="new-password" id="new-password" placeholder="New Password">
    @if ($errors->has('new-password'))
        <span class="help-block">
            <strong>{{ $errors->first('new-password') }}</strong>
        </span>
    @endif
  </div>


  <div class="form-group">
    <label for="new-password-confirm">New Confirm Password</label>
    <input type="password" class="form-control" name="new-password_confirmation" id="new-password-confirm" placeholder="New Confirm Password">
  </div>

  <div class="row">
    <!-- /.col -->
    <div class="col-4">
      <button class="btn btn-primary" type="submit">Update Password</button>
    </div>
    <!-- /.col -->
  </div>
</form> 
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->

<script type="text/javascript">
  $('#updateuserpassForm').validate({ 
      // initialize the plugin
      rules: {
       current-password: {
        required: true
      },

      new-password: {
        required: true
      },

      new-password_confirmation: {
        required: true
      }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
</script>
</div>
@endsection         