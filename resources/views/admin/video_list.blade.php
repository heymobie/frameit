@extends('admin.layout.layout')
@section('title', 'Video Gallery')

@section('current_page_css')
   <link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('current_page_js')
   <script src="{{url('/')}}/resources/assets/plugins/datatables/jquery.dataTables.js"></script>
   <script src="{{url('/')}}/resources/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
   <script type="text/javascript">
      $(function () {
          $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
          });
        });
   </script>

   <script type="text/javascript">
     function delete_video(video_id){
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
             type: 'POST',
             url: "<?php echo url('/admin/delete_video'); ?>",
             enctype: 'multipart/form-data',
             data:{video_id:video_id,'_token':'<?php echo csrf_token(); ?>'},
             //cache:false,
             //contentType: false,
             //processData: false,
             beforeSend:function(){
                 return confirm("Are you sure you want to delete this video?");
              },
             success: function(resultData) { 
                var obj = JSON.parse(resultData);
                if (obj.status == 'succ') {
                   alert(obj.msg);
                   location.reload();
                } else {

                   alert(obj.msg);
                }
             },
             error: function(errorData) {
                console.log(errorData);
                alert('Please refresh page and try again!');
             }
          });
      }
   </script>
@endsection

@section('content')
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
               <div class="container-fluid">
                  <div class="row mb-2">
                     <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{$user_info->fullname}}</h1>
                     </div>
                     <!-- /.col -->
                     <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                           <li class="breadcrumb-item"><a href="#">Home</a></li>
                           <li class="breadcrumb-item active">Video Gallery</li>
                        </ol>
                     </div>
                     <!-- /.col -->
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
               <div class="container-fluid">


                  <!-- Small boxes (Stat box) -->
                     <table id="example2" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>SNo.</th>
                        <th>Image</th>
                        <th>Create Date</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                        <tbody>
                           @if(!$video_list->isEmpty())
                              <?php $i=1; ?>
                              @foreach($video_list as $arr)
                                 <tr>
                                    <td>{{$i}}</td>
                                    <td>
                                      <video width="320" height="240" controls>
                                        <source src="{{url('/')}}/public/uploads/feed_videos/{{$arr->media_url}}">
                                      </video>
                                    </td>
                                    <td>{{(!empty($arr->created_at) ? date('d-m-Y H:i A',strtotime($arr->created_at)) : 'N/A')}}</td>
                                    <td>
                                       <a href="javascript:void(0)" onclick="delete_video('<?php echo $arr->id; ?>');"><i class="fa fa-trash" aria-hidden="true" alt="profile" title="profile"></i></a>
                                    </td>
                                 </tr>
                                 <?php $i++; ?>
                              @endforeach
                           @endif
                            
                      </tbody>
                    </table>   
                  <!-- /.row -->

                  
               </div>
               <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
         </div>
@endsection         