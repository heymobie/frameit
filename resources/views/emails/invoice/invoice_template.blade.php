<!DOCTYPE html>
<!-- saved from url=(0065)http://votivelaravel.in/designer/FrameIt/HTML/email_template.html -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Email Template</title>

<link href="{{url('/').'/resources/views/emails/invoice/css'}}" rel="stylesheet">

<style type="text/css">
    
@media(min-width:320px) and (max-width: 767px) 
{
img.logo_im {
    width: auto;
    max-width: 100%;
}

.bill_add span {
    width: 100% !important;
}

.bill_add {
    width: 92% !important;
}
span.fir_str {
    padding-top: 7px;
    border-top: 1px solid #e0e0e0;
    margin-top: 12px;
}

span.Date_s {
    width: 100% !important;
    float: left;
    line-height: 25px;
}

span.Date_s_on {
    width: 100% !important;
    float: left;
}

body.main_b_s {
    margin: 10px auto !important;
}

.main_h_s {
    padding: 0 0px !important;
    max-width: 900px !important; 
    margin: 0 auto !important;
}

.main_h_ss {
    width: 90% !important;
}

}

</style>



</head>
<body class="main_b_s" style="font-family: &#39;open Sans&#39;;font-size: 14px; line-height:20px;">
    <div class="main_h_s" style="padding: 0 10px;max-width: 600px;margin: 0 auto;">
        

        <div class="main_h_ss" style="max-width:600px;width:100%;padding:10px;margin:0 auto 30px;border:1px solid #faaa1b;background: #faaa1b">
        	<div style="background: #fff;padding: 15px;">
                <div style="padding:17px 30px 30px;border:1px solid #fff;background:rgba(243, 243, 243, 0.72);">
                <div style="text-align:center">
                    <h2 style="color: #fff;"><img src="{{url('/').'/resources/front_assets/img/mixshoot_logo.png'}}" class="logo_im"></h2>
                </div>
                <div style="border-bottom:1px dashed #8a8a8a94;margin:15px auto 15px;padding:10px;display:block;overflow:hidden;max-width: 400px; text-align: center; color: #fff;">
                    <h4 style="margin:0 0; font-size: 28px; color: #000; font-weight: 500; margin-bottom: 15px;">Hii {{!empty($user_info->first_name)? $user_info->first_name : 'N/A'}} {{!empty($user_info->last_name)? $user_info->last_name : 'N/A'}}</h4>
                </div>

                <div style=" display:block; overflow:hidden">
                    <p style=" margin:0 0 8px 0; color: #faaa1b; text-align: center; font-size: 20px; font-weight: 600;">Thank you for your order</p>
                    <p style="text-align: center;"><a style="color: #faaa1b;font-size: 14px;font-weight: 600; color: #000; text-align: center; text-decoration:none;" href="#">Get More Details</a></p>
                </div>
                <div style="text-align: center;">
                    <a href="#" style="text-decoration:none; color:#000; padding-bottom:2px; display:inline-block; text-align: center;">Visit site : - Frameit</a>
                </div>
        </div>
            		
                <div style="width: 100%;display: block;text-align: center;padding-bottom: 10px;padding-top: 10px;margin-bottom: 10px;margin-top: 14px;font-size: 20px;">
                    <span class="Date_s_on" style="width: 30%; display: inline-block; vertical-align: top; padding-bottom: 5px;"><strong>Order Date:</strong></span>
                    <span class="Date_s" style="width: 35%; display: inline-block; vertical-align: top; padding-bottom: 5px;">{{(!empty($order_info->created_at) ? date('d / F / Y',strtotime($order_info->created_at)) : 'N/A')}}</span>
                </div>


                <span style=" width: 100%;display: inline-block;vertical-align: top;padding-bottom: 5px;margin: 10px 0 7px;"><strong>Billing Address:</strong></span>

                <div class="bill_add" style=" width: 97%;display: block;border-top: 1px solid #9999994d;padding: 13px 10px;border-left: 1px solid #9999994d;border-right: 1px solid #9999994d;">

        	       <span class="" style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                    <strong style="font-weight: 600;color: #000;">Product Order ID:</strong></span> 
                    <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">#{{!empty($order_info->id) ? $order_info->id : ''}}</span>
                    <span class="fir_str" style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                    <strong style="font-weight: 600;color: #000;">Payment Method:</strong></span>
                    <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">Visa Card</span>
                </div>

                <div class="bill_add" style="width: 97%;display: block;border-top: 1px solid #9999994d;padding: 13px 10px;border-left: 1px solid #9999994d;border-right: 1px solid #9999994d;">
                    <span class="" style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                    <strong style="font-weight: 600;color: #000;">User Name:</strong></span> 
                    <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">{{!empty($user_info->first_name) ? $user_info->last_name : ''}}</span>
                    <span class="fir_str" style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                    <strong style="font-weight: 600;color: #000;">Address:</strong></span>
                    <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">{{!empty($user_info->address) ? $user_info->address : ''}}</span>
                </div>

                <div class="bill_add" style="width: 97%;display: block;border-top: 1px solid #9999994d;padding: 13px 10px;border-left: 1px solid #9999994d;border-right: 1px solid #9999994d;">
                   <span class="" style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                   <strong style="font-weight: 600;color: #000;">Phone Number:</strong></span> 
                    <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">{{!empty($user_info->contact_number) ? $user_info->contact_number : ''}}</span>
                    <span class="fir_str" style="width: 10%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                   <strong style="font-weight: 600;color: #000;">Email Address:</strong></span>
                    <span style="width: 10%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">{{!empty($user_info->email) ? $user_info->email : ''}}</span>
                </div>

                <div class="bill_add" style="width: 97%;display: block;border: 1px solid #9999994d;padding: 13px 10px;">
                   <span class="" style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                    <strong style="font-weight: 600;color: #000;">Delivery Type:</strong></span> 
                    <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">012345</span>
                    <span class="fir_str" style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                    <strong style="font-weight: 600;color: #000;">Delivery Date:</strong></span>
                    <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">{{!empty($order_info->delivery_date) ? $order_info->delivery_date : ''}}</span>
                </div>


                <span style="width: 100%;display: inline-block;vertical-align: top;padding-bottom: 5px;margin: 25px 0 7px;"><strong>Shippping Address:</strong></span>

                <div class="bill_add" style=" width: 97%;display: block;border-top: 1px solid #9999994d;padding: 13px 10px;border-left: 1px solid #9999994d;border-right: 1px solid #9999994d;">

                <span style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                <strong style="font-weight: 600;color: #000;">User Name:</strong></span> 
                <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">{{!empty($order_info->fullname) ? $order_info->fullname : ''}}</span>
                <span class="fir_str" style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                <strong style="font-weight: 600;color: #000;">Contact Number:</strong></span>
                <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">{{!empty($order_info->contact_number) ? $order_info->contact_number : ''}}</span>
                </div>

                <div class="bill_add" style=" width: 97%;display: block;border: 1px solid #9999994d;padding: 13px 10px;">

                <span style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                <strong style="font-weight: 600;color: #000;">Country:</strong></span> 
                <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">{{!empty($order_info->country) ? $order_info->country : ''}}</span>
                <span class="fir_str" style="width: 28%; display: inline-block; vertical-align: top; padding-bottom: 0px;">
                <strong style="font-weight: 600;color: #000;">Address:</strong></span>
                <span style="width: 20%; display: inline-block; vertical-align: top; padding-bottom: 0px;font-weight: 100;">{{!empty($order_info->street_address) ? $order_info->street_address : ''}}, {{!empty($order_info->city) ? $order_info->city : ''}}, {{!empty($order_info->state) ? $order_info->state : ''}},
                    {{!empty($order_info->zip_code) ? $order_info->zip_code : ''}}
                </span>
                </div>



                 <div style="margin-top: 27px;">
                     <h2><a href="http://votivelaravel.in/designer/FrameIt/HTML/email_template.html#" style="text-decoration: none; color: #faaa1b; font-size: 24px;">Order #121</a></h2>
                     <h4 style="font-weight: 500;margin-bottom: 8px;">Product Details:-</h4>
                   
                     <div style="margin-top: 27px; overflow-x: auto;">
                     <table style="border-collapse: collapse; border:1px solid #ccc; width: 100%;">
                         <tbody>
                            <tr style="border:1px solid #ccc; ">
                                 <td style="border:1px solid #ccc; padding: 10px;"><strong>S. No:</strong></td>
                                 <td style="border:1px solid #ccc; padding: 10px;"><strong>Frame Image:</strong></td>
                                 <td style="border:1px solid #ccc; padding: 10px;"><strong>Product Image:</strong></td>
                                 <td style="border:1px solid #ccc; padding: 10px;"><strong>Product Price:</strong></td>
                                 <td style="border:1px solid #ccc; padding: 10px;"><strong>Product Qty:</strong></td>
                                 <td style="border:1px solid #ccc; padding: 10px;"><strong>Total Price:</strong></td>
                            </tr>
                            <?php $i=1; $total = 0; $sum=0;?>
                            @foreach($order_details as $arr)
                            <?php 
                            $sub_total = $arr->price * $arr->quantity;
                            $sum += $sub_total;
                            $total = $sum + $arr->shipping_price - $arr->wallet_discount - $arr->coupon_discount; 
                            ?>
                                <tr style="border:1px solid #ccc; ">
                                     <td style="border:1px solid #ccc; padding: 10px;">{{$i}}</td>  
                                     <td style="border:1px solid #ccc; padding: 10px;"><img style="width:100px;" src="{{url('/')}}/public/uploads/frame_images/{{$arr->images}}" /></td>   
                                     <td style="border:1px solid #ccc; padding: 10px;"><img style="width:100px;" src="{{url('/')}}/public/uploads/order_images/{{$arr->tile_image}}" /></td>
                                     <td style="border:1px solid #ccc; padding: 10px;">{{$arr->price}} {{$arr->shipping_currency}}</td>
                                     <td style="border:1px solid #ccc; padding: 10px;">{{$arr->quantity}}</td>
                                     <td style="border:1px solid #ccc; padding: 10px;">{{$sub_total}} {{$arr->shipping_currency}}</td>
                                </tr>
                            <?php $i++; ?>
                            @endforeach
                    
                        </tbody>
                    </table>
                    </div>  
                 </div>   

                 <div style="margin-top: 2px;">
                  <table style="border-collapse: collapse; border:1px solid #ccc; width: 100%;">
                      <tbody><tr style="border:1px solid #ccc; ">
                      <td style="border:1px solid #ccc; padding: 10px; width: 78%;"><strong style="font-weight: 600;color: #6f6f6f;">
                      Total:</strong></td>
                      <td style="border:1px solid #ccc; padding: 10px;"><strong style="font-weight: 600;color: #6f6f6f;">{{$sum}} {{$arr->shipping_currency}}</strong></td>
                      </tr>
                      <tr style="border:1px solid #ccc; ">
                      <td style="border:1px solid #ccc; padding: 10px; width: 78%;"><strong style="font-weight: 600;color: #6f6f6f;">
                      Shipping Price:</strong></td>
                      <td style="border:1px solid #ccc; padding: 10px;"><strong style="font-weight: 600;color: #6f6f6f;">{{$arr->shipping_price}} {{$arr->shipping_currency}}</strong></td>
                      </tr>
                      <tr style="border:1px solid #ccc; ">
                      <td style="border:1px solid #ccc; padding: 10px; width: 78%;"><strong>Wallet Discount:</strong></td>
                      <td style="border:1px solid #ccc; padding: 10px;"><strong style="font-weight: 600;">{{$arr->wallet_discount}} {{$arr->shipping_currency}}</strong></td>
                      </tr>
                      <tr style="border:1px solid #ccc; ">
                      <td style="border:1px solid #ccc; padding: 10px; width: 78%;"><strong>Coupon Discount:</strong></td>
                      <td style="border:1px solid #ccc; padding: 10px;"><strong style="font-weight: 600;">{{$arr->coupon_discount}} {{$arr->shipping_currency}}</strong></td>
                      </tr>
                      <tr style="border:1px solid #ccc; ">
                      <td style="border:1px solid #ccc; padding: 10px; width: 78%;"><strong>Grand Total:</strong></td>
                      <td style="border:1px solid #ccc; padding: 10px;"><strong style="font-weight: 600;">{{$total}} {{$arr->shipping_currency}}</strong></td>
                      </tr>
                  </tbody></table>

                 </div> 

        		<p style="text-align: center;  padding-top: 15px;">
                <a style="color: #ffffff;font-size: 15px;font-weight: 600;padding: 12px 18px;border: 2px dashed #ce9023;display: inline-block;text-decoration: none;background: #faaa1b;" href="https://votivelaravel.in/newframeit/" target="_blank">Track Order</a>
               </p>
        	</div>
        </div>
    </div>


</body></html>