@extends('user.layout.layout')
@section('title', 'User - Profile')

@section('current_page_css')
<style type="text/css">
#logreg-forms .social-login{
  width:390px;
  margin:0 auto;
  margin-bottom: 14px;
}
#logreg-forms .social-btn{
  font-weight: 100;
  color:white;
  /*width:190px;*/
  font-size: 0.9rem;
}

#logreg-forms .facebook-btn{  background-color:#3C589C; }

#logreg-forms .google-btn{ background-color: #DF4B3B; }

#logreg-forms .form-signup .social-btn{ width:210px; }

.form-signup .social-login{
  width:210px !important;
  margin: 0 auto;
}

@media screen and (max-width:500px){
  #logreg-forms  .social-login{
    width:200px;
    margin:0 auto;
    margin-bottom: 10px;
  }
  #logreg-forms  .social-btn{
    font-size: 1.3rem;
    font-weight: 100;
    color:white;
    width:200px;
    height: 56px;
    
  }
  #logreg-forms .social-btn:nth-child(1){
    margin-bottom: 5px;
  }
  #logreg-forms .social-btn span{
    display: none;
  }
  #logreg-forms  .facebook-btn:after{
    content:'Facebook';
  }
  
  #logreg-forms  .google-btn:after{
    content:'Google+';
  }
  
}

#logreg-forms .facebook-btn {
  background-color: #3C589C;
}
</style>
@endsection

@section('current_page_js')

<script type="text/javascript">
  $('#signup_form').validate({ 
      // initialize the plugin
      rules: {
       name: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength : 6
      },
      confirm_password: {
        required: true,
        equalTo : "#password"
      },
      contact_number: {
        required: true,
        digits:true,
        minlength : 8,
        maxlength : 13
      },
    },
    submitHandler: function(form) {
         //form.submit();
         register();
       }
     });

  $.ajaxSetup({
    headers: {'votive':'123456'}
  });

  function register() {
    var $this = $('form#signup_form')[0];
    var formData = new FormData($this);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/user_register'; ?>",
      data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          console.log(resultData);
          if (resultData.status) {
            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';
            $("#signupResBox").html(result_alert);
            document.getElementById("signup_form").reset();
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#signupResBox").html(result_alert);
          }
        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#signupResBox").html(result_alert);
        }
      });
  }

  $('#forgot_pass_form').validate({ 
      // initialize the plugin
      rules: {
       email: {
        required: true,
        email: true
      },
      password: {
        required: true
      }
    },
    submitHandler: function(form) {
         //form.submit();
         forgot_password();
       }
     });

  $('#login_form').validate({ 
      // initialize the plugin
      rules: {
       email: {
        required: true,
        email: true
      },
      password: {
        required: true
      }
    },
    submitHandler: function(form) {
         //form.submit();
         login();
       }
     });

  function login() {
    var $this = $('form#login_form')[0];
    var formData = new FormData($this);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/login'; ?>",
      data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          console.log(resultData);
          if (resultData.status) {
            //resultData.data.id
            document.getElementById("login_form").reset();
            window.location.href = "<?php echo url('/custom_auth'); ?>"+"/"+resultData.data.id;
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#loginResBox").html(result_alert);
          }
        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#loginResBox").html(result_alert);
        }
      });
  }

  function forgot_password() {
    var $this = $('form#forgot_pass_form')[0];
    var formData = new FormData($this);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/forgot_password'; ?>",
      data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          console.log(resultData);
          if (resultData.status) {
            //resultData.data.id
            document.getElementById("forgot_pass_form").reset();
            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#forgotPassResBox").html(result_alert);
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#forgotPassResBox").html(result_alert);
          }
        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#forgotPassResBox").html(result_alert);
        }
      });
  }
  
</script>
  <script type="text/javascript">
    $(".cat_id").click(function(){
      var category_id = $(this).data("cat");
      window.location.href = "<?php echo url('/category'); ?>"+"/"+category_id;
    });
  </script>
@endsection

@section('tag_body')
<body class="bx_logIn">
  @endsection

  @section('content')
  <section id="" class="section selectFramStyles bx_logMain">
    <div class="container">
      <div class="selectFrameSec">
        <!-- <h2>Get Started</h2> -->
      </div>
      <div class="stp_one">
        <div class="content">
          <div class="bg-layer">
            <div class="bg-circle"></div>
          </div>          
          <div id="logreg-forms">
            <form class="log_form" id="login_form" method="post" action="<?php echo url('/').'/weblogin'; ?>">
              <div id="loginResBox">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> {{ Session::get('message') }}</div>        
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> {{ Session::get('error') }}</div>
                @endif
              </div>
              @csrf
              <div class="form-label">
                <h2>Category</h2>                
              </div>              
              <div class="in_firs flex-gird">
                @foreach($category as $c)
                 <label class="customradio">
                  <input class="FormInput cat_id" type="radio" name="category_id" value="{{$c->cat_id}}" data-cat="{{$c->cat_id}}">  <span class="checkmark">{{$c->category_name}}</span>  </label>
                @endforeach
              </div>
              
          <!--     <div class="button_cont">
                <input class="btn_Submit" type="submit" value="Sign In">
              </div> -->
                            
            </form>

                     
          </div>
        </div>
      </div>
      <div class="framesTabing">
       <!--  <div class="sectionWraper"></div> -->
      </div>
    </div>
  </section>


  @endsection

  @section('page_footer')

  
  @endsection