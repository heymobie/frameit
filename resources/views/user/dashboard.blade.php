@extends('user.layout.layout')
@section('title', 'User - Profile')

@section('current_page_css')
@endsection

@section('current_page_js')

<?php $user_id=Auth::user()->id; ?>
<script>
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });
  var user_id = "<?php echo $user_id ?>";
  $(document).ready(function(){
    var formData = new FormData();
    formData.append('user_id',user_id);
    //alert(id);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/getuserdetails'; ?>",
      data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          //console.log(resultData);
          if(resultData.status){
            $("#name").html(resultData.response.first_name);

            if(resultData.response.profile_pic){          

                $("#usrImg").attr("src",resultData.response.profile_pic);
            }else{
              $("#usrImg").attr("src","http://votivelaravel.in/frameit/resources/front_assets/img/user_image.jpg");
              //$("#usrImg").attr("src","http://localhost/frameit/resources/front_assets/img/user_image.jpg");
            }

            $("#err_msg").html(result_alert);


          }

         },error: function(errorData) { 
            console.log(errorData);
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            
            $("#err_msg").html(result_alert);
          }
        });

    
      });

    </script>



@endsection

@section('tag_body')
  <body>
@endsection

@section('content')
   <section id="" class="section bx_mainCrop">
    <div class="selectFrameSec">
      <h2>Frame Style</h2>
      <!--      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home" class="active show"><div class="frmStyl"><img src="img/cleanIcon.svg"><span>Clean</span></div></a></li>
        <li><a data-toggle="tab" href="#menu1" cl><div class="frmStyl "><img src="img/everIcon.svg"><span>Ever</span></div></a></li>
        <li><a data-toggle="tab" href="#menu2"><div class="frmStyl"><img src="img/boldIcon.svg"><span>Bold</span></div></a></li>
        <li><a data-toggle="tab" href="#menu3"><div class="frmStyl"><img src="img/classicIcon.svg"><span>Classic</span></div></a></li>
      </ul>   --> 
    </div>





 



 <div id="my_profile_sec" class=" area-padding">
  <div class="container">
    <div class="row  wow slideInUp" data-wow-delay=".1s">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="section-headline text-center">
          <h2><span>My</span>  Dashboard</h2>
        </div>
      </div>
    </div>
    <div class="col-lg-12" role="tabpanel">
      <div class="row manag-tabs">
       <?php 

 $currentPath= Route::getFacadeRoot()->current()->uri();
// echo $currentPath;
        ?>
        <div class="col-sm-3">
          <ul class="nav nav-pills brand-pills nav-stacked" role="tablist">
            <?php

            $currentPath= Route::getFacadeRoot()->current()->uri(); 
            if($currentPath=='dashboard'){
              $classAct='active';
            }else{
              $classAct='';
            } 
            // $currentPath= Route::getFacadeRoot()->current()->uri(); 
            // if($currentPath=='dashboard'){
            //   $classAct='active';
            // }else{
            //   $classAct='';
            // }

            // if($currentPath=='profile'){
            //   $classAct='active';
            // }else{
            //   $classAct='';
            // }

            ?> 

            


            <li class="brand-nav <?php echo $classAct; ?>"><a href="{{url('/dashboard')}}" aria-controls="tab1" role="tab"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>

            <!-- <li class="brand-nav active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li> -->

            

            <!-- <li class="brand-nav"><a href="#" aria-controls="tab2" role="tab" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li> -->

            <li class="brand-nav"><a href="{{url('/profile')}}" aria-controls="tab2" role="tab"><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li>
            <li class="brand-nav"><a href="#" aria-controls="tab3" role="tab" data-toggle="tab"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Profile</a></li>
            <li class="brand-nav"><a href="#" aria-controls="tab4" role="tab" data-toggle="tab"><i class="fa fa-sign-out" aria-hidden="true"></i> Change Password</a></li>
            <!-- <li class="brand-nav"><a href="{{url('/logout')}}" aria-controls="tab5" role="tab" data-toggle="tab"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li> -->
            <li class="brand-nav"><a href="{{url('/logout')}}" aria-controls="tab5" role="tab" ><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
          </ul>
        </div>
        <div class="col-sm-9">
          <div class="tab-content">
          
            <div role="tabpanel" class="tab-pane active" id="tab1">
              <div class="profile-basic background-white p20">
                
                <form>
                  <div class="prof-img">
                    <div class="user-photo"> <a href="#"> 

                      <!-- <img src="{{url('/').'/resources/front_assets'}}/img/user_image.jpg" alt="User Photo"> -->

                      <img id="usrImg" src="" alt="User Photo"> </a> 
                    </div>
                  </div>
              
                  <div class="prsn-info my-profl ne_ur_pr">
                  
                  <div class="row">
                    
                  <div class="usr_nm">
                  <h2 id="name"></h2>
                 
        </div>
                         
                  <div class="form-group col-lg-12">
                   <p><label>Country </label><strong>NYC</strong></p>
                  </div>
                  
                  <div class="form-group col-lg-12">
                   <p><label>Country </label><strong>USA</strong></p>
                  </div> 
                      
                      
                      
                  </div>
                  
                    <!-- /.row --> 
                  </div>
                </form>
                
                
                <div class="bottom_ur_dashbo">
                <div class="col-md-12 wow slideInUp" data-wow-delay=".2s">
               
                <div class="row">
                
                <h2>Welcome to frameit</h2>

               

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
               
                <p><strong>Lorem Ipsum is simply dummys?</strong>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                </p>
                
                </div>
                
                </div>
                </div>
                
              </div>
            </div>
            
            <div role="tabpanel" class="tab-pane " id="tab2">
              <div class="profile-basic background-white p20">
                <div class="row">
                  <div class="profile-basic background-white p20 np20">
                <form>
                  <div class="prof-img">
                    <div class="user-photo"> <a href="#"> <img src="img/user_image.jpg" alt="User Photo"> </a> </div>
                  </div>
                  <div class="prsn-info my-profl ne_ur_pr">
                    <div class="row">
                      <div class="usr_nm">
                        <h2>John Deo</h2>
                        </div>
                      <div class="form-group col-lg-12">
                        <p>
                          <label>User Name </label>
                          <strong>johndeo</strong></p>
                      </div>
                      <!-- /.form-group -->
                      
                      <div class="form-group col-lg-12">
                        <p>
                          <label>Mobile </label>
                          <strong>+91 1234 567 890</strong></p>
                      </div>
                      <!-- /.form-group -->
                      
                      <div class="form-group col-lg-12">
                        <p>
                          <label>Country </label>
                          <strong>India</strong></p>
                      </div>
                      <!-- /.form-group --> 
                      
                    </div>
                    <!-- /.row --> 
                  </div>
                </form>
                <div class="bottom_ur_pro">
                  <div class="col-md-12 wow slideInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInUp;">
                    <div class="row">
                      <div class="projt-list">
                       
                        <div class="tab-menu"> 
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li class="active"> <a href="#p-view-1" role="tab" data-toggle="tab" aria-expanded="true">About</a> </li>
                            <li class=""> <a href="#p-view-2" role="tab" data-toggle="tab" aria-expanded="true">EXPERIENCE</a> </li>
                          </ul>
                        </div>
                        
                        <div class="tab-content">
                          <div class="tab-pane active" id="p-view-1">
                            <div class="tab-inner">
                              <div class="event-content head-team">
                                <div class="use_bot_one">
                                  <div class="col-md-3">
                                    <div class="use_bot_on_tx"> <strong> Biography</strong> </div>
                                  </div>
                                  <div class="col-md-8">
                                    <div class="use_bot_on_tx">
                                      <p>You have not yet provided any information.</p>
                                    </div>
                                  </div>
                                  <div class="col-md-1">
                                    <div class="use_bot_on_tx"> <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> </div>
                                  </div>
                                </div>
                                <div class="use_bot_one">
                                  <div class="col-md-3">
                                    <div class="use_bot_on_tx"> <strong> Links</strong> </div>
                                  </div>
                                  <div class="col-md-8">
                                    <div class="use_bot_on_tx">
                                      <p>You have not added any links yet.</p>
                                    </div>
                                  </div>
                                  <div class="col-md-1">
                                    <div class="use_bot_on_tx"> <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> </div>
                                  </div>
                                </div>
                                <div class="use_bot_one">
                                  <div class="col-md-3">
                                    <div class="use_bot_on_tx"> <strong> Skills</strong> </div>
                                  </div>
                                  <div class="col-md-8">
                                    <div class="use_bot_on_tx">
                                      <p>Web Design</p>
                                    </div>
                                  </div>
                                  <div class="col-md-1">
                                    <div class="use_bot_on_tx"> <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> </div>
                                  </div>
                                </div>
                                <div class="use_bot_one">
                                  <div class="col-md-3">
                                    <div class="use_bot_on_tx"> <strong> Networks</strong> </div>
                                  </div>
                                  <div class="col-md-8">
                                    <div class="use_bot_on_tx">
                                      <p>You have not added any networks yet.</p>
                                    </div>
                                  </div>
                                  <div class="col-md-1">
                                    <div class="use_bot_on_tx"> <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> </div>
                                  </div>
                                </div>
                                <div class="use_bot_one">
                                  <div class="col-md-3">
                                    <div class="use_bot_on_tx"> <strong> Causes</strong> </div>
                                  </div>
                                  <div class="col-md-8">
                                    <div class="use_bot_on_tx">
                                      <p>Employment Services</p>
                                    </div>
                                  </div>
                                  <div class="col-md-1">
                                    <div class="use_bot_on_tx"> <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="tab-pane" id="p-view-2">
                            <div class="tab-inner">
                              <div class="event-content head-team">
                                <div class="exp_tab_sec">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div><!-- /.row -->

            </div>
            </div>
            
            <div role="tabpanel" class="tab-pane" id="tab3">
              <div class="profile-basic background-white p20">
                <form>
                  <div class="prof-img">
                    <div class="user-photo"> <a href="#"> <img src="img/user_img.jpg" alt="User Photo"> </a> </div>
                  </div>
                  <div class="prsn-info my-profl ne_ur_pr">
                    <div class="row">
                      <div class="usr_nm">
                        <h2>Marina jonson </h2>
                        <span>Volunteer</span> </div>
                      <div class="form-group col-lg-12">
                        <p>
                          <label>User Name </label>
                          <strong>marina j.</strong></p>
                      </div>
                      <!-- /.form-group -->
                      
                      <div class="form-group col-lg-12">
                        <p>
                          <label>City </label>
                          <strong>Indore</strong></p>
                      </div>
                      <!-- /.form-group -->
                      
                      <div class="form-group col-lg-12">
                        <p>
                          <label>Country </label>
                          <strong>India</strong></p>
                      </div>
                      <!-- /.form-group --> 
                      
                    </div>
                    <!-- /.row --> 
                  </div>
                </form>
              </div>
            </div>


            <div role="tabpanel" class="tab-pane" id="tab4">
              <div class="profile-basic background-white p20"> 
                <div class="profile-basic background-white p20">
                  <div class="profile-basic background-white p20 np21">
                    <div class="edit_pro">
                    <h3 class="ed_pro">Change Password</h3>
                   
                      <div class="">
                      <div class="doc-prfl-left">
                            <div class="row">
                              <div class="form-group">
                                <div class="row">
                                <label class="col-sm-4 col-md-4 col-lg-4 control-label">Old Password:</label>
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                  <input type="password" class="form-control" id="oldpassword" name="oldpassword">
                                    <input type="hidden" class="form-control" name="mobile_no" id="mobile_no" value="9827563277">
                                    <span toggle="#oldpassword" class="pass-view fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                </div></div>
                              </div>
                              <div class="form-group"><div class="row">
                                <label class="col-sm-4 col-md-4 col-lg-4 control-label">New Password:</label>
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                  <input type="password" class="form-control" name="newpassword" id="newpassword">
                                   <span toggle="#newpassword" class="pass-view fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                </div></div>
                              </div>
                              <div class="form-group">
                                <div class="row">
                                <label class="col-sm-4 col-md-4 col-lg-4 control-label">Confirm Password:</label>
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                  <input type="password" class="form-control" name="conpassword" id="conpassword">
                                  <span toggle="#conpassword" class="pass-view fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                </div></div>
                              </div>
                              
                              <div class="form-group mng-edit-prf-btn">
                          <label class="col-sm-4 col-md-4 col-lg-4 control-label"></label>
                          <div class="col-sm-8 col-md-8 col-lg-8">
                             <button class="btn btn-primary color-btn" name="change_password_btn" id="change_password_btn">Change Password</button>
                          </div>
                        </div>
                  
                          </div>
                          </div><!-- /.row -->
                        </div>
                        
                    </div>
                    </div>
               </div>
              </div>
            </div>
            
            <div role="tabpanel" class="tab-pane" id="tab4">
              <div class="profile-basic background-white p20"> </div>
            </div>
            
          </div>
        </div>
      </div>
    </div> 
  </div>
</div>
</section>
@endsection

@section('page_footer')
  
@endsection