@extends('user.layout.layout')
@section('title', 'User - Profile')

@section('current_page_css')
@endsection

@section('current_page_js')

<?php  $user_id=Auth::user()->id; ?>
<script type="text/javascript">

  $.ajaxSetup({
    headers: {'votive':'123456'}
  });
  
  var user_id = "<?php echo $user_id ?>";
  $(document).ready(function(){
    var formData = new FormData();
    formData.append('user_id',user_id);
    //alert(id);
    $.ajax({
      type: 'POST',
      //beforeSend: function(xhr){xhr.setRequestHeader('votive':'123456');},
      url: "<?php echo url('/').'/api/getuserdetails'; ?>",
      data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          //console.log(resultData);
          if(resultData.status){
          $("#first_name").val(resultData.response.first_name);
          $("#last_name").val(resultData.response.last_name);
          //$("#email").val(resultData.response.email);
          $("#contact_number").val(resultData.response.contact_number);

          if(resultData.response.date_of_birth){
            $("#date_of_birth").val(resultData.response.date_of_birth);
          }else{
            $("#date_of_birth").val();
          }

          if(resultData.response.gender){
            $("#gender").val(resultData.response.gender);
          }else{
            $("#gender").val();
          }

          if(resultData.response.address){
            $("#address").val(resultData.response.address);
          }else{
            $("#address").val();
          }

         if(resultData.response.profile_pic){          

              $("#blah").attr("src",resultData.response.profile_pic);
          }else{
            $("#blah").attr("src","http://localhost/frameit/resources/front_assets/img/user_image.jpg");
          }

          $("#err_msg").html(result_alert);


              }

           },error: function(errorData) { 
              console.log(errorData);
              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
              
              $("#err_msg").html(result_alert);
            }
          });
    });
   

    $('#editProfile_form').validate({ 
      // initialize the plugin
      rules: {
       first_name: {
        required: true
      },
      last_name: {
        required: true
      },
      gender:{
        required: true,
      },
      date_of_birth:{
        required: true,
      },
      address:{
        required: true,
      },
      contact_number: {
        required: true,
        digits:true,
        minlength : 8,
        maxlength : 13
      },
    },
    submitHandler: function(form) {
         //form.submit();
         update_profile();
       }
    });

    $.ajaxSetup({
      headers: {'votive':'123456'}
    });

    function update_profile() {
      var $this = $('form#editProfile_form')[0];
      var formData = new FormData($this);
      //var user_id = "<?php //echo $user_id ?>";
      //formData.append('user_id',user_id);
      console.log(formData);
      $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/updateprofile'; ?>",
        data: formData,
          //dataType: "text",
          enctype: 'multipart/form-data',
          cache:false,
          contentType: false,
          processData: false,
          success: function(resultData) { 
            console.log(resultData);
            if (resultData.status) {
              var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';
              $("#signupResBox").html(result_alert);
              window.location.href = "<?php echo url('/edit-profile'); ?>";
              //document.getElementById("signup_form").reset();
            } else {
              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
              
              $("#signupResBox").html(result_alert);
            }
          },
          error: function(errorData) { 
            //console.log(errorData);
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            
            $("#signupResBox").html(result_alert);
          }
        });
    }

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#imgInp").change(function() {
      readURL(this);

     var file = this.files[0];
        var fileType = file.type;
        var match = ['image/jpeg', 'image/png', 'image/jpg'];
        if(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]))){
            alert('Sorry, only JPG, JPEG, & PNG files are allowed to upload.');
            $("#imgInp").val('');
            return false;
        }
  });
  
  $( function() {
    //$( "#date_of_birth" ).datepicker();
    var date = $('#date_of_birth').datepicker({ dateFormat: 'yy-mm-dd' }).val();
  } );

</script>



<script type="text/javascript">
  //start get user order list
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });

  var user_id = "<?php echo $user_id ?>";
  
  $(document).ready(function(){
    var formData = new FormData();
    formData.append('user_id',user_id);
    $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/getAllUserOrderList'; ?>",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          //console.log(resultData);
          if(resultData.status){

            var orderArray = [];
            var j=1;
            $.each(resultData.response.orderList, function( i, l ){
              var order_id = resultData.response.orderList[i].order_id;
              var site_url = "<?php echo url('/').'/order_details'; ?>";
          
              var orders = '<div class="edit_pr_section_one"><div class="col-md-2"><div class="section_one_nn"><p> Order id:</p><strong>'+resultData.response.orderList[i].order_id+'</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p>Placed one</p><strong>'+resultData.response.orderList[i].created_at+'</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p> Items</p><strong>'+resultData.response.orderList[i].total_tile+' Frames '+resultData.response.orderList[i].frame_name+' Frame Style</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p> Paid on</p><strong>'+resultData.response.orderList[i].created_at+'</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p> Total</p><strong>USD '+resultData.response.orderList[i].total_price+' </strong></div></div><div class="col-md-2"><div class="section_one_nn manage"><a href="#" class="mang_ac ">Manage</a><a href="'+site_url+'/'+order_id+'">Details</a></div></div></div>';

              orderArray.push(orders);
              j++;
            });
            

          }else{

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> No order Found.</div>';
            $("#err_msg").html(result_alert);

          }

        },error: function(errorData) { 

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            $("#err_msg").html(result_alert);

        }
    });
  });

  //end get user order list
  </script>

 @foreach($orderList as $order1)
  <script type="text/javascript">
  $(document).ready(function() {
    $(".mang_ac<?php echo $order1->order_id; ?>").click(function () {
      $(".orderList").hide();
      $(".detail<?php echo $order1->order_id; ?>").show();


    });
  });     
</script>
@endforeach


@endsection

@section('tag_body')
  <body>
@endsection

@section('content')
  <section id="" class="section bx_mainCrop">
    <div class="selectFrameSec">
  
    </div>

 <div id="my_profile_sec" class=" area-padding">
  <div class="container">
  <div class="inner_secPro">

    <div class="row  wow slideInUp" data-wow-delay=".1s">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="section-headline text-center">
         <!--  <h2><span>Edit</span> Profile</h2> -->

        <div class="inn_h_secc">
                <a href="{{url('/')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to homepage</a>
                <h3>My Order</h3>
              </div>

        </div>
      </div>
    </div>
    <div class="col-lg-12" role="tabpanel">
      <div class="row manag-tabs">
       
        <div class="col-sm-2">
          <ul class="nav nav-pills brand-pills nav-stacked" role="tablist">
            <?php

            $currentPath= Route::getFacadeRoot()->current()->uri(); 
            if($currentPath=='edit-profile'){
              $classAct='active';
            }else{
              $classAct='';
            } 
            // $currentPath= Route::getFacadeRoot()->current()->uri(); 
            // if($currentPath=='dashboard'){
            //   $classAct='active';
            // }else{
            //   $classAct='';
            // }

            // if($currentPath=='profile'){
            //   $classAct='active';
            // }else{
            //   $classAct='';
            // }

            ?>
            <!-- <li class="brand-nav "><a href="#" aria-controls="tab1" role="tab" data-toggle="tab"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li> -->
            <li class="brand-nav"><a href="{{url('/profile')}}" aria-controls="tab2" role="tab" ><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li>
            <li class="brand-nav <?php echo $classAct; ?>"><a href="{{url('/my_order')}}" aria-controls="tab3" role="tab" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> My Order </a></li>

            <li class="brand-nav"><a href="{{url('/save_frame_list')}}" aria-controls="tab3" role="tab" ><i class="fa fa-shopping-cart" aria-hidden="true"></i> Save Order </a></li>

            <!--  <li class="brand-nav"><a href="{{url('/my-rewards')}}" aria-controls="tab4" role="tab"><i class="fa fa-sign-out" aria-hidden="true"></i> My Rewards</a></li> -->
          
          </ul>
        </div>
     
        <div class="col-sm-10">
          <div class="tab-content">
          
            <div role="tabpanel" class="tab-pane " id="tab1">
              <div class="profile-basic background-white p20">
                
                <form>
                  <div class="prof-img">
                    <div class="user-photo"> <a href="#"> <img src="img/user_image.jpg" alt="User Photo"> </a> </div>
                  </div>
              
                  <div class="prsn-info my-profl ne_ur_pr">
                  
                  <div class="row">
                    
                  <div class="usr_nm">
                  <h2>John Deo</h2>
                 
        </div>
                         
                  <div class="form-group col-lg-12">
                   <p><label>Country </label><strong>USA</strong></p>
                  </div>
                  
                  <div class="form-group col-lg-12">
                   <p><label>Status </label><strong>Ready to Volunteer</strong></p>
                  </div> 
                      
                      
                      
                  </div>
                  
                    <!-- /.row --> 
                  </div>
                </form>
                
                
                <div class="bottom_ur_dashbo">
                <div class="col-md-12 wow slideInUp" data-wow-delay=".2s">
               
                <div class="row">
                
                <h2>Welcome to frameit</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
               
                <p><strong>Lorem Ipsum is simply dummys?</strong>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                </p>
                
                </div>
                
                </div>
                </div>
                
              </div>
            </div>
            
            <div role="tabpanel" class="tab-pane" id="tab2">
              <div class="profile-basic background-white p20">
                <div class="row">


              <div class="profile-basic background-white p20 np20">
                <form>
                  <div class="prof-img">
                    <div class="user-photo"> <a href="#"> <img src="img/user_image.jpg" alt="User Photo"> </a> </div>
                        <div class="full-width">
            
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <input type="file" id="main-input" class="form-control form-input form-style-base">
                            <h4 id="fake-btn" class="form-input fake-styled-btn text-center truncate"><span class="margin"> Choose File</span></h4>
                        </div>

                    </div>
                  </div>
                  <div class="prsn-info my-profl ne_ur_pr">
                    <div class="row">
                      <div class="usr_nm">
                        <h2>John Deo</h2>
                         <span class="pro_edit"><a href="#"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit </a></span>
                        
                      <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>User Name </label>
                          <strong>@johndeo</strong></p>
                      </div>
                      <!-- /.form-group -->
                      
                      <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>Email </label>
                          <strong>johndeo@gmail.com</strong></p>
                      </div>
                      <!-- /.form-group -->
                      
                      <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>Phone Number </label>
                          <strong>+91 1234 112 121</strong></p>
                      </div>
                      <!-- /.form-group --> 

                       <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>Date of Birth </label>
                          <strong>Dec / 18 / 2018</strong></p>
                      </div>

                       <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>Gender </label>
                          <strong>Male</strong></p>
                      </div>

                        <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>Address </label>
                          <strong>Lorem Ipsum is simply dummy </strong></p>
                      </div>
                      
                    </div>
                    <!-- /.row --> 
                  </div>
                
              </div>
                </form>


                <div class="bottom_ur_pro">
                                  
                                  <div class="col-md-12 wow slideInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInUp;">
          <div class="row">

            

            <div class="projt-list">

              <div class="tab-menu">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li class="active">
                    <a href="#p-view-1" role="tab" data-toggle="tab" aria-expanded="true">About</a>
                  </li>
                  <li class="">
                    <a href="#p-view-2" role="tab" data-toggle="tab" aria-expanded="false">EXPERIENCE</a>
                  </li>
                  
                </ul>
              </div>
           
              <div class="tab-content">
              
                <div class="tab-pane active" id="p-view-1">
                  <div class="tab-inner">
                    <div class="event-content head-team">
                    
                    <div class="use_bot_one">
                      <div class="row">
                    <div class="col-md-3">
                    <div class="use_bot_on_tx">
                   <strong> Biography</strong>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="use_bot_on_tx">
                    <p>Lorem Ipsum is simply dummy.</p>
                    </div>
                    </div>
                    <div class="col-md-1">
                    <div class="use_bot_on_tx">
                    <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                    </div></div>
                    </div> 
                    
                    <div class="use_bot_one">
                       <div class="row">
                    <div class="col-md-3">
                    <div class="use_bot_on_tx">
                    <strong> Links</strong>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="use_bot_on_tx">
                    <p>Lorem Ipsum is simply dummy.</p>
                    </div>
                    </div>
                    <div class="col-md-1">
                    <div class="use_bot_on_tx">
                    <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                    </div>
                    </div></div>
                    
                    <div class="use_bot_one">
                       <div class="row">
                    <div class="col-md-3">
                    <div class="use_bot_on_tx">
                    <strong> Skills</strong>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="use_bot_on_tx">
                    <p>Lorem Ipsum is simply dummy.</p>
                    </div>
                    </div>
                    <div class="col-md-1">
                    <div class="use_bot_on_tx">
                    <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                    </div>
                    </div></div>
                    
                    <div class="use_bot_one">
                       <div class="row">
                    <div class="col-md-3">
                    <div class="use_bot_on_tx">
                    <strong> Networks</strong>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="use_bot_on_tx">
                    <p>Lorem Ipsum is simply dummy.</p>
                    </div>
                    </div>
                    <div class="col-md-1">
                    <div class="use_bot_on_tx">
                    <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                    </div></div>
                    </div>
                    
                    <div class="use_bot_one">
                       <div class="row">
                    <div class="col-md-3">
                    <div class="use_bot_on_tx">
                    <strong> Causes</strong>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="use_bot_on_tx">
                    <p>Lorem Ipsum is simply dummy.</p>
                    </div>
                    </div>
                    <div class="col-md-1">
                    <div class="use_bot_on_tx">
                    <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                    </div></div>
                    </div>
                      
                    </div>
                  </div>
                </div>
                
                <div class="tab-pane" id="p-view-2">
                  <div class="tab-inner">
                    <div class="event-content head-team">                      
                    <div class="exp_tab_sec">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                    
                    </div>
                   
                      
                    </div>
                 </div>
                 
                </div>
                
              </div>

            </div>

          </div>
        </div>
                                  
                                  
                                  </div>
            
              </div>


                </div><!-- /.row -->


              </div>
            </div>
            
            <div role="tabpanel" class="tab-pane active" id="tab3">

              <div class="profile-basic background-white p20 ed_p">
              

                <div class="edit_pro">
                      
                 <!--  <h3 class="ed_pro">Edit Profile</h3>
                  <div id="signupResBox"></div>       
                  <form id="editProfile_form" method="post" action="#" enctype="multipart/form-data">
                    <?php $user_id=Auth::user()->id ?>
                  <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                  <div class="row">
                  
                  <div class="col-md-3">
                      <div class="form-group">
                        <div class="user-photo">  

                      
                      <img  id="blah"  src="" alt="User Photo"> </a> 
                   </div>
                   <div class="full-width">
                   <div class="col-lg-12 col-md-12 col-sm-12">
                    <input type="file" name="profile_pic" id="imgInp" class="form-control form-input form-style-base">
                    <h4 id="fake-btn" class="form-input fake-styled-btn text-center truncate"><span class="margin"> Choose File</span></h4>
                   </div>
                   </div>
                   </div>
                   </div>

                   <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleFormControlInput1">First name </label>
                       
                        <input name="first_name" type="text" class="form-control" id="first_name" placeholder="first name">
                      </div>
                   </div>
                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleFormControlInput1">Last name </label>
                        <input name="last_name" type="text" class="form-control" id="last_name" placeholder="last name">
                      </div>
                    </div>
                  

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleFormControlInput1">Select gender </label>
                        <select  name="gender" id="gender" class="form-control">
                            <option value="">Select</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                      
                      </div>
                    </div>  

                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleFormControlInput1">Phone Number </label>
                        <input name="contact_number" type="text" class="form-control" id="contact_number" placeholder="phone number">
                      </div>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleFormControlInput1">DOB </label>
                       
                        <input name="date_of_birth" type="text" class="form-control" id="date_of_birth" >
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="exampleFormControlInput1">Address </label>
                        <input name="address" type="text" class="form-control" id="address" placeholder="address">
                      </div>
                    </div>
                    
                  
                    <div class="form-group mng-edit-prf-btn">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                    <input class="btn btn-primary color-btn" type="submit" value="Save">
                    </div></div>

                  </div>  
                    
                 </form> -->





<div class="edit_pr_section orderList">

@foreach($order as $ord)

  <div class="edit_pr_section_one">
  <div class="col-md-2">
  <div class="section_one_nn">
  <p> Order id:</p>
  <strong>{{$ord->id}}</strong>
  </div></div>

  <div class="col-md-2">
  <div class="section_one_nn">
  <p>Placed on</p>
  <strong>{{$ord->created_at}}</strong>
  </div></div>

  <div class="col-md-2">
  <div class="section_one_nn">
  <p> Items</p>
  <strong>{{$ord->total_tile}} Frames {{$ord->title}} Frame Style</strong>
  </div></div>

  <div class="col-md-2">
  <div class="section_one_nn">
  <p>Paid on</p>
  <strong>{{$ord->created_at}}</strong>
  </div></div>

  <div class="col-md-2">
  <div class="section_one_nn">
  <p> Total</p>
  <strong>USD {{$ord->total_price}}</strong>
  </div></div>

  <div class="col-md-2">
  <div class="section_one_nn">
  <a href="#" class="mang_ac{{$ord->id}}">Manage</a>
  <a href="{{url('/order_details')}}/{{$ord->id}}">Details</a>
  </div></div>

  </div>

@endforeach


</div>


<div >

@foreach($orderList as $order) 
<div class="edit_pr_section_two detail{{$order->order_id}}"  style="display: none;">
<div class="edit_pr_section_two_top">

<span class="hhh_head"><h1>Order ID : {{$order->customer_order_id}}</h1>  <b>placed on {{$order->created_at}}</b>    <strong class="tot_r">TOTAL: <b>USD {{$order->total_price}}</b></strong></span>
<span class="httli">Quantity: <strong >{{$order->quantity}}x Frames: {{$order->title}} Frame Style, Color : {{$order->color}} </strong></span>

<div class="orderdetail">
<!-- <img src="{{url('/')}}/public/uploads/order_images/{{$order->tile_image}}" style="width:150px;height:160px">
 -->
   <?php if($order->alignment=="vertical"){ ?>
                          @if($order->rotation=='0')

                               <?php if($order->type=='square'){ ?>

                                <style type="text/css">
                                html body .orderdetail img.up_images {
                                  position: absolute;
                                  top: 10px;
                                  margin: 0px !important;
                                  width: 220px !important;
                                  min-width: 255px !important;
                                  height: 255px !important;
                                  left: 2px !important;
                                  max-width: 214px;
                                  }
                                  .orderdetail img.set_frame.saveframe {
                                  width: 100% !important;
                                  max-width: 270px !important;
                                  /* height: 220px !important; */
                                  overflow: hidden;
                                  z-index: 3;
                                  position: relative;
                                  }
                                </style>
                             
                                <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$order->images}}" style="transform: rotate(<?php echo $order->rotation.'deg';?>);display: block;margin-right: auto !important;width: 24%;" >
                                <img  src="{{url('/')}}/public/uploads/order_images/{{$order->tile_image}}"  class="up_images" style="display: block;margin-left: 0%;margin-right: auto !important;width: 191px !important;height: 238px;margin-top: -29%;"> 

                              <?php }else{ ?> 
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$order->images}}" style="transform: rotate(<?php echo $order->rotation.'deg';?>);display: block;margin-right: auto !important;width: 24%;" >
                                <img  src="{{url('/')}}/public/uploads/order_images/{{$order->tile_image}}"  class="up_images" style="display: block;margin-left: 0%;margin-right: auto !important;width: 191px !important;height: 318px !important; margin-top: -29%;">

                              <?php } ?>   

                              @else

                              <?php if($order->type=='square'){ ?>

                                <style type="text/css">
                                html body .orderdetail img.up_images {
                                  position: absolute;
                                  top: 10px;
                                  margin: 0px !important;
                                  width: 220px !important;
                                  min-width: 255px !important;
                                  height: 255px !important;
                                  left: 2px !important;
                                  max-width: 214px;
                                  }
                                  .orderdetail img.set_frame.saveframe {
                                  width: 100% !important;
                                  max-width: 270px !important;
                                  /* height: 220px !important; */
                                  overflow: hidden;
                                  z-index: 3;
                                  position: relative;
                                  }
                                </style>
                             
                                <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$order->images}}" style="transform: rotate(<?php echo $order->rotation.'deg';?>);display: block;margin-right: auto !important;width: 24%;" >
                                <img  src="{{url('/')}}/public/uploads/order_images/{{$order->tile_image}}"  class="up_images" style="display: block;margin-left: 0%;margin-right: auto !important;width: 191px !important;height: 238px;margin-top: -29%;"> 

                              <?php }else{ ?> 
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$order->images}}" style="transform: rotate(<?php echo $order->rotation.'deg';?>);display: block;margin-left: 13% !important;margin-right: auto !important;height: 270px;
    width: 202px !important;">
                              <img  src="{{url('/')}}/public/uploads/order_images/{{$order->tile_image}}"  class="up_images" style="display: block;    position: absolute;
    top: 41px;
    margin: 0px !important;
    width: 170px !important;
    min-width: 253px !important;
    height: 186px !important;
    left: 2px !important;
    max-width: 214px;">

                              <?php } ?>
                              
                              
                             @endif

                        <?php }?>
                    <?php if($order->alignment=="horizontal"){ ?>
                          @if($order->rotation=='90')
                             
                                  <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$order->images}}" style="transform: rotate(0deg);display: block;margin-right: auto !important;    width: 24%;height: 272px;" >
                                <img  src="{{url('/')}}/public/uploads/order_images/{{$order->tile_image}}"  class="up_images" style="display: block;
    margin-left: 0%;
    margin-right: auto !important;
    width: 191px !important;
    height: 247px;
    margin-top: -31%;"> 

                              @else
                              
                              <img class="set_frame saveframe"  src="{{url('/')}}/public/uploads/frame_images/{{$order->images}}" style="transform: rotate(90deg);    display: block;
    margin-left: 6% !important;
    margin-right: auto !important;
    width: 21%;
    height: 290px;">
                              <img  src="{{url('/')}}/public/uploads/order_images/{{$order->tile_image}}"  class="up_images" style="    display: block;
    margin-left: 0%;
    margin-top: -27%;
    height: 165px;
    width: 275px;">
                             @endif

                        <?php }?>

 </div>


<div class="wid_1ss">
 <span class="sp_lll">
 <p> Tracking Code: </p>  
 <b> {{$order->tracking_code}} </b>
 </span>

 <span class="sp_lll">
 <p> Estimated Delivery Date: </p>  
 <b> {{$order->delivery_date}} </b>
 </span>

</div>

</div>


<div class="edit_pr_section_two_two">
<div class="row">
<div class="col-md-6">

<div class="edit_prlll">
@foreach($shipaddress as $ship)
@if($ship->id == $order->address_id) 
<div class="d1"><h3>Shipping Address</h3>
<strong>{{$ship->address_type}}</strong>
<p>{{$ship->street_address}}, {{$ship->city}}, {{$ship->state}}, {{$ship->zip_code}}
{+ {{$ship->contact_number}} }</p>
</div>

<div class="d1"><h3>Billing Address</h3>

<strong>{{$ship->address_type}}</strong>
<p>{{$ship->street_address}}, {{$ship->city}}, {{$ship->state}}, {{$ship->zip_code}}
{+ {{$ship->contact_number}} }</p>
</div>
@endif
@endforeach

</div>

</div>
<div class="col-md-6">
<div class="edit_prrrr">
<h3>Total Summary</h3>
<div class="dr1">
<p><span class="frmLll">{{$order->quantity}} frames: {{$order->title}} Frame Style</span>  <b> USD <?php echo  $order->total_price - $order->shipping_price; ?> </b> </p>
<p><span class="frmLll">Shipping</span>  <b> USD {{$order->shipping_price}} </b> </p>
<!-- <p><span class="frmLll">Redeem Reward Credits: 10pt</span> <b> RM 10.00 </b> </p> -->

<p class="b_ta"></p>
<p><span class="frmLll">Total</span> <b> USD {{$order->total_price}} </b> </p>
</div>
<div class="dr2">
Paid on {{$order->created_at}} <br>
by online Banking
</div>
</div>
</div>

</div>
</div>


</div>



@endforeach
</div>
</div>
</div>
</div>
            



<div role="tabpanel" class="tab-pane" id="tab4">
              <div class="profile-basic background-white p20">
<div class="profile-basic background-white p20">
                   
                   
                    <div class="edit_pro">
                    <h3 class="ed_pro">Change Password</h3>
                   
                      <div class="">
                      <div class="doc-prfl-left">
                            <div class="row">
                              <div class="form-group">
                                <label class="col-sm-4 col-md-4 col-lg-4 control-label">Old Password:</label>
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                  <input type="password" class="form-control" id="oldpassword" name="oldpassword">
                                    <input type="hidden" class="form-control" name="mobile_no" id="mobile_no" value="9827563277">
                                    <span toggle="#oldpassword" class="pass-view fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-4 col-md-4 col-lg-4 control-label">New Password:</label>
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                  <input type="password" class="form-control" name="newpassword" id="newpassword">
                                   <span toggle="#newpassword" class="pass-view fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-4 col-md-4 col-lg-4 control-label">Confirm Password:</label>
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                  <input type="password" class="form-control" name="conpassword" id="conpassword">
                                  <span toggle="#conpassword" class="pass-view fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                </div>
                              </div>
                              
                              <div class="form-group mng-edit-prf-btn">
                          <label class="col-sm-4 col-md-4 col-lg-4 control-label"></label>
                          <div class="col-sm-8 col-md-8 col-lg-8">
                             <button class="btn btn-primary color-btn" name="change_password_btn" id="change_password_btn">Change Password</button>
                          </div>
                        </div>
                  
                          </div>
                          </div><!-- /.row -->
                        </div>
                        
                    </div>
                    </div>
               </div>
            </div>

              <div role="tabpanel" class="tab-pane" id="tab5">
              <div class="profile-basic background-white p20">

               </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    

    </div>
  </div>
</div>




  </section>
@endsection

@section('page_footer')
  
@endsection