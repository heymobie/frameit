@extends('user.layout.layout')
@section('title', 'User - Profile')

@section('current_page_css')
@endsection

@section('current_page_js')
<script type="text/javascript">
	$.ajaxSetup({
		headers: {'votive':'123456'}
	});
	$(document).ready(function(){
		var formData = new FormData();
		$.ajax({
			type: 'POST',
			url: "<?php echo url('/').'/api/getAllFeedList'; ?>",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(resultData){ 
				var yourArray = [];
				$.each(resultData.response.feedList, function( i, l ){
					//console.log(resultData.response.feedList[i].image_path+'/'+resultData.response.feedList[i].image);
					var feed ='<div class="clip"><div class="testimonial-item"><div class="social-user"><img class="social-icon" src="{{url('/').'/resources/front_assets'}}/img/instagram-logo.png"> <span>'+resultData.response.feedList[i].title+'</span></div><div class="socialBigImg"><img class="image" src="'+resultData.response.feedList[i].image_path+'/'+resultData.response.feedList[i].image+'"></div><div class="info"><div class="text">'+resultData.response.feedList[i].description+'</div></div></div></div>';
					yourArray.push(feed);
				});
				$("#FeedGallery").html(yourArray);
			}
		});
	});
</script>
@endsection

@section('tag_body')
<body>
	@endsection

	@section('content')                            
	<!--==========================
	    Hero Section
	    ============================-->
	    <section id="" class="section home_page_sec">
	    	<div class="sectionWraper">
	    		<div class="hero-container">
	    			<div class="hero-text" data-wow-delay="100ms">
	    				<div class="logoMain wow fadeInLeft"><!-- <img src="{{url('/').'/resources/front_assets'}}/img/logo.png"> --></div>
	    				<div class="bannerText wow fadeInLeft"><span>Turn your walls into storytelling memory lane.</span></div>
	    				<div class="photoImg wow fadeInLeft"><img src="{{url('/').'/resources/front_assets'}}/img/photo.png"><span>8"X10"</span></div>
	    			</div>
	    			<div class="hero-banner " >
	    				<img src="{{url('/').'/resources/front_assets'}}/img/banner.jpg">
	    			</div>
	    		</div>
	    	</div>
	    </section><!-- #hero -->

	  <!--==========================
	    Get Started Section
	    ============================-->

	    <section id="Explore_sec" class="section">
	    	<div class="sectionWraper">
	    		<div class="row">
	    			<div class="col-md-6">
	    				<div class="bnrBotTex wow fadeInUp" data-wow-delay="100ms">
	    					<div class="iconBig">
	    						<!-- <img class="" src="{{url('/').'/resources/front_assets'}}/img/iconBig.jpg"> -->
	    						<img class="" src="{{url('/').'/resources/front_assets'}}/img/iconBig.jpg">
	    					</div>
	    					<div class="iconText"><h2>Hassle Free</h2><p>Sticks on the wall, no tools required </p></div>
	    				</div>
	    			</div>
	    			<div class="col-md-6">
	    				<div class="bnrBotTex wow fadeInUp" data-wow-delay="200ms">
	    					<div class="iconBig"><img class="" src="{{url('/').'/resources/front_assets'}}/img/img_icon_two.jpg"></div>
	    					<div class="iconText"><h2>Re-arrange anytime</h2><p>Removeable and leave no marks </p></div>
	    				</div>
	    			</div>

					<div class="col-md-6">
	    				<div class="bnrBotTex wow fadeInUp" data-wow-delay="100ms">
	    					<div class="iconBig">
	    						<!-- <img class="" src="{{url('/').'/resources/front_assets'}}/img/iconBig.jpg"> -->
	    						<img class="" src="{{url('/').'/resources/front_assets'}}/img/img_icon_three.jpg">
	    					</div>
	    					<div class="iconText"><h2>High Quality Print</h2><p>Bright & Vivid High Definition Print </p></div>
	    				</div>
	    			</div>
	    			<div class="col-md-6">
	    				<div class="bnrBotTex wow fadeInUp" data-wow-delay="200ms">
	    					<div class="iconBig"><img class="" src="{{url('/').'/resources/front_assets'}}/img/img_icon_four.jpg"></div>
	    					<div class="iconText"><h2>Perfect Size</h2><p>As a gift of precious memory or to display at home or office! </p></div>
	    				</div>
	    			</div>



	    		</div>
	    	</div>
	    </section>

	  <!--==========================
	    Screenshots Section
	    ============================-->
<section id="" class="section lastSection">
       <div class="sectionWraper instaSection">
        <div class="container">
          
        	<div class="add_views_sec">
        		<h3 style="font-weight: 700;color:white;">Three frames are RM99</h3>
        		<h4 style="font-size:25px;font-weight: 100;line-height: 2;color:white;">Each additional is rm33</h4>
        		<br>
        		<a href="javascript:void(0)"> <img class="image" src="{{url('/').'/resources/front_assets'}}/img/video_add.jpg"> </a>

        	</div>

          <div class="section-title text-center wow fadeInUp" data-wow-delay="100ms">
            <h2>Explore the wall of memories created with us</h2>
          </div>
        </div>

        <div id="FeedGallery11" class="slider center wow fadeInUp" data-wow-delay="200ms">
          <div class="clip">
            <div class="testimonial-item">
              <!-- <div class="social-user"><img class="social-icon" src="{{url('/').'/resources/front_assets'}}/img/instagram-logo.png"> <span>@HeyItsJenna</span></div> -->
              <div class="socialBigImg"><img class="image" src="{{url('/').'/resources/front_assets'}}/img/instaImg1.jpeg"></div>
              <div class="info">                           
                <div class="text">Easy installation! Easy to move around Looks stunning!</div>
              </div>
            </div>
          </div>
          <div class="clip">
            <div class="testimonial-item">
             <!--  <div class="social-user"><img class="social-icon" src="{{url('/').'/resources/front_assets'}}/img/instagram-logo.png"> <span>@HeyItsJenna</span></div> -->
              <div class="socialBigImg"><img class="image" src="{{url('/').'/resources/front_assets'}}/img/instaImg2.jpeg"></div>
              <div class="info">                           
                <div class="text">Easy installation! Easy to move around Looks stunning!</div>
              </div>
            </div>
          </div>
          <div class="clip">
            <div class="testimonial-item">
             <!--  <div class="social-user"><img class="social-icon" src="{{url('/').'/resources/front_assets'}}/img/instagram-logo.png"> <span>@HeyItsJenna</span></div> -->
              <div class="socialBigImg"><img class="image" src="{{url('/').'/resources/front_assets'}}/img/instaImg3.jpeg"></div>
              <div class="info">                           
                <div class="text">Easy installation! Easy to move around Looks stunning!</div>
              </div>
            </div>
          </div>
          <div class="clip">
            <div class="testimonial-item">
             <!--  <div class="social-user"><img class="social-icon" src="{{url('/').'/resources/front_assets'}}/img/instagram-logo.png"> <span>@HeyItsJenna</span></div> -->
              <div class="socialBigImg"><img class="image" src="{{url('/').'/resources/front_assets'}}/img/instaImg4.jpeg"></div>
              <div class="info">                           
                <div class="text">Easy installation! Easy to move around Looks stunning!</div>
              </div>
            </div>
          </div>
          <div class="clip">
            <div class="testimonial-item">
             <!--  <div class="social-user"><img class="social-icon" src="{{url('/').'/resources/front_assets'}}/img/instagram-logo.png"> <span>@HeyItsJenna</span></div> -->
              <div class="socialBigImg"><img class="image" src="{{url('/').'/resources/front_assets'}}/img/instaImg5.jpeg"></div>
              <div class="info">                           
                <div class="text">Easy installation! Easy to move around Looks stunning!</div>
              </div>
            </div>
          </div>
          <div class="clip">
            <div class="testimonial-item">
             <!--  <div class="social-user"><img class="social-icon" src="{{url('/').'/resources/front_assets'}}/img/instagram-logo.png"> <span>@HeyItsJenna</span></div> -->
              <div class="socialBigImg"><img class="image" src="{{url('/').'/resources/front_assets'}}/img/instaImg6.jpeg"></div>
              <div class="info">                           
                <div class="text">Easy installation! Easy to move around Looks stunning!</div>
              </div>
            </div>
          </div>
        </div>


        <div class="main_bottom_sec">
        	<ul>
        		<li><a href="{{url('/')}}/terms_and_condition"> Terms of use </a></li>
        		<li><a href="{{url('/')}}/privacy_policy"> Privacy policy </a></li>
        		<li><a href="mailto:contact@frameit.com.my" target="_blank"> Contact </a></li>
        	</ul>
		
		</div>


      </div>
    </section>
		@endsection

		@section('page_footer')
		<footer class="footerSectopm wow fadeInUp">
			<div class="footerBtn">
				<!-- <button>Start Here <i class="fa fa-chevron-right"></i></button> -->
				@if(empty(Auth::user()->id))
			      <!-- <a data-toggle="modal" href="javascript:void(0);" data-target="#loginModal">Start Here  <span class="rig_arr"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a> -->
			      <a href="{{url('/step_1')}}">Get Started<span class="rig_arr"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
			    @else
			      <a href="{{(!empty(Auth::user()->id) ? url('/category') : url('/login'))}}">Get Started<span class="rig_arr"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
			    @endif
			</div>
		</footer>
		@endsection