<?php
$action = Route::currentRouteAction();

$action_arr = explode('@', $action);

?>
<style>
	#header{
		box-shadow: 0 3px 7px rgba(0, 0, 0, 0.1);
	}
</style>
<?php $segment1 =  Request::segment(1);  ?>
<!-- <header id="header" class="header header-hide new_h_s"> -->
	<header id="header" class="">
	    <div class="container">

	    	<div class="new_header_main_s">
	    	@if($segment1=="select_frame")
	    		<div class="col-md-4">
	        @else
	        	<div class="col-md-4">
	        @endif		

				<div class="new_h_l">
          @if(!empty(Auth::user()->id))
            <a href="#"><span><!-- <i class="fa fa-user-o" aria-hidden="true"></i> -->
            	<img src="{{url('/').'/resources/front_assets'}}/img/user_orange.png">
           
                  </span> <b>{{ !empty(Auth::user()->first_name)? Auth::user()->first_name:''}}</b>
                  <!-- <strong class="ser_bord"><span class="serq"><i class="fa fa-star-o" aria-hidden="true"></i>
                  </span> {{ !empty(Auth::user()->wallet_balance)? Auth::user()->wallet_balance:''}} pt</strong> -->
              </a>
            @else
            <a href="#"  data-toggle="modal" data-target="#loginModal"><span><i class="fa fa-user-o" aria-hidden="true"></i></span> <b>Sign in</b></a>
            @endif
				</div>

			</div>
        <div class="col-md-4" style="text-align: center;">
<a href="<?php echo url('/'); ?>"><img src="http://votivelaravel.in/newframeit/resources/front_assets/img/logopixtwo.png" class="logo-si"></a>
        </div>
			@if($segment1=="select_frame")
			<div class="col-md-4">
		      <div id="logo" class="wow fadeInUp" data-wow-delay="300ms">
		         <center><h3 style="font-size: 20px;padding-top: 10px;">
		            Frame Style
		         </h3></center>
		        
		      </div>
		  </div>
		  @endif
	      @if($segment1=="select_frame")
	    		<div class="col-md-4">
	        @else
	        	<div class="col-md-4">
	        @endif
	      	
			<div class="right_side_m">
<!-- 			<div id="mySidenav" class="sidenav">
			  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			   <div class="side_bot_s">
        
      <div class="new_h_l rig_is">
        @if(!empty(Auth::user()->id))
        <a href="#"><span><i class="fa fa-user-o" aria-hidden="true"></i></span> <b>{{ !empty(Auth::user()->first_name)? Auth::user()->first_name:''}}</b></a>
        @else
        <a href="#"  data-toggle="modal" data-target="#loginModal"><span><i class="fa fa-user-o" aria-hidden="true"></i></span> <b>Sign in</b></a>
        @endif
      </div>


      <div class="side_bot_s_b">

      <p><a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModalCenterFaq"><span><img src="{{url('/')}}/resources/front_assets/img/help.png"></span> <b> Frequent Questions</b></a></p>
      <p><a href="{{url('/profile')}}"><span><img src="{{url('/')}}/resources/front_assets/img/user.png"></span> <b> Manage Account</b></a></p>
      <p><a href="{{url('/order_history')}}"><span><img src="{{url('/')}}/resources/front_assets/img/cart.png"></span> <b> My Orders</b></a></p>
      @if(!empty(Auth::user()->id))
        <p><a href="#" data-toggle="modal" data-target="#rewardnew"><span><img src="{{url('/')}}/resources/front_assets/img/star.png"></span> <b> My Rewards</b> </a>  
        <strong> <img src="{{url('/')}}/resources/front_assets/img/str_i.png">{{ !empty(Auth::user()->wallet_balance)? Auth::user()->wallet_balance:''}} pt</strong></p>
        <p><a href="{{url('/logout')}}"><span><img src="{{url('/')}}/resources/front_assets/img/user.png"></span> <b> Logout</b></a></p>
      @else
        <p><a href="#"><span><img src="{{url('/')}}/resources/front_assets/img/star.png"></span> <b> My Rewards</b> </a></p>
      @endif
      </div>

      <div class="side_bot_s_bbb">
      <p>THE PANTRY</p>
      <span>Legal</span>
      </div>


      </div>
			</div>	 -->
				<span style="font-size:30px;cursor:pointer" onclick="openNav()"> </span>

        <button type="button" class="btn btn-info btn-lg side_bbut" data-toggle="modal" data-target="#side_menu_popup">&#9776;</button>
			</div>

	      </div>	
	  	</div>

	    </div>
	  </header>
	  <!-- #header -->

	  
  <!-- Modal -->
  <div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content login_popo">
        
        <div class="modal-body">
          <div class="stp_one">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="content">
          <div class="bg-layer">
            <div class="bg-circle"></div>
          </div>          
          <div id="logreg-forms">
            <form class="log_form" id="login_form" method="post" action="#">
              <div id="loginResBox">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> {{ Session::get('message') }}</div>        
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> {{ Session::get('error') }}</div>
                @endif
              </div>
              @csrf
              <div class="form_on_s">
              <div class="form-label">
                <img src="{{url('/').'/resources/front_assets'}}/img/img_logoLogin.png">                
              </div>              
              <div class="in_firs">
                <span class="suIcon">
                  <img src="{{url('/').'/resources/front_assets'}}/img/img_loUser.png">
                </span>
                <input class="FormInput" type="email" name="email" placeholder="Username">
              </div>
              <div class="in_firs">
                <span class="suIcon">
                  <img src="{{url('/').'/resources/front_assets'}}/img/img_lolk.png">
                </span>
                <input class="FormInput" type="password" name="password" placeholder="Password">
              </div>
            </div>
              <div class="button_cont">
                <input class="btn_Submit" type="submit" value="Sign In">
              </div>
              <div class="in_firsSign">
               <a data-toggle="modal" href="javascript:void(0);" data-target="#signupModal" id="signp">Sign Up</a>
                <a href="javascript:void(0);" data-toggle="modal" data-target="#forgotPassModal" id="forgetp" class="trigt">Forget login ?</a>
              </div>              
           

            <div class="or-line">
             <span>Or</span>
           </div>

            <div class="bnBtn">
              <div class="social-login">
                <a href="{!! url('auth/facebook') !!}">
                  <button class="btn facebook-btn social-btn" type="button">
                    <span>
                      <img src="{{url('/').'/resources/front_assets'}}/img/icon_fbOne.png"> Continue with Facebook
                    </span>
                  </button>
                </a>
              </div>
              <div class="social-login">
                <a href="{{ url('/redirect') }}">
                  <button class="btn google-btn social-btn" type="button">
                    <span>  <img src="{{url('/').'/resources/front_assets'}}/img/icon_gOne.png"> Continue with Google</span>
                  </button>
                </a>
              </div>  
            </div> 
             </form>           
          </div>
        </div>
      </div>
        </div>
    <!--<div class="modal-footer login_popth">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>

    <div id="signupModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">      
      <div class="modal-content" style="display: inline-table;">        
        <div class="modal-body">
          <div class="logreg-forms">
            <div id="signupResBox"></div>
            <form class="sign_form" id="signup_form" method="post" action="#">
             <button type="button" class="close" data-dismiss="modal">&times;</button>  
             @csrf
             <div class="form-label">Sign Up</div>
             <h4 class="bel_hd">Please enter details:</h4>
             <div class="in_firs">
              <input class="FormInput" type="text" name="name" placeholder="Name">
            </div>
            <div class="in_firs">
              <input class="FormInput" type="email" name="email" placeholder="Email">
            </div>
            <div class="in_firs">
              <input class="FormInput" type="password" id="password" name="password" placeholder="Password">
            </div>
            <div class="in_firs">
              <input class="FormInput" type="password" name="confirm_password" placeholder="Confirm Password">
            </div>
            <div class="in_firs">
              <input class="FormInput" type="text" name="contact_number" placeholder="Phone Number">
            </div>
            <div class="button_cont">
              <input class="btn_Submit" type="submit" value="Sign Up">
            </div>
          </form>
        </div>
      </div>    
    </div>
  </div>
</div>

<!-- Modal -->
<div id="forgotPassModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">     
    <div class="modal-content" style="display: inline-table;">        
      <div class="modal-body">
        <div class="logreg-forms">
          <div id="forgotPassResBox"></div>
          <form class="sign_form" id="forgot_pass_form" method="post" action="#">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            @csrf
            <div class="form-label" style="padding-top:15% ">Forget Username</div>  
            <h4 class="bel_hd">Please enter email address:</h4>              
            <div class="in_firs">
              <input class="FormInput" type="email" name="email" placeholder="Email">
            </div>
            <div class="button_cont">
              <input class="btn_Submit" type="submit" value="Submit">
            </div>
          </form>
        </div>        
      </div>

    </div>
  </div>
</div>

<!-- Faq Modal -->
<div class="modal fade pop_address" id="exampleModalCenterFaq" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">        
            <div class="modal-body">
              <div class="address-form">
                <div class="top-bar-container">
                  <div class="top-bar">
                    <div class="left-comp">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times; <span class="cls_btn">Close</span></span>
                      </button>        
                    </div>
                    <div class="title_fgh">Frequent Questions</div>                  
                  </div>
                  <div class="bottom-comp"></div>
                </div>

                <ul class="list_fq scrollbar_s faqList" id="style-1">
                  
                </ul>              
              </div>     
            </div>        
          </div>
        </div>
      </div>





<!-- Modal -->
  <div class="modal fade pop_cart popop_right_s" id="side_menu_popup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
         

 <div id="mySidenav" class="sidenav">
       <!--  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> -->
         <div class="side_bot_s">
        
      <div class="new_h_l rig_is">
        @if(!empty(Auth::user()->id))
        <a href="#"><span>
        @if(!empty(Auth::user()->profile_pic))
          <img src="{{Auth::user()->profile_pic}}" style="width: 30px;
    height: 30px;
    border-radius: 50%;">
        @else
          <img src="{{url('/').'/resources/front_assets'}}/img/user_white.png">
        @endif  
        </span> <b>{{ !empty(Auth::user()->first_name)? Auth::user()->first_name:''}}</b></a>
        <p>Not you ? <a href="{{url('/logout')}}"><span></span> <b> Signout <i class="fa fa-angle-right" aria-hidden="true"></i></b></a></p>
        @else
        <a href="#"  data-toggle="modal" data-target="#loginModal"><span><i class="fa fa-user-o" aria-hidden="true"></i></span> <b>Sign in</b></a>
        @endif
      </div>


      <div class="side_bot_s_b">

      <p><a href="javascript:void(0);" id="faq" data-toggle="modal" data-target="#exampleModalCenterFaq"><b> Frequent Questions</b></a></p>
      <p><a href="{{url('/profile')}}"><b> Manage Account</b></a></p>
      <p><a href="{{url('/my_order')}}"><b> My Orders</b></a></p>
      @if(!empty(Auth::user()->id))
        <!-- <p><a href="{{url('/')}}/my-rewards" ><span><img src="{{url('/')}}/resources/front_assets/img/star.png"></span> <b> My Rewards</b> </a>  
        <strong> <img src="{{url('/')}}/resources/front_assets/img/str_i.png">{{ !empty(Auth::user()->wallet_balance)? Auth::user()->wallet_balance:''}} pt</strong></p> -->
        <!-- <p><a href="{{url('/logout')}}"><span><img src="{{url('/')}}/resources/front_assets/img/user.png"></span> <b> Logout</b></a></p> -->
      @else
       <!--  <p><a href="#"><span><img src="{{url('/')}}/resources/front_assets/img/star.png"></span> <b> My Rewards</b> </a></p> -->
      @endif
      </div>

      <div class="side_bot_s_bbb">
      <p>THE PANTRY</p>
      <span>Legal</span>
      </div>


      </div>
      </div>



        </div>
    
      </div>
      
    </div>
  </div>