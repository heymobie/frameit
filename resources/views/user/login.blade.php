@extends('user.layout.layout')
@section('title', 'User - Profile')

@section('current_page_css')
<style type="text/css">
#logreg-forms .social-login{
  width:390px;
  margin:0 auto;
  margin-bottom: 14px;
}
#logreg-forms .social-btn{
  font-weight: 100;
  color:white;
  /*width:190px;*/
  font-size: 0.9rem;
}

#logreg-forms .facebook-btn{  background-color:#3C589C; }

#logreg-forms .google-btn{ background-color: #DF4B3B; }

#logreg-forms .form-signup .social-btn{ width:210px; }

.form-signup .social-login{
  width:210px !important;
  margin: 0 auto;
}

@media screen and (max-width:500px){
  #logreg-forms  .social-login{
    width:200px;
    margin:0 auto;
    margin-bottom: 10px;
  }
  #logreg-forms  .social-btn{
    font-size: 1.3rem;
    font-weight: 100;
    color:white;
    width:200px;
    height: 56px;
    
  }
  #logreg-forms .social-btn:nth-child(1){
    margin-bottom: 5px;
  }
  #logreg-forms .social-btn span{
    display: none;
  }
  #logreg-forms  .facebook-btn:after{
    content:'Facebook';
  }
  
  #logreg-forms  .google-btn:after{
    content:'Google+';
  }
  
}

#logreg-forms .facebook-btn {
  background-color: #3C589C;
}
</style>
@endsection

@section('current_page_js')

<script type="text/javascript">
  $('#signup_form').validate({ 
      // initialize the plugin
      rules: {
       name: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength : 6
      },
      confirm_password: {
        required: true,
        equalTo : "#password"
      },
      contact_number: {
        required: true,
        digits:true,
        minlength : 8,
        maxlength : 13
      },
    },
    submitHandler: function(form) {
         //form.submit();
         register();
       }
     });

  $.ajaxSetup({
    headers: {'votive':'123456'}
  });

  function register() {
    var $this = $('form#signup_form')[0];
    var formData = new FormData($this);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/user_register'; ?>",
      data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          console.log(resultData);
          if (resultData.status) {
            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';
            $("#signupResBox").html(result_alert);
            document.getElementById("signup_form").reset();
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#signupResBox").html(result_alert);
          }
        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#signupResBox").html(result_alert);
        }
      });
  }

  $('#forgot_pass_form').validate({ 
      // initialize the plugin
      rules: {
       email: {
        required: true,
        email: true
      },
      password: {
        required: true
      }
    },
    submitHandler: function(form) {
         //form.submit();
         forgot_password();
       }
     });

  $('#login_form').validate({ 
      // initialize the plugin
      rules: {
       email: {
        required: true,
        email: true
      },
      password: {
        required: true
      }
    },
    submitHandler: function(form) {
         //form.submit();
         login();
       }
     });

  function login() {
    var $this = $('form#login_form')[0];
    var formData = new FormData($this);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/login'; ?>",
      data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          console.log(resultData);
          if (resultData.status) {
            //resultData.data.id
            document.getElementById("login_form").reset();
            window.location.href = "<?php echo url('/custom_auth'); ?>"+"/"+resultData.data.id;
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#loginResBox").html(result_alert);
          }
        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#loginResBox").html(result_alert);
        }
      });
  }

  function forgot_password() {
    var $this = $('form#forgot_pass_form')[0];
    var formData = new FormData($this);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/forgot_password'; ?>",
      data: formData,
        //dataType: "text",
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 
          console.log(resultData);
          if (resultData.status) {
            //resultData.data.id
            document.getElementById("forgot_pass_form").reset();
            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#forgotPassResBox").html(result_alert);
          } else {
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            
            $("#forgotPassResBox").html(result_alert);
          }
        },
        error: function(errorData) { 
          console.log(errorData);
          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          
          $("#forgotPassResBox").html(result_alert);
        }
      });
  }
  
</script>

@endsection

@section('tag_body')
<body class="bx_logIn">
  @endsection

  @section('content')
  <section id="" class="section selectFramStyles bx_logMain">
    <div class="container">
      <div class="selectFrameSec">
        <!-- <h2>Get Started</h2> -->
      </div>
      <div class="stp_one">
        <div class="content">
          <div class="bg-layer">
            <div class="bg-circle"></div>
          </div>          
          <div id="logreg-forms">
            <form class="log_form" id="login_form" method="post" action="<?php echo url('/').'/weblogin'; ?>">
              <div id="loginResBox">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> {{ Session::get('message') }}</div>        
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> {{ Session::get('error') }}</div>
                @endif
              </div>
              @csrf
              <div class="form-label">
                <img src="http://votivelaravel.in/frameit/resources/front_assets/img/img_logoLogin.png">                
              </div>              
              <div class="in_firs">
                <span class="suIcon">
                  <img src="http://votivelaravel.in/frameit/resources/front_assets/img/img_loUser.png">
                </span>
                <input class="FormInput" type="email" name="email" placeholder="User name">
              </div>
              <div class="in_firs">
                <span class="suIcon">
                  <img src="http://votivelaravel.in/frameit/resources/front_assets/img/img_lolk.png">
                </span>
                <input class="FormInput" type="password" name="password" placeholder="Pasword">
              </div>
              <div class="button_cont">
                <input class="btn_Submit" type="submit" value="Sign In">
              </div>
              <div class="in_firsSign">
                <a data-toggle="modal" href="javascript:void(0);" data-target="#signupModal">Signup</a>
                <a href="javascript:void(0);" data-toggle="modal" data-target="#forgotPassModal" class="trigt">Forgot Password</a>
              </div>              
            </form>

            <div class="bnBtn">
              <div class="social-login">
                <a href="{!! url('auth/facebook') !!}">
                  <button class="btn facebook-btn social-btn" type="button">
                    <span>
                      <img src="http://votivelaravel.in/frameit/resources/front_assets/img/icon_fbOne.png"> Continue with Facebook
                    </span>
                  </button>
                </a>
              </div>
              <div class="social-login">
                <a href="{{ url('/redirect') }}">
                  <button class="btn google-btn social-btn" type="button">
                    <span>  <img src="http://votivelaravel.in/frameit/resources/front_assets/img/icon_gOne.png"> Continue with Google</span>
                  </button>
                </a>
              </div>  
            </div>            
          </div>
        </div>
      </div>
      <div class="framesTabing">
        <div class="sectionWraper"></div>
      </div>
    </div>
  </section>

  <div id="signupModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">      
      <div class="modal-content" style="display: inline-table;">        
        <div class="modal-body">
          <div class="logreg-forms">
            <div id="signupResBox"></div>
            <form class="sign_form" id="signup_form" method="post" action="#">
             <button type="button" class="close" data-dismiss="modal">&times;</button>  
             @csrf
             <div class="form-label">Sign Up</div>
             <h4 class="bel_hd">Please enter details:</h4>
             <div class="in_firs">
              <input class="FormInput" type="text" name="name" placeholder="Name">
            </div>
            <div class="in_firs">
              <input class="FormInput" type="email" name="email" placeholder="Email">
            </div>
            <div class="in_firs">
              <input class="FormInput" type="password" id="password" name="password" placeholder="Pasword">
            </div>
            <div class="in_firs">
              <input class="FormInput" type="password" name="confirm_password" placeholder="Confirm Password">
            </div>
            <div class="in_firs">
              <input class="FormInput" type="text" name="contact_number" placeholder="Phone Number">
            </div>
            <div class="button_cont">
              <input class="btn_Submit" type="submit" value="Sign Up">
            </div>
          </form>
        </div>
      </div>    
    </div>
  </div>
</div>






<!-- Modal -->
<div id="forgotPassModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">     
    <div class="modal-content" style="display: inline-table;">        
      <div class="modal-body">
        <div class="logreg-forms">
          <div id="forgotPassResBox"></div>
          <form class="sign_form" id="forgot_pass_form" method="post" action="#">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            @csrf
            <div class="form-label">Forget Password</div>  
            <h4 class="bel_hd">Please enter email address:</h4>              
            <div class="in_firs">
              <input class="FormInput" type="email" name="email" placeholder="Email">
            </div>
            <div class="button_cont">
              <input class="btn_Submit" type="submit" value="Submit">
            </div>
          </form>
        </div>        
      </div>

    </div>
  </div>
  @endsection

  @section('page_footer')
  
  @endsection