@extends('user.layout.layout')
@section('title', 'User - Profile')

@section('current_page_css')
@endsection

@section('current_page_js')
@endsection

@section('tag_body')
  <body class="step_main">
@endsection

@section('content')
	<section id="" class="section selectFramStyles">

     <div class="container">
    <div class="selectFrameSec">
      <h2>3/3</h2>        
    </div>
    <div class="stp_one">
      <div class="content">
        <div class="bg-layer">
          <div class="bg-circle"></div>
        </div>

        <div id="second_step_form"></div>
        <form class="log_form" action="#" method="post" id="third_step_form">
          <div class="form-label">Hey There!</div>
          <div class="in_firs">
            <input class="FormInput" type="email" name="email" placeholder="What's your email?" value="">
          </div>
          <div class="button_cont">            
            <input class="btn_Submit" type="submit" value="Continue">
          </div>
        </form>
      </div>
    </div>
    <div class="framesTabing">
      <div class="sectionWraper"></div>
    </div>
  </div>
  </section> 
@endsection

@section('page_footer')
	
@endsection