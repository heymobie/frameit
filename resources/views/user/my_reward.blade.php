@extends('user.layout.layout')
@section('title', 'User - Reward')

@section('current_page_css')
@endsection

@section('current_page_js')

<?php  $user_id=Auth::user()->id; ?>
<script type="text/javascript">

  //start get ship address list

    function reffercode() {

    var name = $('#name').val();
    var referral_email_from=$('#referral_email_to').val();
    var referral_url=$('#referral_url').val();
    //alert(referral_email_from);
    $.ajax({

      type: "POST",

      dataType: "json",

      url: "<?php echo url('/sendrefferalcode'); ?>",

      data: {'name':name,'referral_email_from':referral_email_from,'referral_url':referral_url},

      success: function(data){ 

        if(data.status=='success'){ 

          $('#mymsg1').html('<p style="color:green;font-size:18px"><b>send successfully !</b></p>'); 
          

        }else{
          $('#mymsg1').html('<p style="color:red;font-size:18px">'+data.msg+'</p>'); 
          // alert();

        }

      }

    });

  }
</script>

<script type="text/javascript">

  $.ajaxSetup({
    headers: {'votive':'123456'}
  });
  
  var user_id = "<?php echo $user_id ?>";
  $(document).ready(function(){
    var formData = new FormData();
    formData.append('user_id',user_id);
    //alert(id);
    $.ajax({
      type: 'POST',
      //beforeSend: function(xhr){xhr.setRequestHeader('votive':'123456');},
      url: "<?php echo url('/').'/api/getuserdetails'; ?>",
      data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          //console.log(resultData);
          if(resultData.status){
          $("#first_name").val(resultData.response.first_name);
          $("#last_name").val(resultData.response.last_name);
          //$("#email").val(resultData.response.email);
          $("#contact_number").val(resultData.response.contact_number);

          if(resultData.response.date_of_birth){
            $("#date_of_birth").val(resultData.response.date_of_birth);
          }else{
            $("#date_of_birth").val();
          }

          if(resultData.response.gender){
            $("#gender").val(resultData.response.gender);
          }else{
            $("#gender").val();
          }

          if(resultData.response.address){
            $("#address").val(resultData.response.address);
          }else{
            $("#address").val();
          }

         if(resultData.response.profile_pic){          

              $("#blah").attr("src",resultData.response.profile_pic);
          }else{
            $("#blah").attr("src","http://localhost/frameit/resources/front_assets/img/user_image.jpg");
          }

          $("#err_msg").html(result_alert);


              }

           },error: function(errorData) { 
              console.log(errorData);
              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
              
              $("#err_msg").html(result_alert);
            }
          });
    });
   

    $('#editProfile_form').validate({ 
      // initialize the plugin
      rules: {
       first_name: {
        required: true
      },
      last_name: {
        required: true
      },
      gender:{
        required: true,
      },
      date_of_birth:{
        required: true,
      },
      address:{
        required: true,
      },
      contact_number: {
        required: true,
        digits:true,
        minlength : 8,
        maxlength : 13
      },
    },
    submitHandler: function(form) {
         //form.submit();
         update_profile();
       }
    });

    $.ajaxSetup({
      headers: {'votive':'123456'}
    });

    function update_profile() {
      var $this = $('form#editProfile_form')[0];
      var formData = new FormData($this);
      //var user_id = "<?php //echo $user_id ?>";
      //formData.append('user_id',user_id);
      console.log(formData);
      $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/updateprofile'; ?>",
        data: formData,
          //dataType: "text",
          enctype: 'multipart/form-data',
          cache:false,
          contentType: false,
          processData: false,
          success: function(resultData) { 
            console.log(resultData);
            if (resultData.status) {
              var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';
              $("#signupResBox").html(result_alert);
              window.location.href = "<?php echo url('/edit-profile'); ?>";
              //document.getElementById("signup_form").reset();
            } else {
              var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
              
              $("#signupResBox").html(result_alert);
            }
          },
          error: function(errorData) { 
            //console.log(errorData);
            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            
            $("#signupResBox").html(result_alert);
          }
        });
    }

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#imgInp").change(function() {
      readURL(this);

     var file = this.files[0];
        var fileType = file.type;
        var match = ['image/jpeg', 'image/png', 'image/jpg'];
        if(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]))){
            alert('Sorry, only JPG, JPEG, & PNG files are allowed to upload.');
            $("#imgInp").val('');
            return false;
        }
  });
  
  $( function() {
    //$( "#date_of_birth" ).datepicker();
    var date = $('#date_of_birth').datepicker({ dateFormat: 'yy-mm-dd' }).val();
  } );

</script>
<script type="text/javascript">
 function alertmsg(){
  alert("You don't have referral code..");
  }
</script>

<script type="text/javascript">
  //start get user order list
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });

  var user_id = "<?php echo $user_id ?>";
  
  $(document).ready(function(){
    var formData = new FormData();
    formData.append('user_id',user_id);
    $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/getAllUserOrderList'; ?>",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          //console.log(resultData);
          if(resultData.status){

            var orderArray = [];
            var j=1;
            $.each(resultData.response.orderList, function( i, l ){
              var order_id = resultData.response.orderList[i].order_id;
              var site_url = "<?php echo url('/').'/order_details'; ?>";
              //var orders = '<tr><td>'+j+'</td><td>'+resultData.response.orderList[i].order_id+'</td><td>'+resultData.response.orderList[i].frame_name+'</td><td>'+resultData.response.orderList[i].total_tile+'</td><td>'+resultData.response.orderList[i].total_price+' MYR</td><td>'+resultData.response.orderList[i].tracking_code+'</td><td>'+resultData.response.orderList[i].order_status+'</td><td>'+resultData.response.orderList[i].delivery_date+'</td><td>'+resultData.response.orderList[i].created_at+'</td><td><a href="'+site_url+'/'+order_id+'">View Details</a></td></tr>';


              var orders = '<div class="edit_pr_section_one"><div class="col-md-2"><div class="section_one_nn"><p> Order id:</p><strong>'+resultData.response.orderList[i].order_id+'</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p>Placed one</p><strong>'+resultData.response.orderList[i].created_at+'</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p> Items</p><strong>'+resultData.response.orderList[i].total_tile+' Frames '+resultData.response.orderList[i].frame_name+' Frame Style</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p> Paid on</p><strong>'+resultData.response.orderList[i].created_at+'</strong></div></div><div class="col-md-2"><div class="section_one_nn"><p> Total</p><strong>MYR '+resultData.response.orderList[i].total_price+' </strong></div></div><div class="col-md-2"><div class="section_one_nn"><a href="#" class="mang_ac">Manage</a></div></div></div>';

              orderArray.push(orders);
              j++;
            });
            $(".orderList").html(orderArray);

          }else{

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> No order Found.</div>';
            $("#err_msg").html(result_alert);

          }

        },error: function(errorData) { 

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            $("#err_msg").html(result_alert);

        }
    });
  });
  //end get user order list
  </script>

  <script type="text/javascript">

  //start get ship address list

    function applycode() {

    var user_id = $('#user').val();
    var code=$('#code').val();
    
    $.ajax({

      type: "POST",

      dataType: "json",

      url: "<?php echo url('/api/userApplyCouponCode'); ?>",

      data: {'user_id':user_id,'coupon_code':code},

      success: function(data){ 

        if(data.status=='success'){ 

          $('#mymsg').html('<p style="color:green">Apply coupon successfully !</p>'); 
           //var delay = 4000; 

          //setTimeout(function(){ window.location.href = "<?php echo url('/my-rewards'); ?>"; }, delay);

        //window.location.href = "<?php //echo url('/my-rewards'); ?>"; 

        }else{
          $('#mymsg').html('<p style="color:red">'+data.msg+'</p>'); 
          // alert();

        }

      }

    });

  }
</script>

 
@endsection

@section('tag_body')
  <body>
@endsection

@section('content')
  <section id="" class="section bx_mainCrop">
    <div class="selectFrameSec">
    
 <!--<ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home" class="active show"><div class="frmStyl"><img src="img/cleanIcon.svg"><span>Clean</span></div></a></li>
        <li><a data-toggle="tab" href="#menu1" cl><div class="frmStyl "><img src="img/everIcon.svg"><span>Ever</span></div></a></li>
        <li><a data-toggle="tab" href="#menu2"><div class="frmStyl"><img src="img/boldIcon.svg"><span>Bold</span></div></a></li>
        <li><a data-toggle="tab" href="#menu3"><div class="frmStyl"><img src="img/classicIcon.svg"><span>Classic</span></div></a></li>
      </ul>   --> 
    </div>

 <div id="my_profile_sec" class=" area-padding">
  <div class="container">
  <div class="inner_secPro">

    <div class="row  wow slideInUp" data-wow-delay=".1s">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="section-headline text-center">
         <!--  <h2><span>Edit</span> Profile</h2> -->

        <div class="inn_h_secc">
                <a href="{{url('/')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to homepage</a>
                <h3>My Reward</h3>
              </div>

        </div>
      </div>
    </div>
    <div class="col-lg-12" role="tabpanel">
      <div class="row manag-tabs">
       
        <div class="col-sm-2">
          <ul class="nav nav-pills brand-pills nav-stacked" role="tablist">
            <?php

            $currentPath= Route::getFacadeRoot()->current()->uri(); 
            if($currentPath=='edit-profile'){
              $classAct='active';
            }else{
              $classAct='';
            } 
            // $currentPath= Route::getFacadeRoot()->current()->uri(); 
            // if($currentPath=='dashboard'){
            //   $classAct='active';
            // }else{
            //   $classAct='';
            // }

            // if($currentPath=='profile'){
            //   $classAct='active';
            // }else{
            //   $classAct='';
            // }

            ?>
            <!-- <li class="brand-nav "><a href="#" aria-controls="tab1" role="tab" data-toggle="tab"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li> -->
            <li class="brand-nav"><a href="{{url('/profile')}}" aria-controls="tab2" role="tab" ><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li>

            <li class="brand-nav <?php echo $classAct; ?>"><a href="{{url('/my_order')}}" aria-controls="tab3" role="tab" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> My Order </a></li>
           

<!-- <li class="brand-nav active <?php// echo $classAct; ?>"><a href="{{url('/my-rewards')}}" aria-controls="tab4" role="tab" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>My Rewards </a></li>
 -->
<!--              <li class="brand-nav active">
            <a href="#" aria-controls="tab4" role="tab"><i class="fa fa-sign-out" aria-hidden="true"></i> My Rewards</a></li>
 -->
           <!--  <li class="brand-nav"><a href="{{url('/logout')}}" aria-controls="tab5" role="tab"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li> -->
          </ul>
        </div>
     
        <div class="col-sm-10">
          <div class="tab-content">
          
            <div role="tabpanel" class="tab-pane " id="tab1">
              <div class="profile-basic background-white p20">
                
                <form>
                  <div class="prof-img">
                    <div class="user-photo"> <a href="#"> <img src="img/user_image.jpg" alt="User Photo"> </a> </div>
                  </div>
              
                  <div class="prsn-info my-profl ne_ur_pr">
                  
                  <div class="row">
                    
                  <div class="usr_nm">
                  <h2>John Deo</h2>
                 
        </div>
                         
                  <div class="form-group col-lg-12">
                   <p><label>Country </label><strong>USA</strong></p>
                  </div>
                  
                  <div class="form-group col-lg-12">
                   <p><label>Status </label><strong>Ready to Volunteer</strong></p>
                  </div> 
                      
                      
                      
                  </div>
                  
                    <!-- /.row --> 
                  </div>
                </form>
                
                
                <div class="bottom_ur_dashbo">
                <div class="col-md-12 wow slideInUp" data-wow-delay=".2s">
               
                <div class="row">
                
                <h2>Welcome to frameit</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
               
                <p><strong>Lorem Ipsum is simply dummys?</strong>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                </p>
                
                </div>
                
                </div>
                </div>
                
              </div>
            </div>
            
            <div role="tabpanel" class="tab-pane" id="tab2">
              <div class="profile-basic background-white p20">
                <div class="row">


              <div class="profile-basic background-white p20 np20">
                <form>
                  <div class="prof-img">
                    <div class="user-photo"> <a href="#"> <img src="img/user_image.jpg" alt="User Photo"> </a> </div>
                        <div class="full-width">
            
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <input type="file" id="main-input" class="form-control form-input form-style-base">
                            <h4 id="fake-btn" class="form-input fake-styled-btn text-center truncate"><span class="margin"> Choose File</span></h4>
                        </div>

                    </div>
                  </div>
                  <div class="prsn-info my-profl ne_ur_pr">
                    <div class="row">
                      <div class="usr_nm">
                        <h2>John Deo</h2>
                         <span class="pro_edit"><a href="#"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit </a></span>
                        
                      <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>User Name </label>
                          <strong>@johndeo</strong></p>
                      </div>
                      <!-- /.form-group -->
                      
                      <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>Email </label>
                          <strong>johndeo@gmail.com</strong></p>
                      </div>
                      <!-- /.form-group -->
                      
                      <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>Phone Number </label>
                          <strong>+91 1234 112 121</strong></p>
                      </div>
                      <!-- /.form-group --> 

                       <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>Date of Birth </label>
                          <strong>Dec / 18 / 2018</strong></p>
                      </div>

                       <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>Gender </label>
                          <strong>Male</strong></p>
                      </div>

                        <div class="form-group col-lg-12 clear_b">
                        <p>
                          <label>Address </label>
                          <strong>Lorem Ipsum is simply dummy </strong></p>
                      </div>
                      
                    </div>
                    <!-- /.row --> 
                  </div>
                
              </div>
                </form>


                <div class="bottom_ur_pro">
                                  
                                  <div class="col-md-12 wow slideInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInUp;">
          <div class="row">

            

            <div class="projt-list">

              <div class="tab-menu">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li class="active">
                    <a href="#p-view-1" role="tab" data-toggle="tab" aria-expanded="true">About</a>
                  </li>
                  <li class="">
                    <a href="#p-view-2" role="tab" data-toggle="tab" aria-expanded="false">EXPERIENCE</a>
                  </li>
                  
                </ul>
              </div>
           
              <div class="tab-content">
              
                <div class="tab-pane active" id="p-view-1">
                  <div class="tab-inner">
                    <div class="event-content head-team">
                    
                    <div class="use_bot_one">
                      <div class="row">
                    <div class="col-md-3">
                    <div class="use_bot_on_tx">
                   <strong> Biography</strong>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="use_bot_on_tx">
                    <p>Lorem Ipsum is simply dummy.</p>
                    </div>
                    </div>
                    <div class="col-md-1">
                    <div class="use_bot_on_tx">
                    <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                    </div></div>
                    </div> 
                    
                    <div class="use_bot_one">
                       <div class="row">
                    <div class="col-md-3">
                    <div class="use_bot_on_tx">
                    <strong> Links</strong>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="use_bot_on_tx">
                    <p>Lorem Ipsum is simply dummy.</p>
                    </div>
                    </div>
                    <div class="col-md-1">
                    <div class="use_bot_on_tx">
                    <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                    </div>
                    </div></div>
                    
                    <div class="use_bot_one">
                       <div class="row">
                    <div class="col-md-3">
                    <div class="use_bot_on_tx">
                    <strong> Skills</strong>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="use_bot_on_tx">
                    <p>Lorem Ipsum is simply dummy.</p>
                    </div>
                    </div>
                    <div class="col-md-1">
                    <div class="use_bot_on_tx">
                    <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                    </div>
                    </div></div>
                    
                    <div class="use_bot_one">
                       <div class="row">
                    <div class="col-md-3">
                    <div class="use_bot_on_tx">
                    <strong> Networks</strong>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="use_bot_on_tx">
                    <p>Lorem Ipsum is simply dummy.</p>
                    </div>
                    </div>
                    <div class="col-md-1">
                    <div class="use_bot_on_tx">
                    <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                    </div></div>
                    </div>
                    
                    <div class="use_bot_one">
                       <div class="row">
                    <div class="col-md-3">
                    <div class="use_bot_on_tx">
                    <strong> Causes</strong>
                    </div>
                    </div>
                    <div class="col-md-8">
                    <div class="use_bot_on_tx">
                    <p>Lorem Ipsum is simply dummy.</p>
                    </div>
                    </div>
                    <div class="col-md-1">
                    <div class="use_bot_on_tx">
                    <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                    </div></div>
                    </div>
                      
                    </div>
                  </div>
                </div>
                
                <div class="tab-pane" id="p-view-2">
                  <div class="tab-inner">
                    <div class="event-content head-team">                      
                    <div class="exp_tab_sec">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                    
                    </div>
                   
                      
                    </div>
                 </div>
                 
                </div>
                
              </div>

            </div>

          </div>
        </div>
                                  
                                  
                                  </div>
            
              </div>


                </div><!-- /.row -->


              </div>
            </div>
            
            <div role="tabpanel" class="tab-pane " id="tab3">

              <div class="profile-basic background-white p20 ed_p my_rewd">
              

                <div class="edit_pro">
 
                   <div class="tab_four_sec">
                   <div class="col-md-6">
                   <div class="tab_four_sec_l"> 
                   <h3>Share & get rewarded</h3> 
               <p class="shr">Share this Product with your friends and earn Reward Credits for a discount your referral make a purchase from us.</p>
                <p class="adl_ad sharin_se12">
                 @if(Auth::user()->my_referral_code==NULL)
                    <a href="#" onclick="alertmsg()">Start Sharing</a>
                  @else
                     <a href="#" class="sharin_se">Start Sharing</a>
                  @endif
                </p>


                   </div>
                   </div>


                   <div class="col-md-6">
                    <div class="tab_four_sec_r">  

                     
                   </div></div>
                  </div>





                </div>
              </div>
            </div>
            















            <div role="tabpanel" class="tab-pane active" id="tab4">
              <div class="profile-basic background-white p20 my_rewd_main">
                    <div class="profile-basic background-white p20 my_rewd">
                   
                      <div class="four_tab_main">

                   <div class="tab_four_sec">
                   <div class="col-md-6">
                   <div class="tab_four_sec_l"> 
                     <h3>Share & get rewarded</h3> 
               <p class="shr">Share this Product with your friends and earn Reward Credits for a discount your referral make a purchase
from us.</p>
<p class="adl_ad">
  @if(Auth::user()->my_referral_code==NULL)
                    <a href="#" onclick="alertmsg()" >Start Sharing</a>
                  @else
                    <a href="#" class="star_sh">Start Sharing</a> 
                  @endif
  </p>


                   <div class="tab_four_sec_l_to tb_ssss">
                    <span class="avla_s">
                      <b> Available Reward Credit </b> 
                      <strong>{{$available_credit}}pt</strong>
                      <a href="#"> <i class="fa fa-chevron-down" aria-hidden="true"></i> </a>
                    </span>
                    @if(!empty($rewardList))
                    @foreach($rewardList as $reward)  
                    <span class="avla_ss">
                     <?php if(!empty($reward->first_name)){ ?> 
                     <p class="pp1"><strong>{{$reward->first_name}} {{$reward->last_name}} has use your code and ordered </strong><b>+{{$reward->credit_reward}}pt</b></p> 
                     <p class="pp2">Referral order date: {{$reward->referal_order_date}}</p>
                   <?php } ?>
                    </span>
                    @endforeach
                    @else
                    <span class="avla_ss">
                      Data not available
                    </span>
                    @endif

                  <!--   <span class="avla_sss">
                     <p class="pp1"><strong>John Deo has use your code and ordered </strong>     <b>+9pt</b></p> 
                     <p class="pp2">Referral order date: 12/05/2019</p>
                    </span> -->

                   </div> 





                   <div class="tab_four_sec_l_to">
                    <span class="avla_s">
                      <b> Redeemed Reward Credits  </b> 
                      <strong> {{$redeem_credit}}pt </strong>
                      <a href="#"> <i class="fa fa-chevron-down" aria-hidden="true"></i> </a>
                    </span>
                    @if(!empty($redeemList))
                    @foreach($redeemList as $redeem)  
                    <span class="avla_ss">
                     <p class="pp1"><strong>Order ID: </strong><b>{{$redeem->order_id}}</b></p> 
                     <p class="pp1"><strong>Total Amount Redeemed </strong><b>{{$redeem->amount_redeem}}pt</b></p> 
                     <p class="pp1"><strong>Date of Purchase </strong>     <b>{{$redeem->purchase_date}}</b></p> 
                    </span>
                     @endforeach
                    @else
                    <span class="avla_ss">
                      Data not available
                    </span>
                    @endif

                    <!-- <span class="avla_sss">
                     <p class="pp1"><strong>Order ID: </strong>     <b>xxxxxx</b></p> 
                     <p class="pp1"><strong>Total Amount Redeemed </strong>     <b>5pt</b></p> 
                     <p class="pp1"><strong>Date of Purchase </strong>     <b>5/ 08 / 2019</b></p> 
                    </span> -->

                   </div>




                   </div>
                   </div>


                   <div class="col-md-6">
                    <div class="tab_four_sec_r">  
                      <h3>Insert your promo code</h3>  

                      <div class="form-group org_sss_nn">
                        @if(!empty(Auth::user()->id))
                       <?php $user_id= Auth::user()->id; ?> 
                       @endif
                       <div id="mymsg"></div>
                       <input type="hidden" name="user" id="user" value="<?php echo $user_id;?>">
                      <input type="text" class="form-control" id="code" placeholder="code" name="code">
                      </div>
                      <p class="aas_ad" onclick="applycode();"><a href="#">Claim</a></p>


                      <div class="tab_four_sec_l_to tb_ssss">
                    <span class="avla_s">
                      <b> Claimed Promo </b> 
                      <strong> 10pt </strong>
                      <a href="#"> <i class="fa fa-chevron-down" aria-hidden="true"></i> </a>
                    </span></div>
                      
                    @if(!empty($couponList))
                    <?php //print_r($couponList); ?>
                    
                    <span class="rig_sec_tx">
                      <p>Additional 10% OFF of a minimum purchase</p>
                      <b>promocode xxxxx expires on 30 june 2019</b>
                    </span>
                    
                    @else
                    <span class="rig_sec_tx">
                      Data not available
                    </span>
                    @endif

                   <!--  <span class="rig_sec_tx">
                      <p>Additional 10% OFF of a minimum purchase</p>
                      <b>promocode xxxxx explane on 30 june 2019</b>
                    </span>
 -->
                    <p class="txt-ssss">Use your Reward Credits and Promo on eligible Purchase order during Checkout</p>

                  </div>
                  </div>
                  </div>
                 


 <div class="tab_four_sec_tt">
   <h2>Tell your friends*</h2>
<span class="on_oee">
  <p>Receive $25 coupon when friends make their first purchase.</p>
  <p>Share your referral URL with friends</p>
</span>


<span class="referr_rr"> <i class="fa fa-check" aria-hidden="true"></i> Your Referral URL: <b>{{$referral_url}}</b></span>

<span class="referr_rrRR">
<a href="https://www.facebook.com/" class="fb_bg"><i class="fa fa-facebook" aria-hidden="true"></i>  <b> Share via Facebook </b></a>
<a href="https://twitter.com/login" class="tw_bg"><i class="fa fa-twitter" aria-hidden="true"></i>  <b> Share via Twitter </b></a>
<a href="https://accounts.google.com/login" class="goog_bg"><i class="fa fa-google-plus" aria-hidden="true"></i>  <b> Share via Google+ </b></a>
</span>
<p class="share_vi">or share via email</p>
<div id="mymsg1"></div>

<span class="referr_New_rr">
<input type="text" id="name" class="form-control input-lg inp_l" placeholder="Name" autocomplete="off" spellcheck="false" autocorrect="off" tabindex="1">

<input type="text" id="referral_email_to" class="form-control input-lg inp_l" placeholder="sign up form for user who using referral link" autocomplete="off" spellcheck="false" autocorrect="off" tabindex="1">

<input type="hidden" id="referral_email_from" class="form-control input-lg inp_l" placeholder="Enter email" autocomplete="off" spellcheck="false" autocorrect="off" tabindex="1">

<input type="hidden" id="referral_url" class="form-control input-lg inp_l" value="{{$referral_url}}" placeholder="Enter referral Url" autocomplete="off" spellcheck="false" autocorrect="off" tabindex="1" readonly="">
</span>

 <p class="aas_ad11123"><a href="#" onclick="reffercode();">Send Emails</a></p>  


 </div>


 </div>

                   </div>
               </div>
            </div>





              <div role="tabpanel" class="tab-pane" id="tab5">
              <div class="profile-basic background-white p20">

               </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    

    </div>
  </div>
</div>




  </section>
@endsection

@section('page_footer')
  
@endsection