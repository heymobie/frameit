@extends('user.layout.layout')
@section('title', 'User - Orders')

@section('current_page_css')

@endsection
<?php $id = Request::segment(2); ?>
@section('current_page_js')
<script type="text/javascript">
 
  //start get order details by id

  $(document).ready(function(){
    var order_id = "<?php echo $id; ?>";
    var formData = new FormData();
    formData.append('order_id',order_id);
    $.ajax({
      type: 'POST',
      url: "<?php echo url('/').'/api/getUserOrderDetails'; ?>",
      data: formData,
      cache:false,
      contentType: false,
      processData: false,
      success: function(resultData){ 
        //console.log(resultData);
        if(resultData.status){
          var j=1;
          var yourArray = [];
          var totals =[] , ship_prices =[] , coupon_discounts =[] , wallet_discounts =[] , total_prices =[] ;
          $.each(resultData.response.orderDetails, function( i, l ){
            var ship_price = resultData.response.orderDetails[i].shipping_price;
            var coupon_discount = resultData.response.orderDetails[i].coupon_discount;
            var wallet_discount = resultData.response.orderDetails[i].wallet_discount;
            var total_price = resultData.response.orderDetails[i].total_price;

            var qty = resultData.response.orderDetails[i].quantity;
            var sub_total = resultData.response.orderDetails[i].tilePrice * qty;
            var total = resultData.response.orderDetails[i].tilePrice * resultData.response.orderDetails[i].total_tile;

            var details = '<tr><td>'+j+'</td><td><img src="'+resultData.response.orderDetails[i].frameUrl+'/'+resultData.response.orderDetails[i].frameImage+'" width="120px" style="transform:rotate('+resultData.response.orderDetails[i].rotation+'deg);"></td><td><img src="'+resultData.response.orderDetails[i].tileUrl+'/'+resultData.response.orderDetails[i].tileImage+'" width="120px"></td><td>'+resultData.response.orderDetails[i].quantity+'</td><td>'+resultData.response.orderDetails[i].color+'</td><td>'+resultData.response.orderDetails[i].tilePrice+' USD</td><td>'+sub_total+' USD</td></tr>';

            yourArray.push(details); totals.push(total); ship_prices.push(ship_price); coupon_discounts.push(coupon_discount); wallet_discounts.push(wallet_discount); total_prices.push(total_price);
            j++;
          });

          $(".orderDetailList").html(yourArray);

          var total_count = '<tr><td></td><td></td><td></td><td></td><td></td><td><strong>Total Price</strong></td><td><strong>'+totals[0]+'</strong> USD</td></tr><tr><td></td><td></td><td></td><td></td><td></td><td><strong>Shipping Price</strong></td><td><strong>'+ship_prices[0]+'</strong> USD</td></tr><tr><td></td><td></td><td></td><td></td><td></td><td><strong>Coupon Amount</strong></td><td><strong>'+coupon_discounts[0]+'</strong> USD</td></tr><tr><td></td><td></td><td></td><td></td><td></td><td><strong>Wallet Amount</strong></td><td><strong>'+wallet_discounts[0]+'</strong> USD</td></tr><tr><td></td><td></td><td></td><td></td><td></td><td><strong>Grand Total</strong></td><td><strong>'+total_prices[0]+'</strong> USD</td></tr>';

          $(".total_count").html(total_count);

          //var delay = 1500; 
          //setTimeout(function(){ window.location.href = "<?php //echo url('/order_details'); ?>"; }, delay);

        }   
      }
    });
  });
  //end get order details by id
</script>
@endsection

@section('tag_body')
<body>
@endsection

@section('content') 
<!--==========================-->
<section id="home" class="section bx_innerCrop">
    <div id="my_order_sec" class=" area-padding">
      <div class="container">
        <div class="row  wow slideInUp" data-wow-delay=".1s">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h2><span>Order</span> Details</h2>
            </div>
          </div>
        </div>
	  </div>
	</div>
  <div class="">
    <div class="">
      <div class="tab-content">
        <div id="home" class="tab-pane fade in active show">
          <div class="">
            <div class="col-md-12 cust-table">
              <div class="mt-4"></div>
              <div class="orderTable">
                <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>SNo.</th>
                      <th>Frame Image</th>
                      <th>Tile Image</th>
                      <th>Quantity</th>
                      <th>Color</th>
                     
                      <th>Price</th>
                      <th>Sub-Total</th>
                    </tr>
                  </thead>
                  <tbody class="orderDetailList">
                  </tbody>
                  <tfoot class="total_count">
                  </tfoot>
                </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--============================-->
</body>
@endsection

@section('page_footer')
<!--<footer class="footerSectopm wow fadeInUp">
<div class="footerBtn">
	<button data-toggle="modal" data-target="#exampleModalCenter">Check Out <i class="fa fa-chevron-right"></i></button>
</div>
</footer>-->


@endsection