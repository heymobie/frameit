@extends('user.layout.layout')
@section('title', 'User - Privacy Policy')


<?php $segment1 =  Request::segment(1);  ?>
@section('current_page_css')
@if($segment1 == 'privacy_policy_mobile')
<style type="text/css">
  header#header {
    display: none;
}
</style>
@endif
@endsection


@section('current_page_js')

<script type="text/javascript">
  //start get faq list
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });
  
  $(document).ready(function(){
    var formData = new FormData();
    $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/getAllFaqList'; ?>",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          console.log(resultData);
          if(resultData.status){

            var faqArray = [];
            var j=1;
            $.each(resultData.response.faqList, function( i, l ){

              var faqs = '<div class="card"><div class="card-header bg-warning text-white"><div class="card-link" data-toggle="collapse" href="#collapse'+resultData.response.faqList[i].id+'">'+resultData.response.faqList[i].question+'</div></div><div id="collapse'+resultData.response.faqList[i].id+'" class="collapse" data-parent="#accordion"><div class="card-body">'+resultData.response.faqList[i].answer+'</div></div></div><br/>';

              faqArray.push(faqs);
              j++;
            });
            $(".faqList").html(faqArray);

          }else{

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> No Faq List Found.</div>';
            $("#err_msg").html(result_alert);

          }

        },error: function(errorData) { 

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            $("#err_msg").html(result_alert);

        }
    });
  });
  //end get faq list
</script>
@endsection

@section('tag_body')
<body>
@endsection

@section('content')
<!--==========================-->
<section id="home" class="section bx_mainCrop">
    <div id="my_profile_sec" class=" area-padding" style="padding-top:30px;">
      <div class="container">
        <div class="row  wow slideInUp" data-wow-delay=".1s">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center" style="padding-top:30px;">
              <h2><span>Privacy Policy</span></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container"> 
      <div class="col-lg-12">
        <div class="mt-4"></div>
        <div>
            <p>
              We at Frame Mate Sdn. Bhd.. (as defined below) recognize and respect the importance of maintaining the privacy of our customers. If you are a user, you are contracting with Frame Mate Sdn. Bhd.. (collectively "FRAMEIT", "us", "our", or "we"). If not otherwise defined herein, capitalized terms have the meaning given to them in the Terms of Service, available at http://frameit.com.my/terms-of-service (the "Terms"). This Privacy Notice describes the types of information we collect from you when you visit our App, use our Services, or participate in our Referral Program, as described in the Referral Terms available at http://frameit.com.my//referral-terms ("Referral Program"). This Privacy Notice also explains how we may process, transfer, store and disclose the information collected, as well as your ability to control certain uses of the collected information.
            </p>
            <p>
              If you are contracting with Frame Mate Sdn. Bhd.. such entity is the data controller in respect of certain processing activities outlined in this Privacy Notice. Our registered office is (need addresss). Information may also be collected by additional data controllers as listed below. You are the data controller of any Personal Data you upload to the App for the purpose of printing such Personal Data on a Tile.
            </p>
            <p>
              "Personal Data" means any information that refers, is related to, or is associated with an identified or identifiable individual or as otherwise may be defined by applicable law.
            </p>
            <h3>Privacy Notice Key Points</h3>
            <p>The key points listed below are presented in further detail throughout this Privacy Notice. These key points do not substitute the full Privacy Notice.
            </p>
            <ul>
              <li><b>Personal Data We Collect.</b> When you use our Services, we collect Personal Information provided by you, such as your name, email address, shipping address, phone number, where applicable, and credit card payment information, as well as any Images that you upload to the App. Images with which you provide us may include Sensitive Data such as data about a person's racial origins or religious beliefs or data about children and as more fully detailed below. We also (automatically) collect information about your use of the App. If you contact us for questions or complaints, we will collect the information related to your inquiry. When you visit our Site, we automatically collect you IP address and operating system.
              </li>
              <li><b>How We Use the Data We Collect. </b> We use the data (including Personal Data) we collect and/or receive mainly to administer, provide, and improve the App and Services, contact you with administrative information. We use face detection software for the sole purposes of accurately cropping the Images uploaded by you to the App.
              </li>
              <li><b>Basis for Processing Your Personal Data.</b> Processing your Personal Data is necessary for the performance of the Terms and the provision of the Services to you. Processing for the purposes of developing and enhancing our products and services, for analytics and usage analysis, for fraud prevention and security and for our recordkeeping, protection of our legal rights, and complying with our legal obligations – are all necessary for the purposes of legitimate interests that we pursue. Processing of Sensitive Data is based upon explicit consent.
              </li>
              <li><b>Sharing the Personal Data We Collect.</b> We share the Personal Data we collect with our service providers and subcontractors who assist us in the operation of the App and provision of the Services and process the information on our behalf and under our instructions. If you are in the US, we may also share Personal Data with third-party business partners who may use your information for their own direct marketing purposes.
              </li>
              <li><b>International Transfer.</b> We use service providers and/or subcontractors and/or cooperate with or have business partners and affiliates located in countries other than your own, and send them your Personal Data. We will ensure to have agreements in place with such parties ensuring the same level of privacy and data protection as set forth in this Privacy Notice. You hereby consent to such international transfer.
              </li>
              <li><b>Security.</b> We implement industry standard measures aimed at reducing the risks of damage and unauthorized access or use of Personal Data, but they do not provide absolute information security. Such measures include physical, electronic, and procedural safeguards (such as secure servers, firewalls, antivirus and SSL encryption), access control, and other internal security policies.
              </li>  
              <li><b>Your Rights.</b> Subject to applicable law and additional rights as set forth below, you may have a right to access, update and/or delete your Personal Data and obtain a copy of the Personal Data we have collected about you. You can change your mind at any time about your election to receive marketing communications from us and/or having your Personal Data processed for direct marketing purposes. You also have the right to object at any time to processing your personal data for certain purposes, including marketing purposes. You have the right to withdraw your consent to processing, if provided, at any time by contacting us as detailed in this Privacy Notice.
              </li>
              <li><b>Data Retention.</b> We retain information for as long as necessary for the purposes set forth in this Privacy Notice. To determine the appropriate retention period, we consider the amount, nature, and sensitivity of the Personal Data, the potential risk of harm from unauthorized use or disclosure of your Personal Data, the purposes for which we process your Personal Data and whether those purposes can be achieved through other means, as well as applicable legal requirements.
              </li>
              <li><b>Use of Cookies and Similar Technologies.</b> We use cookies and similar technologies to help personalize your experience by helping save your settings and customizations across visits. You can adjust your settings to determine which cookies you do or do not allow. Changing your settings and/or deleting existing cookies may affect the Services.
              </li>
              <li><b>Children.</b> We do not knowingly collect personally-identifiable information from children under the age of sixteen (16). In the event that you become aware that an individual under the age of sixteen (16) has enrolled without parental permission, please advise us immediately.
              </li>
              <li><b>Communications</b> Subject to your consent and applicable law, we may send you e-mail or other messages about us or our Services. You can stop receiving future communications from us by following the UNSUBSCRIBE link located at the bottom of each communication, by emailing us at <a href="">hi@frameit.com.my..</a>
              </li>
              <li><b>Changes to the Privacy Notice.</b> We may change this Privacy Notice from time to time and shall notify you of such changes.
              </li>


            </ul>
            <h3>Personal Data We Collect</h3>
            <p>
            We collect information from you when you choose to use our App and/or Services and/or participate in the Referral Program, if applicable. In order to use our App and/or receive related Services and/or make a purchase through the App, you will be required to provide us with certain Personal Data. We also collect Personal Data when you make use of the App, request information from us, sign up for newsletters or our email lists, complete online forms, or contact us for any other reason.</p>
            <p>
              The Personal Data that we collect from you includes name, email address, shipping address, phone number, and credit card payment information. Such Personal Data may be collected by us through the App.
            </p>
            <p>
              When you register for the Referral Program, you may be asked to input your email address.
            </p>
            <p>
              Certain categories of Personal Data are subject to special protections under Regulation 2016/679 of the European Parliament and of the Council of 27 April 2016 (General Data Protection) ("GDPR"). These include Personal Data relating to among others, a person's racial origin, religious beliefs, health, sexual orientation, listed in Article 9 of the GDPR ("Special Categories of Data") and Personal Data related to children (together with Special Categories of Data, "Sensitive Data"). We may also collect Sensitive Data, if you provide us with such data.
            </p>
            <p>
              We also collect any materials including Images and/or pictures and/or photos you may upload to the App, including Sensitive Data.. If you choose to link a Linked Account, FRAMEIT will have access to the account information of such Linked Account and the photos or pictures saved in your Linked Account. FRAMEIT will only use the photos you select for creation of a Tile and will not make any use of any additional photos or pictures saved in your Linked Account.
            </p>
            <p>
              In addition, when you use the App, certain information may be automatically gathered about your computer or mobile device such as operating system, IP address, device identifier and subject to your consent as may be required under applicable law, (geo) location, as well as your browsing history and any information regarding your viewing and purchase history on our App and interaction with the App.
            </p>
            <p>
              It is your voluntary decision whether to provide us with any such Personal Data, but if you refuse to provide such information we may not be able to provide you with the Services.
            </p>

            <h3>How We Use Your Personal Data</h3>
             
            <b>General</b>
            <p>We and any of our trusted third-party subcontractors and service providers may use the Personal Data we collect from and about you for any of the following purposes: (1) to provide you with the App and/or Services and to enable you to participate in the Referral Program, if applicable; (2) to respond to your inquiries or requests, contact and communicate with you;
            </p>
            <p>
              to develop new products or services and conduct analyses to improve our current content, products, and Services; (4) we use face detection software for the sole purposes of accurately cropping the Images uploaded by you to the App; (5) to contact you with informational newsletters and promotional materials relating to our App and Services; (6) to review the usage and operations of our App and Services; (7) to use your data in an aggregated, non-specific format for analytical purposes (as detailed below); (8) to prevent fraud, protect the security of our App and Services, and address any problems with the App and/or Services; (9) to provide customer support; and (10) to satisfy our legitimate interests, while at all times ensuring that your rights and freedoms are not affected.
            </p>
            <b>Statistical Information and Analytics</b>
            <p>
              We and/or our service providers use analytics tools, including "Google Analytics" to collect and analyze information about the use of the App and/or Services, such as how often users visit the App, what pages they visit when they do so, and what other sites and mobile applications they used prior to visiting the App. By analyzing the information we receive, we may compile statistical information across a variety of platforms and users, which helps us improve our App and Services, understand trends and customer needs and consider new products and services, and tailor existing products and services to customer desires. The information we collect is anonymous and aggregated and we will not link it to any Personal Data. We may share such anonymous information with our partners, without restriction, on commercial terms that we can determine in our sole discretion. You can find more information about how Google collects information and how you can control such use at https://policies.google.com/technologies/partner-sites.
            </p>

            <b>Legal Uses</b>
            <p>We may use your Personal Data as required or permitted by any applicable law.</p>
            <b>Basis for Processing Your Personal Data</b>
            <p>
              Processing your Personal Data is necessary for the performance of the Terms, offering the Referral Program, if applicable, and the provision of the Services to you, including responding to your inquiries or requests, contacting and communicating with you and providing customer support. When you make a purchase, use our Services or engage in any other transaction with us, we may also process your Personal Data to perform that contract.
            </p>
            <p>  
              Processing for the purposes of developing new and enhancing our products and Services, for analytics and usage analysis, for the marketing of our products and services, for fraud prevention and security and for our recordkeeping and protection of our legal rights – are all necessary for the purposes of legitimate interests that we pursue. In conducting such processing activities, we balance these legitimate interests against the rights and interests of our users. If you would like more information regarding how we make such determinations, please contact us through the contact information specified below.
            </p>
            <p>
              Processing of any Sensitive Data, as more fully described in the Terms, is based upon explicit consent. By entering into the Terms, you represent that you consent to, or have obtained the consent of the relevant data subject for the processing of any such Sensitive Data. If you or the relevant data subject wish to withdraw consent to such processing activities, you can do so at any time by contacting us as detailed in this Privacy Notice. We will process your request as soon as reasonably possible, however it may take a few days for us to update our records before any opt out is effective.
            </p>
            <p>
              Please note that we may process your Personal Data for more than one legal basis depending on the specific purpose for which we are using your Personal Data. Please contact us if you would like details about the specific legal ground we are relying on to process your Personal Data.
            </p>

            <h3>Disclosure of Personal Data We Collect</h3>
            <p>
              We may share your information, including Personal Data, as follows:
            </p>
            <p>Business Partners, Service Providers, Affiliates, and Subcontractors
              We disclose information, including Personal Data we collect from and/or about you, to our trusted service providers, business partners, affiliates, subcontractors, who may use such information to: (1) help us provide you with the App and/or Services and/or Referral Program, including for purposes of payment, printing, and services; and (2) aid in their understanding of how users are using our App and Services.
            </p>
            <p>
              Such service providers, business partners, affiliates, and subcontractors provide us with printing and shipping services, payment processing, IT and system administration services, data backup, security and storage services, data analytics and help us provide marketing services and be in contact with our customers via chat and email.
            </p>
            <h3>US Users</h3>
            <p>
              If you are in the US, we may share your Personal Data with our third-party business partners who may use your information for their own direct marketing purposes. If you prefer that we do not share your Personal Data with business partners for these purposes in the future, please email us at hi@frameit.com.my. Note that such a request will not affect any information we have previously shared and that we will continue to share information with third parties for other purposes, as described in this Privacy Notice.
            </p>
            <h3>Business Transfers</h3>
            <p>
              We may transfer our databases containing your Personal Data if we sell our business or part of it, including in cases of liquidation. Information about our users, including Personal Data, may be
              disclosed as part of, or during negotiations of, any merger, sale of company assets or acquisition and shall continue being subject to the provisions of this Privacy Notice.
            </p>
            <h3>Law Enforcement Related Disclosure</h3>
            <p>
              We will fully cooperate with any law enforcement authorities or court order requesting or directing us to disclose the identity, behavior or (digital) content and information of or related to an individual, including in the event of any user suspected to have engaged in illegal or infringing behavior. We may also share your Personal Data with third parties: (i) if we believe in good faith that disclosure is appropriate to protect our rights, property or safety (including the enforcement of the Terms and this Privacy Notice); (ii) to protect the rights, property or safety of third parties; (iii) when required by law, regulation subpoena, court order or other law enforcement related issues; or (iv) as is necessary to comply with any legal and/or regulatory obligation. You can request such Personal Data as specified herein by emailing us at hi@frameit.com.my.
            </p>
            <h3>Other Uses or Transfer of Your Information</h3>
            <p>
              We allow you to use our App and Services in connection with third-party services, sites, and/or mobile applications. If you use our App and/or Services with or through such third-parties, we may receive information (including Personal Data) about you from those third parties. Please note that when you use third-parties outside of our App and/or Services, their own terms and privacy policies will govern your use of those services.
            </p>
            
            <h3>International Transfer</h3>
            <p>
            We use subcontractors and service providers and have business partners and affiliates who are located in countries other than your own and send them information we receive (including Personal Data). We will ensure that these third parties will be subject to written agreements ensuring the same level of privacy and data protection as set forth in this Privacy Notice, including appropriate remedies in the event of the violation of your data protection rights in such third country.
          </p>
          <p>
            Whenever we transfer your Personal Data to third parties based outside of the European Economic Area (EEA), we ensure a similar degree of protection is afforded to it by ensuring at least one of the following safeguards is implemented:
          </p>
          <ul>
            <li>We will only transfer your Personal Data to countries that have been deemed to provide an adequate level of protection for Personal Data by the European Commission.</li>
            <li>Where we use certain service providers, we may use specific contracts approved by the European Commission which give Personal Data the same protection it has in the EEA.</li>
            <li>Where we use providers based in the US, we may transfer data to them if they have been certified by the EU-US Privacy Shield which requires them to provide similar protection to Personal Data shared between the Europe and the US or any other arrangement which has been approved by the European Commission.</li>
          </ul>  
            <p>Please contact us the contact information listed below if you would like further information on the specific mechanism used by us when transferring your Personal Data out of the EEA.</p>

            <p>You hereby consent to such international transfer described above.</p>

            <h3>Security</h3>
            <p>
            We make efforts to follow generally accepted industry standards to protect the Personal Data submitted to and collected by us, both during transmission and once we receive it, including by implementing the below:
            Safeguards - We employ physical, electronic, and procedural safeguard to protect your data include secure servers, firewalls, antivirus and SSL encryption of data.
            </p>
            <p>
            Access Control - We dedicate efforts for a proper management of system entries and limit access only to authorized personnel on a need to know basis of least privilege rules, review permissions quarterly, and revoke access immediately after employee termination.
            </p>
            <p>
            Internal Policies - We maintain and regularly review and update our privacy related and information security policies.
            </p>
            <p>Personnel - We require new employees to sign non-disclosure agreements according to applicable law and industry customary practice.
            </p>
            <p>Encryption - We encrypt the data in transit using secure protocols.
            </p>
            <p>Database Backup – Our databases are backed up on a periodic basis for certain data and which are verified regularly. Backups are encrypted and stored within the production environment to preserve their confidentiality and integrity and are tested regularly to ensure availability, and are accessed only by authorized personnel.
            </p>
            <p>
            However, no method of transmission over the Internet or method of electronic storage is 100% secure. Therefore, while we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.
            </p>

            Data Controllers
            <p>
            When you use our App or Services, if you have not disabled third party cookies (please see section on Cookies below), in addition to the FRAMEIT Group Entities, additional third parties may also be serving as controllers in connection with the collection of your Personal Data. Their details and contact information are as set forth below:
            <ul>
              <li>Facebook, Inc. –</li>
              <li>Google LLC – Keith Enright</li>
            </ul>
            <p>https://feedback-form.truste.com/watchdog/request
            Director, Privacy Legal
            Google LLC
            Google Data Protection Office 1600 Amphitheatre Pkwy
            Mountain View, California 94043 data-protection-office@google.com
            Phone: 650-253-0000
            Fax: 650-618-1806</p>
            
            <h3>Your Rights - How to Access and Limit Our Use of Certain Information</h3>

            <p>You have certain rights in relation to the Personal Data that we and our co-controllers hold about you, as follows detailed below. For any requests to exercise such rights with respect to information held by our co-controllers, please contact the applicable co-controller directly at the contact information provided above. If you wish for us to notify all co-controllers, please specify that when you contact us in order to exercise any of your rights. We reserve the right to ask for reasonable evidence to verify your identity before we provide you with any information and/or comply with any of your requests, as detailed below:</p>
            <ul>
            <li><b>Right of Access and Data Portability.</b> You have a right to know what Personal Data we collect about you and, in some cases, to have the information communicated to you. Subject to the limitations in applicable law, you may be entitled to obtain from us a copy of the Personal Data you provided to us (excluding information that we obtained from other sources) in a structured, commonly-used, and machine-readable format, and you may have the right to (request that we) transmit such Personal Data to another party. If you wish to exercise this right please contact us letting us know what information in particular you would like to receive and/or transmit. Subject to applicable law, we may charge you with a fee. Please note that we may not be able to provide you with all the information you request, for instance, if the information includes Personal Data about another person. Where we are not able to provide you with information that you have asked for, we will endeavor to explain to you why. We will try to respond to any request for a right of access as soon as possible.
            Right to Correct Personal Data. Subject to the limitations in applicable law, you may request that we update, correct or delete inaccurate or outdated Personal Data and/or that we suspend the use of Personal Data, the accuracy of which you may contest, while we verify the status of that Personal Data. We will correct your Personal Data within a reasonable time from the receipt of your written request thereof.</li>
            <li>
            <b>Deletion of Personal Data ("Right to Be Forgotten").</b> In certain circumstances you have a right to have Personal Data that we hold about you deleted. Should you wish to have any Personal Data about you deleted, please contact us, using the contact information specified in this Privacy Notice. Subject to applicable law, we will delete Personal Data provided to us by a user within a reasonable time from the receipt of a written (including via email) request by such user to delete such collected Personal Data. We cannot restore information once it has been deleted. Please note that to ensure that we do not collect any further Personal Data, you should also delete our App from your mobile devices and clear our cookies from any device where you have used our App. We may retain certain Personal Data (including following your request to delete) for audit and record-keeping purposes, as well as other purposes, all as permissible and/or required under applicable law. We may also retain your information in an anonymized form.
          </li>
          <li>
            <b>Right to Restrict Processing:</b> You may request at any time that we limit the processing of your Personal Date if you believe that either: (i) the Personal Data is inaccurate and wish us to limit processing until we verify its accuracy; (ii) the processing is unlawful, but you do not wish us to erase the Personal Data; (iii) we no longer need the Personal Data for the purposes for which it was collected, but you still need it for the establishment, exercise, or defense of a legal claim; (iv) you have exercised your Right to Object (below) and we are in the process of verifying our legitimate grounds for processing. We may continue to use your Personal Data after a restriction request either: (a) with your consent; (b) for the establishment, exercise or defense of legal claims; or (iii) to protect the rights of another natural or legal person.
          </li>
          <li>
            <b>Direct Marketing Opt Out.</b> You can change your mind at any time about your election to receive marketing communications from us and/or having your Personal Data processed for direct marketing purposes. If you do, please notify us by contacting us as detailed in this Privacy Notice. We will process your request as soon as reasonably possible, however it may take a few days for us to update our records before any opt out is effective.
          </li>
          <li>
            <b>Right to Object.</b> Subject to applicable law, you may have the right to object to processing of your Personal Data including for the purpose of direct marketing.
          </li>
          <li>
            <b>Withdrawal of Consent.</b> You may withdraw your consent in connection with any processing of your Personal Data based on a previously granted consent.
          </li>
          <li>
            <b>Supervisory Authority.</b> If you are a European Citizen, you may have the right to submit a complaint to the relevant supervisory data protection authority.
          </li>
        </ul>
        </p>

          <h3>Data Retention</h3>
          <p>Subject to applicable law and our (regulatory obligations, we retain information as necessary for the purposes set forth above. Any User Content, including Images, will be retained until your account has been terminated, either by us or by you, for any reason. Any Images uploaded by you to the Services and not elected by you to be printed on Tiles will be automatically deleted after thirty (30) days from the day you have uploaded the Image(s). You may ask us to delete any User Content, including Images, by sending us an email, as specified above. We may delete any information (including User Content) from our systems, without notice to you, once we deem it is no longer necessary for the purposes set forth in this Privacy Notice. We may also retain your information in an anonymized form. In addition, retention by any of our processors may vary, in accordance with the processor's retention policy.

          To determine the appropriate retention period, we consider the amount, nature, and sensitivity of the Personal Data, the potential risk of harm from unauthorized use or disclosure of your Personal Data, the purposes for which we process your Personal Data and whether those purposes can be achieved through other means, as well as applicable legal requirements.

          Please contact us through the contact information listed above if you would like details regarding the retention periods for different types of your Personal Data.
        </p>


        <h3>Cookies and Similar Technologies</h3>
            <p>We use cookies and similar technologies to help personalize your experience. Third parties through which we provide the Services and/or our business partners may be placing and reading cookies on your browsers, or using web beacons to collect information in the course of advertising being served on different websites.

            What are Cookies?</p>

            <p>A cookie is a small piece of text that is sent to a user's browser or device. The browser provides this piece of text to the device of the originating user when this visitor returns.</p>
            <ul>
            <li>A "session cookie" is temporary and will remain on your device until you leave the Site.</li>
            <li>A "persistent" cookie may be used to help save your settings and customizations across visits.
            It will remain on your device for much longer or until you delete it.</li>
            <li>First-party cookies are placed by us, while third-party cookies may be placed by a third party.</li>
            We use both first- and third-party cookies.
            <li>Information may also be collected through web beacons, which are small graphic images ("pixel tags"), which usually work together with cookies in order to identify users and user behavior. These may be shared with third parties.</li>
          </ul>

            <p>We may use the terms "cookies" to refer to all technologies that we may use to store data in your browser or device or that collect information or help us identify you in the manner described above.

            How We Use Cookies

            FRAMEIT uses cookies and similar technologies for a number of reasons, for example, in order to help personalize your experience and to help personalize the ads we serve you.

            The specific names and types of the cookies, web beacons, and other similar technologies we use may change from time to time. However, the cookies we use generally fall into one of the following categories:</p>
            <ul>
            <li><b>Necessary-</b> These cookies are necessary in order to allow the App to work correctly. They enable you to access the App, move around, and access different services, features, and
            tools. Examples include remembering previous actions (e.g. entered text) when navigating back to a page in the same session. These cookies cannot be disabled.</li>
            <li><b>Functionality -</b> These cookies remember your settings and preferences and the choices you make (such as language or regional preferences) in order to help us personalize your experience and offer you enhanced functionality and content.</li>
            <li><b>Performance -</b> These cookies can help us collect information to help us understand how you use our App, such as whether you have viewed messages or specific pages and how long you spent on each page. This helps us improve the performance of our App.</li>
            <li><b>Analytics -</b> These cookies help us learn more about which features are popular with our users and how our App can be improved.</li>
            <li><b>Advertising -</b> These cookies are placed in order to delivery content, including ads relevant and meaningful to you and your interests. They may also be used to deliver targeted advertising or to limit the number of times you see an advertisement. This can help us track how efficient advertising campaigns are, both for our own Service and for other websites.</li>
          </ul>

            <h3>How to Adjust Your Preferences</h3>
            <p>
            Most Web browsers are initially configured to accept cookies, but you can change this setting so your browser either refuses all cookies or informs you when a cookie is being sent. In addition, you are free to delete any existing cookies at any time. Please note that some features of the Services may function improperly when cookies are disabled or removed.
            </p>
            <p>
            By changing your device settings, you can prevent your device's advertising identifier being used for interest-based advertising, or you can reset your device's ad identifier. Typically, you can find the ad identifier settings under "privacy" or "ads" in your device's settings, although settings may vary from device to device.
            </p>
            <p>
            Adjusting your preferences as described in this section herein does not mean you will no longer receive advertisements, it only means the advertisement you do see will be less relevant to your interests.</p>

            <h3>Third-Party Applications and Services</h3>

            <p>All use of third-party applications or services is at your own risk and subject to such third party's privacy policies.</p>

            <h3>Communications</h3>

            <p>Subject to your consent and applicable law, we may send you e-mail or other messages and/or a newsletter about us or our Services. You may remove your Personal Data from our mailing list and stop receiving future communication from us by following the UNSUBSCRIBE link located at the bottom of each communication or by emailing us at hi@frameit.com.my. Please note that we reserve the right to send you service-related communications, including service announcements and administrative messages relating to your account, without offering you the opportunity to opt out of receiving them.</p>

            <h3>Children</h3>

            <p>We do not knowingly collect personally-identifiable information from children under the age of sixteen (16). In the event that you become aware that information relating to an individual under the age of sixteen (16) has been provided without parental permission, please advise us immediately.</p>

            <h3>Changes</h3>
            <p>When visiting the App, you shall be asked to accept the terms of this Privacy Notice. If you do not agree with the terms hereof, please do not use the App. We may update this Privacy Notice from time to time, in which case, we shall notify you of such changes. We will post the updated Privacy Notice on this page. Please come back to this page every now and then to make sure you are familiar with the latest version. Any new Privacy Notice will be effective from the date it is accepted by you.</p>

            <h3>Comments and Questions</h3>

            <p>If you have any comments or questions about our privacy notice, or if you wish for us to amend or delete your Personal Data, please contact us at hi@frameit.com.my.</p>

            <p>Last updated: April 2020</p>



  
        </div>
     </div>
  </div>
</section>
<!--============================-->
</body>
@endsection
