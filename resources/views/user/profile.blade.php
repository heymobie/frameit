@extends('user.layout.layout')

@section('title', 'User - Profile')



@section('current_page_css')
<style>
.modal-content.select_form_address_n {
    max-height: 10000px;
}
</style>
@endsection





  @section('tag_body')

    <body>

  @endsection



  @section('content')

   <section id="" class="section bx_mainCrop">

    <div class="selectFrameSec">

      <!-- <h2>Frame Style</h2> -->

        <!--<ul class="nav nav-tabs">

        <li class="active"><a data-toggle="tab" href="#home" class="active show"><div class="frmStyl"><img src="img/cleanIcon.svg"><span>Clean</span></div></a></li>

        <li><a data-toggle="tab" href="#menu1" cl><div class="frmStyl "><img src="img/everIcon.svg"><span>Ever</span></div></a></li>

        <li><a data-toggle="tab" href="#menu2"><div class="frmStyl"><img src="img/boldIcon.svg"><span>Bold</span></div></a></li>

        <li><a data-toggle="tab" href="#menu3"><div class="frmStyl"><img src="img/classicIcon.svg"><span>Classic</span></div></a></li>

      </ul>--> 

    </div>



    <div id="my_profile_sec" class=" area-padding">

      <div class="container">

      <div class="inner_secPro">

      <div class="row  wow slideInUp" data-wow-delay=".1s">

          <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="section-headline text-center">

             <!--  <h2><span>My</span> Profile</h2> -->



              <div class="inn_h_secc">

                <a href="{{url('/')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to homepage</a>

                <h3>Manage Account</h3>

              </div>



            </div>

          </div>

        </div>

        <div class="col-lg-12" role="tabpanel">

          <div class="row manag-tabs">

                 

            <div class="col-sm-2">

              <ul class="nav nav-pills brand-pills nav-stacked" role="tablist">



                <?php 

                  $currentPath= Route::getFacadeRoot()->current()->uri(); 

                  if($currentPath=='profile'){

                    $classAct='active';

                  }else{

                    $classAct='';

                  }



                ?>



                <!--<li class="brand-nav"><a href="{{url('/dashboard')}}" aria-controls="tab1" role="tab"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>-->

                <!-- <li class="brand-nav "><a href="#" aria-controls="tab1" role="tab" data-toggle="tab"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li> -->



                <li class="brand-nav <?php echo $classAct; ?>"><a href="{{url('/profile')}}" aria-controls="tab2" role="tab"> My Profile</a></li>

                <li class="brand-nav "><a href="{{url('/my_order')}}" aria-controls="tab3" role="tab" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> My Order </a></li>

               <!--  <li class="brand-nav"><a href="{{url('/my-rewards')}}" aria-controls="tab4" role="tab"><i class="fa fa-sign-out" aria-hidden="true"></i> My Rewards</a></li> -->

                <!-- <li class="brand-nav active"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li> -->



                <!--<li class="brand-nav"><a href="{{url('/edit-profile')}}" aria-controls="tab3" role="tab"> Edit Profile</a></li>



                <li class="brand-nav"><a href="{{url('/change-password')}}" aria-controls="tab4" role="tab"> Change Password</a></li>



                <li class="brand-nav"><a href="#" aria-controls="tab5" role="tab" data-toggle="tab"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li> -->



                <li class="brand-nav"><a href="{{url('/logout')}}" aria-controls="tab5" role="tab" > Logout</a></li>

          </ul>

        </div>

     

        <div class="col-sm-10">

          <div class="tab-content">

          

            <div role="tabpanel" class="tab-pane " id="tab1">

              <div class="profile-basic background-white p20">

                

               <!--  <form>

                  <div class="prof-img">

                    <div class="user-photo"> <a href="#"> <img src="{{url('/').'/resources/front_assets'}}/img/user_image.jpg" alt="User Photo"> </a> </div>

                  </div>

              

                  <div class="prsn-info my-profl ne_ur_pr">

                  

                  <div class="row">

                    

                    <div class="usr_nm">

                      <h2>John Deo</h2>

                    </div>

                           

                    <div class="form-group col-lg-12">

                     <p><label>Country </label><strong>USA</strong></p>

                    </div>

                    

                    <div class="form-group col-lg-12">

                     <p><label>Status </label><strong>Ready to Volunteer</strong></p>

                    </div> 

                      

                  </div>

                  

                 

                  </div>

                </form> -->





               

                

                <div class="bottom_ur_dashbo">

            <!--     <div class="col-md-12 wow slideInUp" data-wow-delay=".2s">

               

                <div class="row">

                

                <h2>Welcome to frameit</h2>

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

               

                <p><strong>Lorem Ipsum is simply dummys?</strong>

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum

                </p>

                

                </div>

                

                </div> -->

                </div>

                

              </div>

            </div>

            

            <div role="tabpanel" class="tab-pane active" id="tab2">

              <div class="profile-basic background-white p20 inner_new_tab">

                <div class="row">

                <div class="profile-basic background-white p20 np20 ijnner_new_tab">

              <!--   <form>

                  <div class="prof-img">

                    <div class="user-photo">  

                      <img id="usrImg"  src="" alt="User Photo">  </div>

                        <div class="full-width">

                        </div>

                  </div>

                  <div class="prsn-info my-profl ne_ur_pr">

                    <div class="row">

                      <div class="usr_nm">

                        <h2 id="name"></h2>

                         <span class="pro_edit"><a href="{{url('/edit-profile')}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit </a></span>

                        

                      <div class="form-group col-lg-12 clear_b">

                        <p>

                          <label>Name </label>

                          <strong id="uname"></strong></p>

                      </div>

                     

                      

                      <div class="form-group col-lg-12 clear_b">

                        <p>

                          <label>Email </label>

                          <strong id="umail"></strong></p>

                      </div>

                    

                      

                      <div class="form-group col-lg-12 clear_b">

                        <p>

                          <label>Phone Number </label>

                          <strong id="ucontact">+91 </strong></p>

                      </div>

                    



                       <div class="form-group col-lg-12 clear_b">

                        <p>

                          <label>Date of Birth </label>

                          <strong id="udob"></strong></p>

                      </div>



                       <div class="form-group col-lg-12 clear_b">

                        <p>

                          <label>Gender </label>

                          <strong id="ugender"></strong></p>

                      </div>



                        <div class="form-group col-lg-12 clear_b">

                        <p>

                          <label>Address </label>

                          <strong id="upostal"> </strong></p>

                      </div>

                      

                    </div>

                   

                  </div>

                

              </div>

            </form> -->







   <div class="tab_one_sec">



    <div class="add_address_one">

      <div class="col-md-6">

        <div class="tab_one_sec_l">  

          <p class="pr_hd"> <strong class="per_head">Personal Profile </strong>   <a href="#" class="add_n_address">Edit</a> </p>  

          <div class="tab_one_sec_l_bott"> 

            

            <div class="form-group col-lg-12 clear_b">

            <p><label>Profile Photo </label>

            <strong id="">  <img  id="usrImg" width="28px" src="{{url('/')}}/resources/front_assets/img/us_img.png"> </strong></p>

            </div>



            <div class="form-group col-lg-12 clear_b">

            <p><label>Full name </label>

            <strong id="ufullname">Oliver Queen</strong></p>

            </div>



            <div class="form-group col-lg-12 clear_b">

            <p data-toggle="modal" data-target="#changepassword"><label>Change Password</label>

            <strong id=""></strong></p>

            </div>



            <div class="form-group col-lg-12 clear_b">

            <p><label>Change Email </label>

            <strong id="uemail">Oliverq@gmail.com</strong></p>

            </div>



            <div class="form-group col-lg-12 clear_b">

            <p><label>Change Mobile </label>

            <strong id="ucontact">012 345 6789</strong></p>

            </div>



            <div class="form-group col-lg-12 clear_b">

            <p><label> Birthday Date </label>

            <strong id="udob">Not Set</strong></p>

            </div>



            <div class="form-group col-lg-12 clear_b">

            <p><label>Social Media Account </label>

            <strong id="uname">
            @if(Auth::user()->register_by=='facebook')
              <a><img src="{{url('/')}}/resources/front_assets/img/fb.png"></a>  
            @endif
            @if(Auth::user()->register_by=='web')
              <a>Web</a> 
            @endif 
             @if(Auth::user()->register_by=='gmail')
              <a>Gmail</a> 
            @endif 
           <!--  <a href="#"><img src="{{url('/')}}/resources/front_assets/img/ig.png"></a> 

            <a href="#"><img src="{{url('/')}}/resources/front_assets/img/drive_ic.png"></a> --> 

            </strong></p>

            </div>



          </div>

        </div>

      </div>



      <div class="col-md-6">

        <div class="tab_one_sec_r"> 

          <h4>Address Book</h4>

          <a href="#" class="Rig_seed">Edit</a>

          <div class="addressBookList"></div>

        

        </div>

      </div>

    </div>





    <div class="add_address_two">

     <div class="add_address_two_on"> 

        <h4>Address Book</h4>

        <a href="#" class="Rig_seed" data-toggle="modal" data-target="#showAddressBooknew">Make default shipping Address | Make Default Billing Address</a></div>

      <div class="addressList"></div>

      <div class="add_address_two_bot"> 

         

        <!--<div class="add_address_two_bot_o">

           <h5>Default Shipping Address</h5>  



          <span class="add_ons">

           <div class="col-md-2"> 

           <b>Name</b>

          </div>



           <div class="col-md-3"> 

           <b>Phone Number</b>

          </div>



           <div class="col-md-3"> 

           <b>Address</b>

          </div>



           <div class="col-md-3"> 

           <b></b>

          </div>



           <div class="col-md-1"> 

           <b></b>

          </div>

          </span>



          <span class="add_botts">

           <div class="col-md-2"> 

           <b>Oliver queen</b>

          </div>

          <div class="col-md-3"> 

           <b>(+60)12 345 6789</b>

          </div>

          <div class="col-md-3"> 

           <b>The Queen Manslon Damansara Height, Jalan Queen 90000 Petalling Jaya Serangor</b>

          </div>

          <div class="col-md-3"> 

           <b class="poran">Make default shipping Address | Make Default Billing Address</b>

          </div>

          <div class="col-md-1"> 

           <b class="orng"><a href="#" class="address_book_s">  Edit </a></b>

          </div>

          </span>

        </div>-->







        <!--<div class="add_address_two_bot_o">

          <h5>Default Shipping Address</h5>  



          <span class="add_ons">

           <div class="col-md-2"> 

           <b>Name</b>

          </div>



           <div class="col-md-3"> 

           <b>Phone Number</b>

          </div>



           <div class="col-md-3"> 

           <b>Address</b>

          </div>



           <div class="col-md-3"> 

           <b></b>

          </div>



           <div class="col-md-1"> 

           <b></b>

          </div>

          </span>



          <span class="add_botts">

           <div class="col-md-2"> 

           <b>Oliver queen</b>

          </div>

          <div class="col-md-3"> 

           <b>(+60)12 345 6789</b>

          </div>

          <div class="col-md-3"> 

           <b>The Queen Manslon Damansara Height, Jalan Queen 90000 Petalling Jaya Serangor</b>

          </div>

          <div class="col-md-3"> 

           <b class="poran">Make default shipping Address | Make Default Billing Address</b>

          </div>

          <div class="col-md-1"> 

           <b class="orng"><a href="#" class="address_book_s1">  Edit </a></b>

          </div>

          </span>

        </div>-->







      <!--<div class="add_address_two_bot_o">

        <h5>Default Shipping Address</h5>  



        <span class="add_ons">

         <div class="col-md-2"> 

         <b>Name</b>

        </div>



         <div class="col-md-3"> 

         <b>Phone Number</b>

        </div>



         <div class="col-md-3"> 

         <b>Address</b>

        </div>



         <div class="col-md-3"> 

         <b></b>

        </div>



         <div class="col-md-1"> 

         <b></b>

        </div>

        </span>



        <span class="add_botts">

         <div class="col-md-2"> 

         <b>Oliver queen</b>

        </div>

        <div class="col-md-3"> 

         <b>(+60)12 345 6789</b>

        </div>

        <div class="col-md-3"> 

         <b>The Queen Manslon Damansara Height, Jalan Queen 90000 Petalling Jaya Serangor</b>

        </div>

        <div class="col-md-3"> 

         <b class="poran">Make default shipping Address | Make Default Billing Address</b>

        </div>

        <div class="col-md-1"> 

         <b class="orng"><a href="#" class="address_book_s1">  Edit </a></b>

        </div>

        </span>

      </div>-->



      <div class="add_bu">

      <a href="#" class="add_new_adds">+ Add New Address  </a>

      </div>



    </div>    



    </div>





        <div class="add_address_three">



          <h4>Edit Address</h4>





          <form action="/action_page.php">

           

            <div class="form-group org_sss">

              <label for="email"><strong>Address Nickname</strong> <b>| Change</b></label>

              <input type="email" class="form-control" id="email" placeholder="Default Shipping Addeess" name="email">

            </div>

           

            <div class="form-group org_sssss">

              <label for="pwd"><strong>Address</strong> <b>| Change </b></label>

              <input type="password" class="form-control" id="pwd" placeholder="The Queen Mansion, Damansara Heights, Jalan Queen" name="pwd">

            </div>



            <div class="form-group org_sss">

              <label for="email"><strong>Full Name</strong>  <b>| Change</b></label>

              <input type="email" class="form-control" id="email" placeholder="Oliver Queen" name="email">

            </div>

           

            <div class="form-group org_sss">

              <label for="pwd"><strong>State</strong></label>

               <select class="form-control" id="sel1">

                <option>Selangor</option>

                <option>Selangor</option>

                <option>Selangor</option>

                <option>Selangor</option>

              </select>

            </div>



            <div class="form-group org_sss">

              <label for="email"><strong>Phone Number</strong> <b>| Change</b></label>

              <input type="email" class="form-control" id="email" placeholder="012 345 6789" name="email">

            </div>

           

            <div class="form-group org_sss">

              <label for="pwd"><strong>City</strong></label>

                <select class="form-control" id="sel1">

                <option>Petalng Jaya</option>

                <option>Petalng Jaya</option>

                <option>Petalng Jaya</option>

                <option>Petalng Jaya</option>

              </select>

            </div>





            <div class="form-group org_sss"></div>



            <div class="form-group org_sssss">

              <label for="email"><strong>Postcode</strong></label>

              <select class="form-control" id="sel1">

                <option>90000</option>

                <option>90000</option>

                <option>90000</option>

                <option>90000</option>

              </select>

            </div>





         <div class="form-group nn_butto">

            <span class="butt_one_nn"> <button type="submit" class="btn btn-default">Save</button> </span>

            <span class="butt_one_nnp"> <button type="submit" class="btn btn-default">Cancel</button> </span>

            <span class="butt_one_nnopo"> <button type="submit" class="btn btn-default">Remove</button> </span>



        </div>





        </form>





        </div>





    <div class="add_address_four">



      <h4>Add New Address</h4>





      <form id="createShipAddress" action="#" enctype="multipart/form-data">



      	{{ csrf_field() }}

      	

        <input type="hidden" name="user_id" value="{{!empty(Auth::user()->id) ? Auth::user()->id:''}}">

        <div class="form-group org_sss">

          <label for="email"><strong>Address Nickname</strong></label>

          <input type="text" class="form-control" id="address_type" placeholder="Address Nickname" name="address_type">

        </div>

       

        <div class="form-group org_sssss">

          <label for="pwd"><strong>Address</strong></label>

          <input type="text" class="form-control" id="street_address" placeholder="Address" name="street_address">

        </div>



        <div class="form-group org_sss">

          <label for="email"><strong>Full Name</strong>  </label>

          <input type="text" class="form-control" id="fullname" placeholder="Full Name" name="fullname">

        </div>



        <div class="form-group org_sssss">

          <label for="pwd"><strong>Country</strong></label>

           <select class="form-control country_list" id="sel1" name="country">

            <!--<option>Selangor</option>

            <option>Selangor</option>

            <option>Selangor</option>

            <option>Selangor</option>-->

          </select>

        </div>

       

        <div class="form-group org_sss">

          <label for="pwd"><strong>State</strong></label>

          <input type="text" class="form-control" id="state" placeholder="State" name="state">

           <!--<select class="form-control" id="sel1" id="state" name="state">

            <option>Selangor</option>

            <option>Selangor</option>

            <option>Selangor</option>

            <option>Selangor</option>

          </select>-->

        </div>



        <div class="form-group org_sssss">

          <label for="email"><strong>Phone Number</strong> </label>

          <input type="text" class="form-control" id="contact_number" placeholder="Phone Number" name="contact_number">

        </div>

       

        <div class="form-group org_sss">

          <label for="pwd"><strong>City</strong></label>

          <input type="text" class="form-control" id="city" placeholder="City" name="city">

            <!--<select class="form-control" id="sel1">

            <option>Petalng Jaya</option>

            <option>Petalng Jaya</option>

            <option>Petalng Jaya</option>

            <option>Petalng Jaya</option>

          </select>-->

        </div>



          <div class="form-group org_sssss">

          <label for="pwd"><strong>Zip Code</strong></label>

          <input type="text" class="form-control" id="zip_code" placeholder="zip code" name="zip_code">



        </div>





        <div class="form-group org_sss"></div>



        <!--<div class="form-group org_sssss">

          <label for="email"><strong>Postcode</strong></label>

          <select class="form-control" id="sel1">

            <option>90000</option>

            <option>90000</option>

            <option>90000</option>

            <option>90000</option>

          </select>

        </div>-->





     <div class="form-group nn_butto">

        <span class="butt_one_nn"> <button type="submit" name="submit" class="btn btn-default">Save</button> </span>

        <span class="butt_one_nnp"> <a href="{{url('/')}}/profile" class="btn btn-default" style="background: #bebdc0;
    border-radius: 5px;
    width: 100%;
    font-size: 15px;
    letter-spacing: 0.5px;">Cancel</a> </span>

        <!-- <span class="butt_one_nnp"> <button type="submit" class="btn btn-default">Cancel</button> </span> -->

       



    </div>





    </form>





    </div>









    <div class="add_address_five">

    <h4>Edit Profile</h4>

    
    <div id="msg1"></div>
    

    <div class="add_address_five_on">

    <div class="image-upload fill_adddd">
      
      <label for="file-input" id="uploaded_image">

        <img src="{{$user->profile_pic}}"  style="width: 180px;height: 180px;">

      </label>


      <input name="select_file" id="file-input" type="file"  />

    </div>

    <h5>Change Profile</h5>

    </div>


    <form method="post" id="upload_form" enctype="multipart/form-data">
      <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{Auth::user()->id}}" >
    <div class="add_address_five_tw">



        <div class="form-group org_sss_nn">

          <label for="email"><strong>Full Name</strong> <b>| <a id="namechange">Change</a></b></label>

          <input type="text" class="form-control" id="efullname" name="name" value="{{$user->first_name}} {{$user->last_name}}" >

        </div>



        <div class="form-group org_sss_nn">

          <label for="email"><strong>Email Address</strong> <b><!-- | <a id="emailchange">Change</a> --></b></label>

          <input type="email" class="form-control" id="eemail" name="email" value="{{$user->email}}" placeholder="OliverQ@gmail.com" readonly="">

        </div>



        <div class="form-group org_sss_nn">

          <label for="email"><strong>Mobile</strong> <b>| <a id="contactnumberchange">Add</a></b></label>

          <input type="text" class="form-control" id="econtact" name="contact_number" value="{{$user->contact_number}}" placeholder="please enter your mobile" >

        </div>





     <div class="form-group org_sss_nn">
      

            <label for="email"><strong>Social Media Account</strong></label>

            <span  class="soc_on">
            @if(Auth::user()->register_by=='facebook')
              <a><img src="{{url('/')}}/resources/front_assets/img/fb.png"></a>  
            @endif
            @if(Auth::user()->register_by=='web')
              <a>Web</a> 
            @endif 
             @if(Auth::user()->register_by=='gmail')
              <a>Gmail</a> 
            @endif 
           <!--  <a href="#"><img src="{{url('/')}}/resources/front_assets/img/ig.png"></a> 

            <a href="#"><img src="{{url('/')}}/resources/front_assets/img/drive_ic.png"></a> --> 

            </span></p>

          

       <!-- <label for="email"><strong>Social Media Account</strong></label>

       

        <span  class="soc_on">

         <b>  <img src="{{url('/')}}/resources/front_assets/img/fb.png"> </b>

         <p> <strong>OliverQ</strong> 

          <span class="ll">unlink</span>

         </p> 

        </span>



        <span  class="soc_on">

         <b>  <img src="{{url('/')}}/resources/front_assets/img/ig.png"> </b>

         <p> <strong>OliverQ</strong> 

          <span class="ll">unlink</span>

         </p> 

        </span>



        <span  class="soc_on">

         <b>  <img src="{{url('/')}}/resources/front_assets/img/drive_ic.png"> </b>

         <p> <strong>OliverQ@gmail.com</strong> 

          <span class="ll">unlink</span>

         </p> 

        </span>
 -->


     </div>



    </div>



        <div class="add_address_five_the">





            <div class="form-group org_sss_nn">

              <label for="email"><strong>Birthday</strong></label>

              <?php $bod=explode('-',$user->dob); ?>

              <span class="br_o">

                <select class="form-control" name="date" id="sel1">

                @if($user->dob!==Null && !empty($bod))  

                  <?php 
                  for ($x = 01; $x <= 31; $x++){ ?>
                     <option <?php if(str_pad($x, 2, "0", STR_PAD_LEFT)==$bod[2]){ echo 'selected'; } ?>> <?php echo str_pad($x, 2, "0", STR_PAD_LEFT); ?></option>
                  <?php }
                  ?>

                @else
                  <?php 
                  for ($x = 1; $x <= 31; $x++){
                    echo '<option value="'.str_pad($x, 2, "0", STR_PAD_LEFT).'">'.str_pad($x, 2, "0", STR_PAD_LEFT).'</option>';
                  }
                  ?>
                @endif

               </select>

            </span>



            <span class="br_tw">

                <select class="form-control" name="month" id="sel1">

                @if($user->dob!==Null && !empty($bod))  

                  <!-- <option>{{$bod[1]}}</option> -->
                  <option value="01" <?php if($bod[1]=='01'){echo "selected";} ?>>Jan</option>
                  <option value="02" <?php if($bod[1]=='02'){echo "selected";} ?>>Feb</option>
                  <option value="03" <?php if($bod[1]=='03'){echo "selected";} ?>>Mar</option>
                  <option value="04" <?php if($bod[1]=='04'){echo "selected";} ?>>Apr</option>
                  <option value="05" <?php if($bod[1]=='05'){echo "selected";} ?>>May</option>
                  <option value="06" <?php if($bod[1]=='06'){echo "selected";} ?>>Jun</option>
                  <option value="07" <?php if($bod[1]=='07'){echo "selected";} ?>>Jul</option>
                  <option value="08" <?php if($bod[1]=='08'){echo "selected";} ?>>Aug</option>
                  <option value="09" <?php if($bod[1]=='09'){echo "selected";} ?>>Sep</option>
                  <option value="10" <?php if($bod[1]=='10'){echo "selected";} ?>>Oct</option>
                  <option value="11" <?php if($bod[1]=='11'){echo "selected";} ?>>Nov</option>
                  <option value="12" <?php if($bod[1]=='12'){echo "selected";} ?>>Dec</option>

                @else

                  <option value="01">Jan</option>
                  <option value="02">Feb</option>
                  <option value="03">Mar</option>
                  <option value="04">Apr</option>
                  <option value="05">May</option>
                  <option value="06">Jun</option>
                  <option value="07">Jul</option>
                  <option value="08">Aug</option>
                  <option value="09">Sep</option>
                  <option value="10">Oct</option>
                  <option value="11">Nov</option>
                  <option value="12">Dec</option>

                @endif

               </select>

            </span>





            <span class="br_th">
                <?php 
                   $currently_selected = date('Y'); 
                  
                  $earliest_year = 1800; 
                 
                  $latest_year = date('Y'); 
                ?>
                <select class="form-control" name="year" id="sel1">

                  @if($user->dob!==Null && !empty($bod))  

                  <option>{{$bod[0]}}</option>
                  <?php
                  
                  foreach ( range( $latest_year, $earliest_year ) as $i ) {
                  echo '<option value="'.$i.'"'.($i === $bod[0] ? ' selected="selected"' : '').'>'.$i.'</option>';
                  }
                  
                  ?>

                @else

              
                  <?php
                  
                  foreach ( range( $latest_year, $earliest_year ) as $i ) {
                  echo '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                  }
                  
                  ?>

                @endif

                

               </select>

            </span>

         </div>





          <div class="form-group org_sss_nn">

              <label for="email"><strong>Gender</strong></label>

              <span class="br_olll">

                <select class="form-control" name="gender" id="sel1">

                <option <?php if($user->gender=="Male"){ echo 'selected';} ?>>male</option>

                <option <?php if($user->gende=="Female"){ echo 'selected';} ?>>female</option>

               </select>

            </span>

        </div>



        </div>


          <div class="add_bu">

          <input type="submit" id="save_btn" value="Save">

          </div>

        </form>
        </div>





         </div>











            <div class="bottom_ur_pro">

                <div class="col-md-12 wow slideInUp" data-wow-delay=".2s" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInUp;">



           <?php /* ?>                         

          

          <?php */ ?>

        </div>

           </div>

            

              </div>





                </div><!-- /.row -->





              </div>

            </div>

            

            <div role="tabpanel" class="tab-pane" id="tab3">

              <div class="profile-basic background-white p20">

                <form>

                  <div class="prof-img">

                    <div class="user-photo"> <a href="#"> <img src="img/user_img.jpg" alt="User Photo"> </a> </div>

                  </div>

                  <div class="prsn-info my-profl ne_ur_pr">

                    <div class="row">

                      <div class="usr_nm">

                        <h2>Marina jonson </h2>

                        <span>Volunteer</span> </div>

                      <div class="form-group col-lg-12">

                        <p>

                          <label>User Name </label>

                          <strong>marina j.</strong></p>

                      </div>

                      <!-- /.form-group -->

                      

                      <div class="form-group col-lg-12">

                        <p>

                          <label>City </label>

                          <strong>Indore</strong></p>

                      </div>

                      <!-- /.form-group -->

                      

                      <div class="form-group col-lg-12">

                        <p>

                          <label>Country </label>

                          <strong>India</strong></p>

                      </div>

                      <!-- /.form-group --> 

                      

                    </div>

                    <!-- /.row --> 

                  </div>

                </form>

              </div>

            </div>

            

            <div role="tabpanel" class="tab-pane" id="tab4">

              <div class="profile-basic background-white p20">

                <div class="profile-basic background-white p20">

                    <div class="edit_pro">

                    <h3 class="ed_pro">Change Password</h3>

                   

                      <div class="">

                      <div class="doc-prfl-left">

                            <div class="row">

                              <div class="form-group">

                                <label class="col-sm-4 col-md-4 col-lg-4 control-label">Old Password:</label>

                                <div class="col-sm-8 col-md-8 col-lg-8">

                                  <input type="password" class="form-control" id="oldpassword" name="oldpassword">

                                    <input type="hidden" class="form-control" name="mobile_no" id="mobile_no" value="9827563277">

                                    <span toggle="#oldpassword" class="pass-view fa fa-fw fa-eye-slash field-icon toggle-password"></span>

                                </div>

                              </div>

                              <div class="form-group">

                                <label class="col-sm-4 col-md-4 col-lg-4 control-label">New Password:</label>

                                <div class="col-sm-8 col-md-8 col-lg-8">

                                  <input type="password" class="form-control" name="newpassword" id="newpassword">

                                   <span toggle="#newpassword" class="pass-view fa fa-fw fa-eye-slash field-icon toggle-password"></span>

                                </div>

                              </div>

                              <div class="form-group">

                                <label class="col-sm-4 col-md-4 col-lg-4 control-label">Confirm Password:</label>

                                <div class="col-sm-8 col-md-8 col-lg-8">

                                  <input type="password" class="form-control" name="conpassword" id="conpassword">

                                  <span toggle="#conpassword" class="pass-view fa fa-fw fa-eye-slash field-icon toggle-password"></span>

                                </div>

                              </div>

                              

                              <div class="form-group mng-edit-prf-btn">

                          <label class="col-sm-4 col-md-4 col-lg-4 control-label"></label>

                          <div class="col-sm-8 col-md-8 col-lg-8">

                             <button class="btn btn-primary color-btn" name="change_password_btn" id="change_password_btn">Change Password</button>

                          </div>

                        </div>

                  

                          </div>

                          </div><!-- /.row -->

                        </div>

                        

                    </div>

                    </div>

               </div>

            </div>



              <div role="tabpanel" class="tab-pane" id="tab5">

                <div class="profile-basic background-white p20">



                 </div>

              </div>

            

          </div>

        </div>

      </div>

    </div></div>

  </div>

</div>

</section>

@endsection



@section('page_footer')

  

@endsection



@section('current_page_js')





<?php $user_id=Auth::user()->id; ?>

<script>

  $.ajaxSetup({

    headers: {'votive':'123456'}

  });

  var user_id = "<?php echo $user_id ?>";

  $(document).ready(function(){

    var formData = new FormData();

    formData.append('user_id',user_id);

    //alert(id);

    $.ajax({

      type: 'POST',

      url: "<?php echo url('/').'/api/getuserdetails'; ?>",

      data: formData,

        cache:false,

        contentType: false,

        processData: false,

        success: function(resultData){ 

          //console.log(resultData);

          if(resultData.status){

          $("#ufullname").html(resultData.response.first_name+' '+resultData.response.last_name);

          $("#uemail").html(resultData.response.email);

          $("#ucontact").html(resultData.response.contact_number);



          if(resultData.response.date_of_birth){

          $("#udob").html(resultData.response.date_of_birth);

        }else{

          $("#udob").html('N.A.');

        }



        if(resultData.response.gender){

          $("#ugender").html(resultData.response.gender);

        }else{

          $("#ugender").html('N.A.');

        }



        if(resultData.response.address){

          $("#upostal").html(resultData.response.address);

        }else{

          $("#upostal").html('N.A.');

        }



        if(resultData.response.profile_pic){          



            $("#usrImg").attr("src",resultData.response.profile_pic);

        }else{

          $("#usrImg").attr("src","https://votivelaravel.in/frameit/resources/front_assets/img/user_image.jpg");

        }



        $("#err_msg").html(result_alert);



        }



        },error: function(errorData) { 

          console.log(errorData);

          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';

          

          $("#err_msg").html(result_alert);

        }

      });  

    });





 



  // start get all user address list

  $.ajaxSetup({

    headers: {'votive':'123456'}

  });

  var user_id = "<?php echo $user_id ?>";

  $(document).ready(function(){

    var formData = new FormData();

    formData.append('user_id',user_id);

    var address_id = "<?php echo Auth::user()->ship_id; ?>";

    $.ajax({

      type: 'POST',

      url: "<?php echo url('/').'/api/getUserAddressList';?>",

      data: formData,

      enctype: 'multipart/form-data',

      cache:false,

      contentType: false,

      processData: false,

      success: function(resultData){ 

        //console.log(resultData.response);


        var yourArray = [];

        var yourArray1 = [];

        $.each(resultData.response.addressList, function(key, value) { 



        var addressBook = '<strong class="nic_nam">'+resultData.response.addressList[key].address_type+'</strong><div class="rig_s_on"><h5>'+resultData.response.addressList[key].fullname+'</h5><span class="Quennnn"><p>'+resultData.response.addressList[key].street_address+'</p><p>'+resultData.response.addressList[key].city+'</p><p>'+resultData.response.addressList[key].zip_code+'</p><p class="num_s">'+resultData.response.addressList[key].contact_number+'</p></span></div>';

          if(address_id==resultData.response.addressList[key].id){
              var addresslist = '<div class="add_address_two_bot_o"><h5>'+resultData.response.addressList[key].address_type+'</h5><span class="add_ons"><div class="col-md-2"><b>Name</b></div><div class="col-md-3"> <b>Phone Number</b></div><div class="col-md-3"> <b>Address</b></div><div class="col-md-3"> <b></b></div><div class="col-md-1"> <b></b></div></span><span class="add_botts"><div class="col-md-2"><b>'+resultData.response.addressList[key].fullname+'</b></div><div class="col-md-3"> <b>'+resultData.response.addressList[key].contact_number+'</b></div><div class="col-md-3"><b>'+resultData.response.addressList[key].street_address+','+resultData.response.addressList[key].city+','+resultData.response.addressList[key].zip_code+'</b></div><div class="col-md-3"><b class="poran">   Make default shipping Address | Make Default Billing Address </b></div><div class="col-md-1"><b class="orng"><a class="address_book_s"  href="javascript:void(0);" data-toggle="modal" data-target="#editaddress'+resultData.response.addressList[key].id+'">  Edit </a></b></div></span></div>';
           }else{

                var addresslist = '<div class="add_address_two_bot_o"><h5>'+resultData.response.addressList[key].address_type+'</h5><span class="add_ons"><div class="col-md-2"><b>Name</b></div><div class="col-md-3"> <b>Phone Number</b></div><div class="col-md-3"> <b>Address</b></div><div class="col-md-3"> <b></b></div><div class="col-md-1"> <b></b></div></span><span class="add_botts"><div class="col-md-2"><b>'+resultData.response.addressList[key].fullname+'</b></div><div class="col-md-3"> <b>'+resultData.response.addressList[key].contact_number+'</b></div><div class="col-md-3"><b>'+resultData.response.addressList[key].street_address+','+resultData.response.addressList[key].city+','+resultData.response.addressList[key].zip_code+'</b></div><div class="col-md-3"><b class="poran"> </b></div><div class="col-md-1"><b class="orng"><a class="address_book_s"  href="javascript:void(0);" data-toggle="modal" data-target="#editaddress'+resultData.response.addressList[key].id+'">  Edit </a></b></div></span></div>';
          }

          yourArray.push(addressBook); 

          yourArray1.push(addresslist);

        });



        $(".addressBookList").html(yourArray);

        $(".addressList").html(yourArray1);



      }

    });

  });

  // end get all user address list





  </script>

@foreach($addressList as $address)

  <!-- update address start -->
  
      <div class="modal fade pop_address" id="editaddress{{$address->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

        <div class="modal-dialog modal-dialog-centered" role="document">

          <div class="modal-content">        

            <div class="modal-body">

              <div class="address-form">



                <form  id="editShipAddress" action="#" enctype="multipart/form-data">

                  <input type="hidden" name="user_id" value="{{!empty(Auth::user()->id) ? Auth::user()->id:''}}">

                  <input type="hidden" name="ship_id" id="address_id" value="{{$address->id}}">

                  <div class="top-bar-container">

                    <div class="top-bar">

                      <div class="left-comp">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                          <span aria-hidden="true">&times; <span class="cls_btn">Close</span></span>

                        </button>        

                      </div>

                      <div class="title_fgh">Edit Address</div>

                      <div class="right-comp">

                        <input type="submit" class="DoneButton" name="upSbmtform" value="Done">

                        <!-- <a href="#" class="DoneButton">Done</a> -->

                      </div>

                    </div>

                    <div id="updatesignupResBox"></div>

                    <div class="bottom-comp"></div>

                  </div>

                  <div class="ioc_emoh">

                    <img class="cloud-icon" src="{{url('/').'/resources/front_assets'}}/img/icon_home.png">

                  </div>

                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Address Type</div>

                    

                    <input class="FormInput newAddressFormStyle" type="text" id="address_type" name="address_type" placeholder="Address Type" value="{{$address->address_type}}">

                  </div>

                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Full Name</div>

                    

                    <input class="FormInput newAddressFormStyle" type="text" id="fullname" name="fullname" placeholder="Full Name" value="{{$address->fullname}}">

                  </div>

                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Street Address</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="street_address" name="street_address" placeholder="Address"  

                    value="{{$address->street_address}}">

                  </div>           





                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Zip Code</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="zip_code" name="zip_code" placeholder="Zip Code" value="{{$address->zip_code}}">

                  </div>

                  <div class="newAddressInputWrapper country-select-container">

                    <div class="InputLabel">Country</div>

                    <select  id="countrylist" class="FormInput newAddressFormStyle" name="country">
                        @foreach($countries as $country)
                          <option  value="{{$country->id}}" <?php if($country->id==$address->country_id){ echo "selected"; } ?>>{{$country->name}}</option>
                        @endforeach

                    </select>

                  </div>

                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">State / Province</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="state" name="state" placeholder="State" value="{{$address->state}}">

                  </div>

                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">City</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="city" name="city" placeholder="City" value="{{$address->city}}">

                  </div>

                  <div class="newAddressInputWrapper">

                    <div class="InputLabel">Phone Number</div>

                    <input class="FormInput newAddressFormStyle" type="text" id="contact_number" name="contact_number" placeholder="Contact Number" value="{{$address->contact_number}}">

                  </div>             

                </form>

              </div>     

            </div>        

          </div>

        </div>

      </div>

      <!-- update address stop -->

      <script type="text/javascript">
        // start update ship address
$('#editShipAddress').validate({ 
  rules: {
    fullname: {
      required: true,
    },
    country:{
      required: true,
    },
    streetAddress: {
      required: true
    },
    city:{
      required: true,
    },
    state:{
      required: true,
    },
    address:{
      required: true,
    },
    contact_number: {
      required: true,
      digits:true,
      minlength : 8,
      maxlength : 13
    },
  },
  submitHandler: function(form) {
     //form.submit();
     update_address();
   }
 });

$.ajaxSetup({
  headers: {'votive':'123456'}
});

function update_address() {
    var $this = $('form#editShipAddress')[0];
    var formData = new FormData($this);

    $.ajax({

        type: 'POST',
        url: "<?php echo url('/').'/api/editShipAddress'; ?>",
        data: formData,
        enctype: 'multipart/form-data',
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData) { 


          if (resultData.status) {

            var result_alert = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> '+resultData.msg+'</div>';
            $("#updatesignupResBox").html(result_alert);
            $("#shipping_price").html('<span class="tot_l">Shipping Price</span><span class="tot_r">'+resultData.response.ship_price+' '+resultData.response.ship_currency+'</span>');
            // Your delay in milliseconds
            var delay = 1500; 
            setTimeout(function(){ window.location.href = "<?php echo url('/profile'); ?>"; }, delay);

          } else {

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> '+resultData.msg+'</div>';
            $("#updatesignupResBox").html(result_alert);
          }
        },
        error: function(errorData) { 

          var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
          $("#updatesignupResBox").html(result_alert);

        }
      });
  }
  // end update address 
      </script>

      @endforeach

  <script type="text/javascript">

    $("#emailchange").click(function () {

      var email = $('#eemail').val();



    $.ajaxSetup({

      headers: {'votive':'123456'}

    });

  var user_id = "<?php echo $user_id ?>";

  $(document).ready(function(){

    //var formData = new FormData();

    //formData.append('user_id',user_id);

    //alert(id);

    $.ajax({

      type: 'POST',

      url: "<?php echo url('/').'/changeemail'; ?>",

      //data: "user_id="+user_id+"&email="+email,

      data: {

        "_token": "{{ csrf_token() }}",

        "user_id": user_id,

        "email" : email

        },

       

        success: function(resultData){ 

          //console.log(resultData);

        if(resultData.status=='success'){

          alert("Email has updated successfully!");

        }else{

          alert("Email id already exists");

        }

      }  

    });

  }); 

  });  

  </script>

  <script type="text/javascript">

    $("#namechange").click(function () {

      var fullname = $('#efullname').val();

        $.ajaxSetup({

      headers: {'votive':'123456'}

    });

  var user_id = "<?php echo $user_id ?>";

  $(document).ready(function(){

    //var formData = new FormData();

    //formData.append('user_id',user_id);

    //alert(id);

    $.ajax({

      type: 'POST',

      url: "<?php echo url('/').'/changefullname'; ?>",

      //data: "user_id="+user_id+"&email="+email,

      data: {

        "_token": "{{ csrf_token() }}",

        "user_id": user_id,

        "fullname" : fullname

        },

       

        success: function(resultData){ 

          //console.log(resultData);

        if(resultData.status=='success'){

          alert("Fullname has updated successfully!");

        }else{

          alert("Fullname has updated successfully!");

        }

      }  

    });

  }); 

  });  

  </script>

  <script type="text/javascript">

    $("#contactnumberchange").click(function () {

      var contact = $('#econtact').val();

       $.ajaxSetup({

      headers: {'votive':'123456'}

    });

  var user_id = "<?php echo $user_id ?>";

  $(document).ready(function(){

    //var formData = new FormData();

    //formData.append('user_id',user_id);

    //alert(id);

    $.ajax({

      type: 'POST',

      url: "<?php echo url('/').'/changecontactnumber'; ?>",

      //data: "user_id="+user_id+"&email="+email,

      data: {

        "_token": "{{ csrf_token() }}",

        "user_id": user_id,

        "contact" : contact

        },

       

        success: function(resultData){ 

          //console.log(resultData);

        if(resultData.status=='success'){

          alert("Contact Number has updated successfully!");

        }else{

          alert("Contact Number has updated successfully!");

        }

      }  

    });

  }); 

  });     

  </script>


    <script>
$(document).ready(function(){

 $('#upload_form').on('submit', function(event){
  event.preventDefault();
    var formData = new FormData(this);
    //var file_data = $("#file-input").prop("files")[0];  

    //formData.append("file", file_data);

    $.ajaxSetup({

     headers: {'votive':'123456',
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          }

    });
  $.ajax({
   url:"<?php echo url('/').'/api/updateprofile'; ?>",
   method:"POST",
   data:formData,
   dataType:'JSON',
   contentType: false,
   cache: false,
   processData: false,
   success:function(data)
   {
    //$('#message').css('display', 'block');
    //$('#message').html(data.message);
    $('#msg1').html(data.msg);
    //$('#uploaded_image').html(data.uploaded_image);
   }
  })
 });
});

</script>


  <script>
$(document).ready(function(){

 //$('#upload_form').on('submit', function(event){
$("#file-input").change(function() { 
  event.preventDefault();
  var formData = new FormData();
        var file_data = $("#file-input").prop("files")[0];  
        
        formData.append("file", file_data);

   $.ajaxSetup({

      headers: {'votive':'123456',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }

    });
  $.ajax({
   url:"<?php echo url('/').'/changeprofilepic'; ?>",
   method:"POST",
   data:formData,
   dataType:'JSON',
   contentType: false,
   cache: false,
   processData: false,
   success:function(data)
   {
    $('#message').css('display', 'block');
    $('#message').html(data.message);
    $('#message').addClass(data.class_name);
    $('#uploaded_image').html(data.uploaded_image);
   }
  })
 });

});
</script>

<script type="text/javascript">
$.ajaxSetup({

    headers: {'votive':'123456'}

  });

  var user_id = "<?php echo $user_id ?>";

  $(document).ready(function(){

    var formData = new FormData();

    formData.append('user_id',user_id);

    $.ajax({

      type: 'POST',

      url: "<?php echo url('/').'/api/getUserAddressList';?>",

      data: formData,

      enctype: 'multipart/form-data',

      cache:false,

      contentType: false,

      processData: false,

      success: function(resultData){ 

        //console.log(resultData.response);

        var yourArray = [];

        $.each(resultData.response.addressList, function(key, value) { 



        var addressBook = '<div class="main_pop_s_m_one"><label class="container_on">'+resultData.response.addressList[key].address_type+'<input type="radio" checked="checked" name="address_id" value="'+resultData.response.addressList[key].id+'"><span class="checkmark"></span></label><div class="main_pop_s_m_one_bt"><span><strong>'+resultData.response.addressList[key].fullname+'</strong><a href="#" onclick="editAddressById('+resultData.response.addressList[key].id+')" data-id="'+resultData.response.addressList[key].id+'">Edit</a></span><p>'+resultData.response.addressList[key].street_address+','+resultData.response.addressList[key].city+','+resultData.response.addressList[key].zip_code+'</p><p>'+resultData.response.addressList[key].contact_number+'</p></div></div>';

          yourArray.push(addressBook); 

        });



        $(".addressBookList1").html(yourArray);



      }

    });

  });

     $('#getAddressId').click(function() {

        var addressId = $('input[name=address_id]:checked').val();

        var user_id = "<?php echo Auth::user()->id; ?>";

        var formData = new FormData();

        formData.append('address_id',addressId);

        formData.append('user_id',user_id);

        $.ajax({

            type: 'POST',

            url: "<?php echo url('/').'/api/createdefaultaddress'; ?>",

            data: formData,

            enctype: 'multipart/form-data',

            cache:false,

            contentType: false,

            processData: false,

            success: function(resultData){  

              //var showAddress = '<strong>'+resultData.response.addressList.fullname+'</strong> <p>'+resultData.response.addressList.street_address+','+resultData.response.addressList.city+','+resultData.response.addressList.zip_code+'</p>';
              //alert("Default Address has been updated successfully");
              window.location.href = "<?php echo url('/profile'); ?>";
             // $(".add_address_two" ).tabs( { active: 2 } );

            }

          }); 

    });
  </script>

  @endsection











<!-- <script type="text/javascript">

  $(document).ready(function() {

     

    $("#show_chat_sec").click(function () {

      $(this).closest(".messaging").addClass('openchat');

    });

   

   $(".back_m_l a").click(function () {

      $(this).closest(".messaging").removeClass('openchat');

   });  

    

  });     

</script> -->





<div id="changepassword" class="modal fade show" role="dialog">

    <div class="modal-dialog modal-dialog-centered">      

      <div class="modal-content" style="display: inline-table;">        

        <div class="modal-body">

          <div class="logreg-forms">

            <div id="signupResBox"></div>

            <form class="sign_form" id="change_password" method="post" action="#" novalidate="novalidate">

             <button type="button" class="close" data-dismiss="modal">×</button>  

             <input type="hidden" name="_token" value="aOCfaUkW4DXSuWlO1pfpzSHPDeusW6Nhz0T9Ic3c">             <div class="form-label">Change Password</div>

             <!-- <h4 class="bel_hd"></h4> -->

             <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

             <div class="in_firs">

              <input class="FormInput" type="text" name="current_password" id="current_password" placeholder="Current Password">

            </div>

            <div class="in_firs">

              <input class="FormInput" type="text" name="new_password" id="new_password" placeholder="New Password">

            </div>

            <div class="in_firs">

              <input class="FormInput" type="text" name="new_confirm_password" id="new_confirm_password" placeholder="Confirm Password">

            </div>

            

            <div class="button_cont">

              <input class="btn_Submit" type="submit" value="Change Password">

            </div>

          </form>

        </div>

      </div>    

    </div>

  </div>

</div>


 <div class="modal fade select_form_address" id="showAddressBooknew" role="dialog">

    <div class="modal-dialog select_form_addressll">

    

      <!-- Modal content-->

      <div class="modal-content select_form_address_n">

        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">&times; Close</button>

          <h4 class="modal-title">Address Book</h4>

          <a href="#"></a>

        </div>

        <div class="modal-body select_form_address_nn">

         

        <div class="main_pop_s_m addressBookList1">

        

        </div>



        </div>

        <div class="modal-footer">

         

          <button type="submit" class="btn btn-default Sel_add Rig_seed" id="getAddressId">Select</button>

        </div>

      </div>

      

    </div>

  </div>





