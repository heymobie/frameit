@extends('user.layout.layout')
@section('title', 'User')


@section('current_page_css')

@endsection


@section('current_page_js')

<script type="text/javascript">
  //start get faq list
  $.ajaxSetup({
    headers: {'votive':'123456'}
  });
  
  $(document).ready(function(){
    var formData = new FormData();
    $.ajax({
        type: 'POST',
        url: "<?php echo url('/').'/api/getAllFaqList'; ?>",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(resultData){ 
          console.log(resultData);
          if(resultData.status){

            var faqArray = [];
            var j=1;
            $.each(resultData.response.faqList, function( i, l ){

              var faqs = '<div class="card"><div class="card-header bg-warning text-white"><div class="card-link" data-toggle="collapse" href="#collapse'+resultData.response.faqList[i].id+'">'+resultData.response.faqList[i].question+'</div></div><div id="collapse'+resultData.response.faqList[i].id+'" class="collapse" data-parent="#accordion"><div class="card-body">'+resultData.response.faqList[i].answer+'</div></div></div><br/>';

              faqArray.push(faqs);
              j++;
            });
            $(".faqList").html(faqArray);

          }else{

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> No Faq List Found.</div>';
            $("#err_msg").html(result_alert);

          }

        },error: function(errorData) { 

            var result_alert = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Opps!</strong> Some internal issue occured. Please refresh page and try again.</div>';
            $("#err_msg").html(result_alert);

        }
    });
  });
  //end get faq list
</script>
@endsection

@section('tag_body')
<body>
@endsection

@section('content')


 <section id="" class="section selectFramStyles">

    <div class="container">
     <!--  <div class="selectFrameSec">
        <h2>3/3</h2>        
      </div> -->
      <div class="stp_one">
        <div class="content">
          <div class="bg-layer">
            <div class="bg-circle"></div>
          </div>
          <form id="form1" class="log_form" action="{{url('/')}}/step3_store" method="POST">
             @csrf
            <div class="form-label">Hey  <span class="wave">👋</span></div>
            <div class="in_firs">
              <input class="FormInput" type="email" name="email" id="email" placeholder="What's your email?" value="" required="">
            </div>
            <div class="button_cont">            
              <input class="btn_Submit" id="emailbtn" name="submit" type="submit" value="Pick Photos">
            </div>
          </form>
        </div>
      </div>
   <!--    <div class="framesTabing">
        <div class="sectionWraper"></div>
      </div> -->
    </div>
  </section> 
@endsection