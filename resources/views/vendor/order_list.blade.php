@extends('vendor.layout.layout')
@section('title', 'Order List')

@section('current_page_css')
<link rel="stylesheet" href="{{url('/')}}/resources/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{url('/')}}/resources/assets/css/bootstrap-toggle.min.css">
@endsection

@section('current_page_js')
<script src="{{url('/')}}/resources/assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{url('/')}}/resources/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="{{url('/')}}/resources/assets/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#order_list').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<script type="text/javascript">
 function delete_order_item(order_id){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
   type: 'POST',
   url: "<?php echo url('/admin/delete_order_item'); ?>",
   enctype: 'multipart/form-data',
   data:{order_id:order_id,'_token':'<?php echo csrf_token(); ?>'},
     beforeSend:function(){
       return confirm("Are you sure you want to delete this order item ?");
     },
     success: function(resultData) { 
       console.log(resultData);
       var obj = JSON.parse(resultData);
       if (obj.status == 'success') {
         setTimeout(function() {
            $('#success_message').fadeOut("slow");
          }, 2000 );
          $("#row" + frame_id).remove();
       }
     },
     error: function(errorData) {
      console.log(errorData);
      alert('Please refresh page and try again!');
    }
  });
}
</script>
<script>
  $('select').on("change", function() {
    var order_id = $(this).attr('data-id'); 
    var status = this.value;
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "<?php echo url('/vendor/change_order_status'); ?>",
      data: {'status': status, 'order_id': order_id},
      success: function(data){
        $('#success_message').fadeIn().html(data.success);
        setTimeout(function() {
          $('#success_message').fadeOut("slow");
        }, 2000 );
      },
      error: function(errorData) {
      console.log(errorData);
      alert('Please refresh page and try again!');
    }
    });
  })
</script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
   <div class="container-fluid">
    <div class="row mb-2">
     <div class="col-sm-6">
      <h1 class="m-0 text-dark">Orders</h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
       <li class="breadcrumb-item"><a href="#">Home</a></li>
       <li class="breadcrumb-item active">Order List</li>
     </ol>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
 <div class="container-fluid">
 <p style="display: none;" id="success_message" class="alert alert-success"></p>
  @if ($errors->any())
  <div class="alert alert-danger">
   <ul>
     @foreach ($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
   </ul>
 </div>
 @endif

 @if(Session::has('message'))
 <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
 @endif

 @if(Session::has('error'))
 <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
 @endif

 <!-- Small boxes (Stat box) -->
 <table id="order_list" class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>SNo.</th>
      <th>Frame Name</th>
      <!--<th>Frame Image</th>-->
      <th>Username</th>
      <th>Order ID</th>
      <th>Total Tile</th>
      <th>Total Price</th>
      <th>Delivery Date</th>
      <th>Order Status</th>
      
      <th>Create Date</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @if(!$order_list->isEmpty())
    <?php $i=1; ?>
    @foreach($order_list as $arr) 
    <tr id="row{{$arr->id}}">
      <td>{{$i}}</td>
      <td>{{$arr->title}}</td>
      <!--<td>
        <img style="width: 150px;" src="{{url('/')}}/public/uploads/frame_images/{{$arr->images}}">
      </td>-->
      <td>{{$arr->first_name}} {{$arr->last_name}}</td>
      <td>{{$arr->id}}</td>
      <td>{{$arr->total_tile}}</td>
      <td>{{$arr->total_price}} {{$arr->currency}}</td>
      <td>{{$arr->delivery_date}}</td>
      <td>
        <select class="form-control" name="order_status" data-id="{{$arr->id}}">
          <option value="pending" {{ $arr->order_status == 'pending' ? 'selected' : '' }}>PENDING</option>
          <option value="accept" {{ $arr->order_status == 'accept' ? 'selected' : '' }}>ACCEPT</option>
          <option value="processing" {{ $arr->order_status == 'processing' ? 'selected' : '' }}>PROCESSING</option>
          <option value="shipped" {{ $arr->order_status == 'shipped' ? 'selected' : '' }}>SHIPPED</option>
          <option value="deliverd" {{ $arr->order_status == 'deliverd' ? 'selected' : '' }}>DELIVERED</option>
          <!--<option value="returned" {{ $arr->order_status == 'returned' ? 'selected' : '' }}>RETURNED</option>
          <option value="canceled" {{ $arr->order_status == 'canceled' ? 'selected' : '' }}>CANCELED</option>-->
        </select>
      </td>
      <!--<td>
        <input data-id="{{$arr->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $arr->status ? 'checked' : '' }}>
      </td>-->
      <td>{{(!empty($arr->created_at) ? date('d-m-Y H:i A',strtotime($arr->created_at)) : 'N/A')}}</td>
      <td>

        <a href="{{url('/vendor/view_order_details')}}/{{base64_encode($arr->id)}}">View Details</a>

        <!--<a href="javascript:void(0)" onclick="delete_order_item('<?php echo $arr->id; ?>');"><i class="fa fa-trash" aria-hidden="true" alt="order" title="order"></i></a>-->

      </td>
    </tr>
    <?php $i++; ?>
    @endforeach
    @endif

  </tbody>
</table>   
<!-- /.row -->


</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection         