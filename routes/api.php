<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::group(['middleware' => 'checkHeader'], function () {
	/*Gmail and FB login*/
	//Route::post('/sociallogin', 'Api\SocialAuthGoogleController@loginRegister');

	/*login*/
	Route::post('login', 'Api\ApiLoginController@login');

	/*registration*/
	Route::post('user_register', 'Api\ApiLoginController@register');
	Route::post('registration', 'Api\UserController@user_registration');

	/*otp verification*/
	Route::post('verifyotp', 'Api\UserController@verifyotp');

	/*forgot password*/
	Route::post('forgotpassword', 'Api\UserController@forgotpassword');

	/*change password*/
	Route::post('changepassword', 'Api\UserController@changepassword');
	/*reset password*/
	Route::post('Resetpassword','Api\UserController@Resetpassword');
	/*resend otp*/
	Route::post('resendotp', 'Api\UserController@resendotp');

	/*update user profile*/
	Route::post('updateprofile', 'Api\UserController@updateprofile');

	/*update user profile*/
	Route::post('getuserdetails', 'Api\UserController@getuserdetails');

	/*block user*/
	Route::post('blockuser', 'Api\UserController@blockuser');

	/*search user*/
	Route::post('searchuser', 'Api\UserController@searchuser');

	/*get user list*/
	Route::get('getuserlist', 'Api\UserController@getuserlist');

	/*get block user list*/
	Route::post('getblockuserlist', 'Api\UserController@getblockuserlist');
	
	/*follow user request*/
	Route::post('sendfollowrequest', 'Api\FollowController@sendfollowrequest');	

	/*get followers list*/
	Route::post('followerlist', 'Api\FollowController@getfollowerslist');	

	/*get following list*/
	Route::post('followinglist', 'Api\FollowController@getfollowinglist');

	/*get follow request*/
	Route::post('getfollowrequests', 'Api\FollowController@getFollowRequest');

	/*create group*/
	Route::post('creategroup', 'Api\GroupController@creategroup');	

	/*create group*/
	Route::post('getgroupdetails', 'Api\GroupController@getgroupdetails');	

	/*add group member*/
	Route::post('addgroupmember', 'Api\GroupController@addgroupmember');		
	/*remove group member*/
	Route::post('removegroupmember', 'Api\GroupController@removegroupmember');	

	/*exit group*/
	Route::post('exitgroup', 'Api\GroupController@exitgroup');

	/*group users list*/
	Route::post('groupuserlist', 'Api\GroupController@groupuserlist');

	/*get friend list*/
	Route::post('getfriendlist', 'Api\FriendController@getfriendlist');

	/*private dob*/
	Route::post('privatedob', 'Api\UserController@privatedob');

	/*private dob*/
	Route::post('checkuserblockstatus', 'Api\UserController@checkuserblockstatus');

	/*create feed*/
	Route::post('createfeed', 'Api\FeedController@createfeed');
	
	/*edit feed text*/
	Route::post('editfeedtext', 'Api\FeedController@editfeedtext');

	/*get user feed list*/
	Route::post('getUserFeedlist', 'Api\FeedController@getUserFeedlist');	

	/*delete feed*/
	Route::post('deletefeed', 'Api\FeedController@deletefeed');	

	/*get user feed list*/
	Route::post('getFollowFeedlist', 'Api\FeedController@getFollowFeedlist');

	/*get user image gallarey list*/
	Route::post('imagegallery', 'Api\FeedController@getImageGallarey');

	/*get user video gallarey list*/
	Route::post('videogallery', 'Api\FeedController@getVideoGallarey');

	/*get notification listing*/
	Route::post('getnotificationlisting', 'Api\NotificationController@getNotificationListing');
	
	Route::post('/forgot_password','Api\ApiLoginController@forgot_password');	

	/*get notification listing*/
	Route::post('/social_access','Api\ApiLoginController@social_access');

	/*get feed listing*/
	Route::post('/getAllFeedList','Api\FeedController@getAllFeedList');	

	/*get Video listing*/
	Route::post('/getAllVideoList','Api\FeedController@getAllVideoList');

	/*get Frame listing*/
	Route::post('/getAllFrameList','Api\ApiFrameController@getAllFrameList');

	Route::post('/getFrameListData','Api\ApiFrameController@getFrameListData');

	/*get Frame listing*/
	Route::post('/getAllFrameListbyid','Api\ApiFrameController@getAllFrameListbyid');

	/*get Frame listing*/
	Route::post('/getAllFramesizebycatid','Api\ApiFrameController@getAllFramesizebycatid');

	/*get Category listing*/
	Route::post('/getAllCategoryList','Api\ApiFrameController@getAllCategoryList');

	/*get country listing*/
	Route::post('/getAllCountryList','Api\ApiShipAddressController@getAllCountryList');

	/*create shipping address listing*/
	Route::post('/getUserAddressList','Api\ApiShipAddressController@getUserAddressList');

	/*create shipping address*/
	Route::post('/createShipAddress','Api\ApiShipAddressController@createShipAddress');

	/*edit shipping address*/
	Route::post('/editShipAddress','Api\ApiShipAddressController@editShipAddress');

	/*set default shipping address*/
	Route::post('/createdefaultaddress','Api\ApiShipAddressController@createdefaultaddress');
	
	/*create order listing*/
	Route::post('/createOrder','Api\ApiCheckoutController@createOrder');

	/*get order listing*/
	Route::post('/getAllUserOrderList','Api\ApiOrderController@getAllUserOrderList');

	/*get order details listing*/
	Route::post('/getUserOrderDetails','Api\ApiOrderController@getUserOrderDetails');

	/*token generate*/
	Route::post('createBraintreeToken', 'Api\ApiPaymentController@createBraintreeToken');

	/*create payment*/
	Route::post('createPayment', 'Api\ApiPaymentController@createPayment');

	/* save payment detail*/
	Route::post('/savePaymentDetail','Api\ApiPaymentController@savePaymentDetail');

	/* get faq listing */
	Route::post('/getAllFaqList','Api\ApiCmsController@getAllFaqList');

	/* get cms page listing */
	Route::post('/getCmsPageList','Api\ApiCmsController@getCmsPageList');

	/* create card details */
	Route::post('/saveCardDetails','Api\ApiPaymentController@saveCardDetails');

	/* update card details */
	Route::post('/updateCardDetails','Api\ApiPaymentController@updateCardDetails');

	/* get card listing */
	Route::post('/getCardDetails','Api\ApiPaymentController@getCardDetails');

	/* get user coupon listing */
	Route::post('/getUserCouponList','Api\ApiCouponController@getUserCouponList');

	/* apply coupon code */
	Route::post('/userApplyCouponCode','Api\ApiCouponController@userApplyCouponCode');

	/* get user wallet amount */
	Route::post('/getUserWalletAmount','Api\ApiCouponController@getUserWalletAmount');

	/* get user referral link */
	Route::post('/getUserReferralLink','Api\ApiCouponController@getUserReferralLink');

	/* create wishlist  */
	Route::post('/createWishlist','Api\ApiCheckoutController@createWishlist');

	/* get user wishlist  */
	Route::post('/getAllUserWishList','Api\ApiCheckoutController@getAllUserWishList');

	/* get user wishlist details */
	Route::post('/getUserWishlistDetails','Api\ApiCheckoutController@getUserWishlistDetails');

	/* get user address details */
	Route::post('/getUserAddressById','Api\ApiShipAddressController@getUserAddressById');

	/* get user reward history */
	Route::post('/getUserRewardHistory','Api\ApiCouponController@getUserRewardHistory');

	/* get user reward history */
	Route::post('/getUserPromoRewardCalculation','Api\ApiCouponController@getUserPromoRewardCalculation');


	/*get all home listing*/
	Route::post('/getAllList','Api\FeedController@getAllList');	

});