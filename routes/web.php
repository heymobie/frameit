<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route for admin with prefix "admin" declared one time in group*/
Route::group(['prefix' => 'admin',  'middleware' => 'admin'], function() {
	Route::get('/dashboard','Admin\DashboardController@index');

	Route::get('change_username', 'Admin\ChangePasswordController@change_username');
	Route::post('update_username', 'Admin\ChangePasswordController@update_username');

	Route::get('change_password', 'Admin\ChangePasswordController@index');
	Route::post('update_password', 'Admin\ChangePasswordController@store');

	Route::get('/customer_earning_list','Admin\ReferralController@customer_earning_list');

	Route::get('/order_list','Admin\OrderController@order_list');
	Route::get('/view_order_details/{id}','Admin\OrderController@view_order_details');
	Route::get('/change_order_status','Admin\OrderController@change_order_status');
	//assign vendor
	Route::get('/assignordertovendor','Admin\OrderController@assignordertovendor');
	//end assign vendor
	Route::post('/delete_order_item','Admin\OrderController@delete_order_item');

	Route::get('/transaction_list','Admin\PaymentController@transaction_list');
	Route::get('/change_transaction_status','Admin\PaymentController@change_transaction_status');
	Route::post('/delete_transaction','Admin\PaymentController@delete_transaction');

	Route::get('/add_referral_commission','Admin\ReferralController@add_referral_commission');
	Route::post('/submit_referral_commission','Admin\ReferralController@submit_referral_commission');
	Route::get('/edit_referral_commission/{id}','Admin\ReferralController@edit_referral_commission');
	Route::post('/update_referral_commission','Admin\ReferralController@update_referral_commission');
	Route::get('/referral_commission_list','Admin\ReferralController@referral_commission_list');
	Route::get('/change_commission_status','Admin\ReferralController@change_commission_status');
	Route::post('/delete_referral_commission','Admin\ReferralController@delete_referral_commission');

	Route::get('/add_coupon','Admin\CouponController@add_coupon');
	Route::post('/submit_coupon','Admin\CouponController@submit_coupon');
	Route::get('/edit_coupon/{id}','Admin\CouponController@edit_coupon');
	Route::post('/update_coupon','Admin\CouponController@update_coupon');
	Route::get('/coupon_list','Admin\CouponController@coupon_list');
	Route::get('/change_coupon_status','Admin\CouponController@change_coupon_status');
	Route::post('/delete_coupon_code','Admin\CouponController@delete_coupon_code');

	Route::get('/add_faq_mgmt','Admin\FaqmgmtController@add_faq_mgmt');
	Route::post('/submit_faq_mgmt','Admin\FaqmgmtController@submit_faq_mgmt');
	Route::get('/edit_faq_mgmt/{id}','Admin\FaqmgmtController@edit_faq_mgmt');
	Route::post('/update_faq_mgmt','Admin\FaqmgmtController@update_faq_mgmt');
	Route::get('/faq_mgmt_list','Admin\FaqmgmtController@faq_mgmt_list');
	Route::get('/change_faq_status','Admin\FaqmgmtController@change_faq_status');
	Route::post('/delete_faq_mgmt','Admin\FaqmgmtController@delete_faq_mgmt');	

	Route::get('/add_cms_mgmt','Admin\CmsmgmtController@add_cms_mgmt');
	Route::post('/submit_cms_mgmt','Admin\CmsmgmtController@submit_cms_mgmt');
	Route::get('/edit_cms_mgmt/{id}','Admin\CmsmgmtController@edit_cms_mgmt');
	Route::post('/update_cms_mgmt','Admin\CmsmgmtController@update_cms_mgmt');
	Route::get('/cms_mgmt_list','Admin\CmsmgmtController@cms_mgmt_list');
	Route::get('/change_cms_status','Admin\CmsmgmtController@change_cms_status');
	Route::post('/delete_cms_mgmt','Admin\CmsmgmtController@delete_cms_mgmt');	

	Route::get('/add_tracking_code','Admin\ShipmentTrackController@add_tracking_code');
	Route::post('/submit_tracking_code','Admin\ShipmentTrackController@submit_tracking_code');
	Route::get('/edit_tracking_code/{id}','Admin\ShipmentTrackController@edit_tracking_code');
	Route::post('/update_tracking_code','Admin\ShipmentTrackController@update_tracking_code');
	Route::get('/tracking_code_list','Admin\ShipmentTrackController@tracking_code_list');
	Route::get('/change_tracking_code_status','Admin\ShipmentTrackController@change_tracking_code_status');
	Route::post('/delete_tracking_code','Admin\ShipmentTrackController@delete_tracking_code');

	Route::get('/add_shipping_price','Admin\ShippingController@add_shipping_price');
	Route::post('/submit_shipping_price','Admin\ShippingController@submit_shipping_price');
	Route::get('/edit_shipping_price/{id}','Admin\ShippingController@edit_shipping_price');
	Route::post('/update_shipping_price','Admin\ShippingController@update_shipping_price');
	Route::get('/shipping_price_list','Admin\ShippingController@shipping_price_list');
	Route::get('/change_shipping_price_status','Admin\ShippingController@change_shipping_price_status');
	Route::post('/delete_shipping_price','Admin\ShippingController@delete_shipping_price');

	Route::get('/add_frame','Admin\FrameController@add_frame');
	Route::post('/submit_frame','Admin\FrameController@submit_frame');
	Route::post('/update_frame','Admin\FrameController@update_frame');
	Route::get('/edit_frame/{id}','Admin\FrameController@edit_frame');
	Route::get('/frame_list','Admin\FrameController@frame_list');
	Route::get('/change_frame_status','Admin\FrameController@change_frame_status');
	Route::post('/delete_frame','Admin\FrameController@delete_frame');

	Route::get('/add_video','Admin\PramotionController@add_video');
	Route::post('/submit_video','Admin\PramotionController@submit_video');
	Route::post('/update_video','Admin\PramotionController@update_video');
	Route::get('/edit_video/{id}','Admin\PramotionController@edit_video');
	Route::get('/pramotion_video_list','Admin\PramotionController@pramotion_video_list');
	Route::get('/change_video_status','Admin\PramotionController@change_video_status');
	Route::post('/delete_pramote_video','Admin\PramotionController@delete_pramote_video');

	Route::get('/add','Admin\FeedController@add');
	Route::post('/submit_feed','Admin\FeedController@submit_feed');
	Route::post('/update_feed','Admin\FeedController@update_feed');
	Route::get('/edit/{id}','Admin\FeedController@edit');
	Route::get('/feed_list','Admin\FeedController@feed_list');
	Route::get('/change_feed_status','Admin\FeedController@change_feed_status');
	Route::post('/delete_feed','Admin\FeedController@delete_feed');

	Route::get('/image_list/{id}','Admin\UserController@image_list');
	Route::post('/delete_image','Admin\UserController@delete_image');

	Route::get('/video_list/{id}','Admin\UserController@video_list');
	Route::post('/delete_video','Admin\UserController@delete_video');

	Route::get('/add_user','Admin\UserController@add_user');
	Route::post('/submit_user','Admin\UserController@submit_user');
	Route::post('/update_user','Admin\UserController@update_user');
	Route::get('/edit_user/{id}','Admin\UserController@edit_user');
	Route::get('/user_list','Admin\UserController@user_list');
        
	Route::get('/user_profile/{id}','Admin\UserController@user_profile');
	Route::get('/change_user_status','Admin\UserController@change_user_status');
	Route::post('/delete_user','Admin\UserController@delete_user');
	Route::get('change_user_password/{id}', 'Admin\UserController@change_user_password');
	Route::post('update_user_password', 'Admin\UserController@update_user_password');

	Route::get('/category_list','Admin\CategoryController@category_list');
	Route::get('/add_category','Admin\CategoryController@add_category');
	Route::post('/submit_category','Admin\CategoryController@submit_category');
	Route::get('/edit_category/{id}','Admin\CategoryController@edit_category');	
	Route::post('/update_category','Admin\CategoryController@update_category');

	Route::get('/showBanner','Admin\Banner@show_banner');
	Route::get('/addBanner','Admin\Banner@addBanner');
	Route::post('/submitBanner','Admin\Banner@submit_banner');
	Route::get('/editBanner/{id}','Admin\Banner@editBanner');
	Route::post('/updateBanner/{id}','Admin\Banner@updateBanner');
	Route::post('/deleteBanner','Admin\Banner@deleteBanner');
	Route::get('/change_banner_status','Admin\Banner@change_banner_status');


    Route::get('/showIcons','Admin\Icon@show_icons');
	Route::get('/addIcons','Admin\Icon@addIcons');
	Route::post('/submitIcons','Admin\Icon@submit_icons');
	Route::get('/editIcons/{id}','Admin\Icon@editIcons');
	Route::post('/updateIcons/{id}','Admin\Icon@updateIcons');
	Route::post('/deleteIcon','Admin\Icon@deleteIcons');
	Route::get('/change_icon_status','Admin\Icon@change_icon_status');

    Route::get('/showFeedTitle','Admin\FeedTitle@show_feed_name');
	Route::get('/addFeedTitle','Admin\FeedTitle@addFeedTitle');
	Route::post('/submitFeedTitle','Admin\FeedTitle@submit_feed_title');
	Route::get('/editFeedTitle/{id}','Admin\FeedTitle@editFeed_title');
	Route::post('/updateFeedTitle/{id}','Admin\FeedTitle@updateFeedTitle');
	Route::post('/deleteFeedTitle','Admin\FeedTitle@deleteFeedTitle');
	Route::get('/change_feed_status','Admin\FeedTitle@change_feedtitle_status');

	//vendor
	Route::get('/vendor/add_user','Admin\VendorController@add_user');
	Route::post('/vendor/submit_user','Admin\VendorController@submit_user');
	Route::post('/vendor/update_user','Admin\VendorController@update_user');
	Route::get('/vendor/edit_user/{id}','Admin\VendorController@edit_user');
	Route::get('/vendor/user_list','Admin\VendorController@user_list');
	Route::get('/vendor/user_profile/{id}','Admin\VendorController@user_profile');
	Route::get('/vendor/change_user_status','Admin\VendorController@change_user_status');
	Route::post('/vendor/delete_user','Admin\VendorController@delete_user');
	Route::get('/vendor/change_user_password/{id}', 'Admin\VendorController@change_user_password');
	Route::post('/vendor/update_user_password', 'Admin\VendorController@update_user_password');


	Route::get('/logout','Admin\DashboardController@logout');
});
//vendor route start
Route::get('/vendorlogin','Admin\VendorController@vendorlogin');
Route::post('/vendor/submit_login','Admin\VendorController@submit_login');
Route::get('/vendor/dashboard','Admin\VendorController@dashboard');
Route::get('/vendor/logout','Admin\VendorController@logout');

Route::get('/vendor/order_list','Admin\VendorController@order_list');
Route::get('/vendor/view_order_details/{id}','Admin\VendorController@view_order_details');
Route::get('/vendor/change_order_status','Admin\VendorController@change_order_status');
Route::get('/newhome','HomeController@newhome');
Route::get('/termsofservice','PageController@terms_of_service');
Route::get('/aboutus','PageController@about_us');
Route::post('/getAllSizeFrameListbycategory','HomeController@getAllSizeFrameListbycategory');
//vendor route end

/* Route for guest */
Route::group(['middleware' => 'guest'], function() {
	Route::get('/admin_login','Admin\LoginController@index');

	Route::post('/submit_login','Admin\LoginController@submit_login');

	Route::get('/forgot_password','Admin\LoginController@forgot_password');
	Route::post('/forgot_password_submit','Admin\LoginController@forgot_password_submit');

	Route::get('/login_one','LoginController@login_one');
	Route::get('/login_two','LoginController@login_two');
	Route::get('/login_three','LoginController@login_three');
	//Route::get('/login','LoginController@login');
	//Route::post('/login','LoginController@login');

	Route::post('/weblogin','LoginController@weblogin');

	Route::post('/register_name','LoginController@register_name');
	Route::post('/register_email','LoginController@register_email');

    Route::get('/signup/{id}','LoginController@signup');

});

Route::group(['middleware' => 'user'], function() {
	
	
	Route::get('/logout','HomeController@logout');
	Route::get('/dashboard','HomeController@user_dashboard');
	Route::get('/profile','HomeController@user_profile');
	Route::get('/edit-profile','HomeController@edit_profile');
	Route::get('/my_order','HomeController@edit_profile');
	Route::get('/save_frame_list','HomeController@my_order');
	Route::get('/change-password','HomeController@change_password');
	Route::get('/faq','HomeController@fqa_page');
	
	Route::get('/crop_image','HomeController@crop_image');
	Route::post('/createOrder','HomeController@createOrder');

	Route::post('/sendrefferalcode','HomeController@sendrefferalcode');

	Route::get('/save_frame_mo','HomeController@save_frame_mo');
	Route::get('/save_rotation','HomeController@save_rotation');
	Route::get('/save_frame_order/{id}','HomeController@save_frame_order');

	Route::get('/save_frame_detail/{id}','HomeController@save_frame_detail');


	Route::get('/save_frame','HomeController@save_frame');
	Route::get('/save_image_quantity','HomeController@save_image_quantity');
	Route::get('/order_history','HomeController@order_history');
	Route::get('/order_details/{id}','HomeController@order_details');
	Route::post('/createShipAddress','HomeController@createShipAddress');
	//20/06/2020
	Route::post('/changeemail','HomeController@changeemail');
	Route::post('/changecontactnumber','HomeController@changecontactnumber');
	Route::post('/changefullname','HomeController@changefullname');
	;
	//25/06/2020
	Route::post('/changeprofilepic','HomeController@changeprofilepic');
});	

/* paydollar payment */
Route::get('/category','HomeController@category');
Route::get('/category/{cat_id}','HomeController@select_category');
Route::post('/select_deep_category','HomeController@select_deep_category');
Route::get('/select_frame','HomeController@select_frame');
Route::get('/select_frame/{cat_id}','HomeController@select_frame');
Route::get('/payment','HomeController@payment');
Route::get('/payment-successful','HomeController@payment_successful');
Route::get('/payment-fail','HomeController@payment_fail');
Route::get('/payment-cancel','HomeController@payment_cancel');
Route::post('/multiple_uploads','HomeController@multiple_uploads');
Route::post('/multiple_uploads_social','HomeController@multiple_uploads_social');
Route::get('/delete_preview_image','HomeController@delete_preview_image');
Route::get('/pay', function () {
    return view('pay');
});

# handle payment response from the backend side of it
Route::get('/dopay', 'PaymentController@handleonlinepay')->name('dopay');

Route::get('/','HomeController@newhome');

Route::get('/home','HomeController@home')->name('home');

Route::get('/privacy_policy','HomeController@home');
Route::get('/term_of_sale','HomeController@home');

Route::get('/step_1','HomeController@step1');
Route::get('/step_2','HomeController@step2');
Route::get('/step_3','HomeController@step3');
Route::post('/step_2','HomeController@step2');
Route::post('/step_3','HomeController@step3');


Route::post('/step1_store','HomeController@step1_store');
Route::post('/step2_store','HomeController@step2_store');
Route::post('/step3_store','HomeController@step3_store');

Route::get('/email_verification/{id}', 'LoginController@email_verification');

Route::get('/custom_auth/{id}', 'LoginController@custom_auth');

Route::get('/my-rewards','HomeController@getUserRewardHistory');


/* Facebook Integration */
Route::get('auth/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleProviderCallback');

/* Google Integration */
Route::get('/redirect', 'SocialAuthGoogleController@redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');

Route::get('/payments', 'PaymentController@payments');

Route::get('/payment/process', 'PaymentController@process')->name('payment.process');


//priyanka 
Route::get('/privacy_policy_mobile','HomeController@privacy_policy');
Route::get('/terms_and_condition_mobile','HomeController@terms_and_condition');


Route::get('/privacy_policy','HomeController@privacy_policy');
Route::get('/terms_and_condition','HomeController@terms_and_condition');
Route::get('/contact_us','HomeController@contact_us');


Route::get('/imagerotateUpload','HomeController@imagerotateUpload');

//Route::get('image-crop', 'HomeController@imageCrop');
Route::post('/image-crop', 'HomeController@imageCropPost');
Route::post('/ajax_select_frame', 'HomeController@ajax_select_frame');
Route::get('/image-crop/{x}/{y}/{w}/{h}/{pic}', 'HomeController@image_crop');

Route::post('save-crop-img','HomeController@saveCropImg');
Route::post('uploadCropImage','HomeController@uploadCropImage');

